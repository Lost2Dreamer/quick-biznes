import 'dart:async';
import 'package:biznes/bloc/Searchproduct/searchbloc.dart';
import 'package:biznes/bloc/Searchproduct/searchevents.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/helper/searchhelper.dart';
import 'package:biznes/product_UI/bottomBar_UI/DropDownWidget.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:toast/toast.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/helper/productdetailshelper.dart';
import 'package:biznes/model/search_product.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import '../model/single_Product.dart';
import 'package:flutter/cupertino.dart';
import '../model/planCategories.dart';
import 'package:biznes/model/planModel.dart';
import 'package:biznes/model/subCategoryModel.dart';
import 'package:biznes/model/userPlanSubscrition.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../model/caegoryModel.dart';
import '../add_product/addProduct.dart';
import '../helper/login_helper.dart';
import '../size_config.dart';

class Membership extends StatefulWidget {
  final String userid;
  final SingleProductdetails singleproduct;
  final String gettingproductid;
  final String page;
  final SearchProductModel searchProductModel;

  Membership(
      {this.userid,
      this.singleproduct,
      this.gettingproductid,
      this.page,
      this.searchProductModel});

  @override
  _MembershipState createState() => _MembershipState();
}

class _MembershipState extends State<Membership> {
  var formKey = GlobalKey<FormState>();
  var scaffoldState = GlobalKey<ScaffoldState>();
  List<PlanModel> availablePlanModel = [];
  List<PlanCategoriesModel> planCategoriesList = [];
  List<SubCategories> subcategory = [];
  List<CategoriesModel> catforbroker = [];
  List<PlanCategoriesModel> planCat = [];
  List<String> data;
  bool isvisible = false;
  String selectCategoryPlan;
  String catforbro;
  String selectSubCategoriesPlan;
  UserPlanSubscription userPlanSubscription;
  UserPlanSubscription userPlanSubscriptionforbroker;
  List<SubCategories> subCategory = [];

  addprodductfunction() {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => AddProduct('membership')));
  }

  getSearchProducts() {
    context.bloc<SearchProductBloc>().add(FetchProduct(
          userId: widget.userid,
          categoryId: widget.searchProductModel.category,
          subcategoryId: widget.searchProductModel.subcategory,
          countryId: widget.searchProductModel.country,
          cityId: widget.searchProductModel.city,
          roleId: widget.searchProductModel.userRole,
        ));
  }

  @override
  void initState() {
    userPlanSubscription = UserPlanSubscription(
        planId: '', userId: '', categoryId: '', subcategories: []);
    getAllPlanCategories(widget.userid, context).then((planCategoryList) {
      setState(() {
        planCategoriesList = planCategoryList;
        print(planCategoriesList[0].title);
      });
    });
    getAllPlan(widget.userid, context).then((planList) {
      setState(() {
        availablePlanModel = planList;
      });
    });
    getCategories().then((value) {
      setState(() {
        catforbroker = value;
      });
    });
    data = [];
    super.initState();
  }

  selected(PlanModel data, BuildContext context) {
    SnackBar snackBar = SnackBar(
      content: Text("Selected Plan:  ${data.title}"),
      duration: Duration(milliseconds: 1500),
    );
    Scaffold.of(context).showSnackBar(snackBar);
    print("selected data ${data.title}");
  }

  void _clearAllItems() {
    userPlanSubscription.subcategories.clear();
    data.clear();
  }

  Widget buildForm(BuildContext context, token, userId) {
    var role = Provider.of<LoginHelper>(context).user.roleName;
    var search = Provider.of<SearchHelper>(context);
    var productdetails = Provider.of<ProductDetailsHelperData>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 5.0,
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 5, right: 5, bottom: 5.0),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Choose Membership Plan",
                  style: TextStyle(
                    fontFamily: fontandcolor.fontfaimlyquicksandbold,
                    fontWeight: FontWeight.normal,
                    fontSize: 3 * SizeConfig.textMultiplier,
                    color: Color(0xff586995),
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xffebf3fa),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: 180,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: availablePlanModel.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: const EdgeInsets.all(15),
                          width: 200,
                          height: 175,
                          decoration: BoxDecoration(
                            color: availablePlanModel[index].planSelected
                                ? Color(0xffffd736)
                                : Colors.white,
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  "${availablePlanModel[index].title}",
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontFamily:
                                        fontandcolor.fontfaimlyquicksandbold,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  "${availablePlanModel[index].duration} months",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily:
                                          fontandcolor.fontfaimlyregular),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  "Free",
                                  // "Rs: ${availablePlanModel[0].price}",
                                  style: TextStyle(
                                      fontFamily: fontandcolor.fontfaimly,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  "Bonus level of ${availablePlanModel[index].totalSubcategory} sub-category",
                                  style: TextStyle(
                                    fontFamily: fontandcolor.fontfaimlyregular,
                                    fontSize: size.convert(context, 10),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 2.0),
                                child: filledButton(
                                  txt: 'Select',
                                  h: size.convert(context, 30),
                                  w: size.convert(context, 100),
                                  onTap: () {
                                    selected(
                                      availablePlanModel[index],
                                      context,
                                    );
                                    int i = 0;
                                    setState(() {
                                      availablePlanModel.forEach((f) {
                                        if (i == index) {
                                          f.planSelected = true;
                                        } else {
                                          f.planSelected = false;
                                        }
                                        i++;
                                      });
                                      userPlanSubscription.planId =
                                          availablePlanModel[index].id;
                                      print(userPlanSubscription.planId);
                                    });
                                  },
                                ),
                              ),
                            ],
                          ),
                        );
                      }),
                ),
                role == "Broker"
                    ? Padding(
                        padding:
                            EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                        child: Container(
                          padding: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.black),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                                color: Colors.black,
                              ),
                              isDense: true,
                              isExpanded: true,
                              onChanged: (value) {
                                setState(() {
                                  catforbroker.forEach((element) {
                                    catforbro = value;
                                    if (element.title == value) {
                                      userPlanSubscription.categoryId =
                                          element.sId;
                                      print(userPlanSubscription.categoryId);
                                    }
                                  });
                                });
                              },
                              items: catforbroker.map((e) {
                                return DropdownMenuItem<String>(
                                  child: Text(
                                    e.title,
                                    style: TextStyle(
                                      fontFamily:
                                          fontandcolor.fontfaimlyregular,
                                    ),
                                  ),
                                  value: e.title,
                                );
                              }).toList(),
                              value: catforbro,
                              hint: Text(
                                "Select Category",
                                style: TextStyle(
                                  fontFamily: fontandcolor.fontfaimlyregular,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    : Center(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 10.0),
                          child: DropDownWidget(
                              context: context,
                              image: "assets/icons/Category.svg",
                              selectedCategory: selectCategoryPlan,
                              onChanged: (value) {
                                setState(() {
                                  planCategoriesList.forEach((f) {
                                    if (f.title == value) {
                                      print(
                                          "this is the catogry id${userPlanSubscription.categoryId}");
                                      getSubCategories(
                                        catId: f.id,
                                      ).then((list) {
                                        setState(() {
                                          userPlanSubscription.categoryId =
                                              f.id;
                                          subcategory = list;
                                          selectCategoryPlan = value;
                                        });
                                      });
                                    } else {}
                                  });
                                  _clearAllItems();
                                });
                              },
                              value: selectCategoryPlan,
                              categories: planCategoriesList,
                              hintext: "Select Category"),
                        ),
                      ),
                SizedBox(
                  height: 12.0,
                ),
                role == "Broker"
                    ? Container()
                    : Text(
                        "Get Bonus in Sub-Categories",
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontFamily: fontandcolor.fontfaimly),
                      ),
                SizedBox(
                  height: 12.0,
                ),
                role == "Broker"
                    ? Container()
                    : Container(
                        padding: EdgeInsets.symmetric(horizontal: 8.0),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, right: 8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Select bonus subcategories",
                                        style: TextStyle(
                                          fontFamily:
                                              fontandcolor.fontfaimlyregular,
                                        ),
                                      ),
                                      CupertinoButton(
                                        padding: EdgeInsets.all(8.0),
                                        minSize: double.minPositive,
                                        child: Icon(
                                          Icons.keyboard_arrow_down,
                                        ),
                                        onPressed: () {
                                          subcategory.length == 0
                                              ? Toast.show(
                                                  "Select SubCategory",
                                                  context,
                                                )
                                              : showDialog(
                                                  context: context,
                                                  barrierDismissible: false,
                                                  builder:
                                                      (BuildContext context) {
                                                    return StatefulBuilder(
                                                      builder:
                                                          (BuildContext context,
                                                              StateSetter
                                                                  setState) {
                                                        return Dialog(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          15)),
                                                          child: Container(
                                                            height: 300,
                                                            child: Column(
                                                              children: <
                                                                  Widget>[
                                                                Expanded(
                                                                  child:
                                                                      Container(
                                                                    child: ListView
                                                                        .builder(
                                                                      itemCount:
                                                                          subcategory
                                                                              .length,
                                                                      itemBuilder:
                                                                          (BuildContext context,
                                                                              int index) {
                                                                        return Column(
                                                                          children: <
                                                                              Widget>[
                                                                            CheckboxListTile(
                                                                              value: userPlanSubscription.subcategories.contains(subcategory[index].sId),
                                                                              title: Text(
                                                                                subcategory[index].title,
                                                                                style: TextStyle(
                                                                                  fontFamily: fontandcolor.fontfaimlyregular,
                                                                                ),
                                                                              ),
                                                                              onChanged: (bool selected) {
                                                                                if (selected == true) {
                                                                                  setState(() {
                                                                                    userPlanSubscription.subcategories.add(subcategory[index].sId);
                                                                                    print(userPlanSubscription.subcategories);
                                                                                    data.add(subcategory[index].title);
                                                                                    print(data);
                                                                                  });
                                                                                } else {
                                                                                  setState(() {
                                                                                    userPlanSubscription.subcategories.remove(subcategory[index].sId);
                                                                                    print(userPlanSubscription.subcategories);
                                                                                    data.remove(subcategory[index].title);
                                                                                    print(data);
                                                                                  });
                                                                                }
                                                                              },
                                                                            ),
                                                                          ],
                                                                        );
                                                                      },
                                                                    ),
                                                                  ),
                                                                ),
                                                                Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .end,
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                      padding:
                                                                          EdgeInsets.all(
                                                                              30),
                                                                      child:
                                                                          GestureDetector(
                                                                        child:
                                                                            Text(
                                                                          'Ok',
                                                                          style:
                                                                              TextStyle(
                                                                            fontFamily:
                                                                                fontandcolor.fontfaimly,
                                                                            color:
                                                                                Colors.green,
                                                                            fontSize:
                                                                                20,
                                                                          ),
                                                                        ),
                                                                        onTap:
                                                                            () {
                                                                          Navigator.of(context)
                                                                              .pop();
                                                                        },
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  });
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                SizedBox(
                  height: role == "Broker" ? 0.0 : 10.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SvgPicture.asset("assets/images/NewMembership.svg"),
                    Text(
                      "Free Trail Membership",
                      style: TextStyle(
                        fontFamily: fontandcolor.fontfaimlyquicksandbold,
                        fontWeight: FontWeight.normal,
                        fontSize: 3 * SizeConfig.textMultiplier,
                        color: Color(0xff586995),
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      "Enjoy Free Trail Subscription for limited time \n However you will Renew your membership after Expiry",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: fontandcolor.fontfaimlyregular,
                        fontWeight: FontWeight.normal,
                        fontSize: 1.5 * SizeConfig.textMultiplier,
                        color: Color(0xff5e5c5c),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    filledButton(
                      txt: "Get Subscription",
                      w: size.convert(context, 300),
                      onTap: () {
                        //This is the user id which is not getting so we
                        setState(() {
                          isvisible = true;
                        });
                        //must provide userid beacuse userpalnsub need user id
                        print(userPlanSubscription.userId = userId);
                        // print(userPlanSubscription.subcategories);
                        // print(userPlanSubscription.categoryId);
                        // print(userPlanSubscription.planId);
                        // if (userPlanSubscription.subcategories.isNotEmpty &&
                        //     userPlanSubscription.categoryId.isNotEmpty &&
                        //     userPlanSubscription.userId.isNotEmpty &&
                        //     userPlanSubscription.planId.isNotEmpty) {

                        role == "Broker"
                            ? brokerplansubscription(
                                    userPlanSubscription, context)
                                .then((value) {
                                if (value == false) {
                                  setState(() {
                                    isvisible = false;
                                  });
                                }
                                if (value == true &&
                                    widget.singleproduct != null &&
                                    widget.page != "writemessage") {
                                  membershipandproductdialog(
                                      contexts: context,
                                      text: "Membership Done",
                                      subtitle:
                                          """Congratulations, You have successfully get membership.Now you can enjoy the Ultimate Features of Quick Bizness.
                                          """,
                                      image: "assets/images/Membership.svg");
                                  getProductDetails(
                                          context: context,
                                          productId: widget.gettingproductid,
                                          userid: widget.userid)
                                      .then((singleProductList) {
                                    setState(() {
                                      isvisible = true;
                                    });
                                    if (singleProductList != null) {
                                      productdetails.productdetailsfunction(
                                          singleProductList);
                                      Navigator.of(context).pop();
                                      // Navigator.of(context).pop();
                                      // Navigator.push(
                                      //     context,
                                      //     PageTransition(
                                      //         child: ProductDetail(
                                      //           product: singleProductList,
                                      //         ),
                                      //         type: PageTransitionType.fade,
                                      //         duration:
                                      //             Duration(milliseconds: 800)));
                                    } else {}
                                  });
                                } else {
                                  Navigator.of(context).pop();
                                }
                              })
                            : getSubscription(userPlanSubscription, context)
                                .then((value) {
                                if (value == false) {
                                  setState(() {
                                    isvisible = false;
                                  });
                                }
                                if (value == true &&
                                    widget.singleproduct != null &&
                                    widget.page != "writemessage") {
                                  membershipandproductdialog(
                                      contexts: context,
                                      text: "Membership Done",
                                      subtitle:
                                          """Congratulations, You have successfully get membership.Now you can enjoy the Ultimate Features of Quick Bizness.
                                          """,
                                      image: "assets/images/Membership.svg");
                                  getProductDetails(
                                          context: context,
                                          productId: widget.gettingproductid,
                                          userid: widget.userid)
                                      .then((singleProductList) {
                                    if (singleProductList != null) {
                                      productdetails.productdetailsfunction(
                                          singleProductList);
                                      Navigator.of(context).pop();
                                      // Navigator.of(context).pop();
                                      // Navigator.push(
                                      //     context,
                                      //     PageTransition(
                                      //         child: ProductDetail(
                                      //           product: singleProductList,
                                      //         ),
                                      //         type: PageTransitionType.fade,
                                      //         duration:
                                      //             Duration(milliseconds: 800)));
                                    } else if (widget.page ==
                                        "Searchproductpage") {
                                      print("error here");
                                    }
                                  });
                                } else if (widget.page == "Searchproductpage") {
                                  membershipandproductdialog(
                                      contexts: context,
                                      text: "Membership Done",
                                      subtitle:
                                          """Congratulations, You have successfully get membership.Now you can enjoy the Ultimate Features of Quick Bizness.
                                          """,
                                      image: "assets/images/Membership.svg");
                                  getSearchProducts();
                                  Navigator.of(context).pop();
                                  // searchedProduct(
                                  //         userid: widget.userid,
                                  //         search: widget.searchProductModel)
                                  //     .then((value) {
                                  //   setState(() {
                                  //     search.productsearch = value;
                                  //     isvisible = false;
                                  //     Navigator.of(context).pop();
                                  //   });
                                  // });
                                  print(widget.searchProductModel);
                                  print("++++++++++++++++++");
                                } else {
                                  Navigator.of(context).pop();
                                }
                              });
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var token = Provider.of<LoginHelper>(context).token;
    var userid = Provider.of<LoginHelper>(context).user.sId;
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          key: scaffoldState,
          body: Builder(
            builder: (context) => Column(
              children: <Widget>[
                CustomAppBar(
                    icon: Icon(Icons.arrow_back),
                    IconPressed: () {
                      Navigator.of(context).pop();
                    },
                    context: context,
                    text: "Get Membership",
                    length: counters.toString(),
                    onTap: () {
                      counterset.zerocunter();
                      Navigator.of(context).push(PageTransition(
                          child: MyNotification(),
                          duration: Duration(milliseconds: 700),
                          type: PageTransitionType.leftToRightWithFade));
                    }),
                Expanded(
                    child: ListView(
                  children: [
                    buildForm(context, token, userid),
                  ],
                )),
              ],
            ),
          ),
        ),
        isvisible
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.4),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}

class Listofselecteddata extends StatefulWidget {
  final List<String> data;
  final int lenght;

  Listofselecteddata({this.data, this.lenght});

  @override
  _ListofselecteddataState createState() => _ListofselecteddataState();
}

class _ListofselecteddataState extends State<Listofselecteddata> {
  List<String> newlist = [];

  @override
  void initState() {
    newlist = widget.data;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 150,
      child: ListView.builder(
        itemCount: widget.lenght,
        itemBuilder: (BuildContext context, int index) {
          return Text(
            newlist[index],
            style: TextStyle(
              fontFamily: fontandcolor.fontfaimlyregular,
            ),
          );
        },
      ),
    );
  }
}
