import 'package:flutter/material.dart';
import '../model/searchmodel.dart';
import 'dart:collection';

class SearchHelper extends ChangeNotifier {
  bool isselected;
  List<ProductSearchModel> _productsearchmodel;

  List<ProductSearchModel> get productsearchmodel => _productsearchmodel;

  set productsearch(List<ProductSearchModel> productsearchmodl) {
    _productsearchmodel = productsearchmodl;
    notifyListeners();
  }

  String _searchString = "";

  UnmodifiableListView<ProductSearchModel> get dogs => _searchString.isEmpty
      ? UnmodifiableListView(_productsearchmodel)
      : UnmodifiableListView(_productsearchmodel.where((message) => message.userRole.length ==
              1
          ? message.userRole[0].title.toLowerCase().contains(_searchString.toLowerCase()) ||
              message.title
                  .toString()
                  .toLowerCase()
                  .contains(_searchString.toString().toLowerCase()) ||
              message.moq
                  .toString()
                  .toLowerCase()
                  .contains(_searchString.toString().toLowerCase()) ||
              message.price.toString().contains(_searchString)
          : message.userRole.length == 2
              ? message.userRole[0].title.toLowerCase().contains(_searchString.toLowerCase()) ||
                  message.userRole[1].title
                      .toLowerCase()
                      .contains(_searchString.toLowerCase()) ||
                  message.title
                      .toString()
                      .toLowerCase()
                      .contains(_searchString.toString().toLowerCase()) ||
                  message.moq
                      .toString()
                      .toLowerCase()
                      .contains(_searchString.toString().toLowerCase()) ||
                  message.price.toString().contains(_searchString)
              : message.userRole.length == 3
                  ? message.userRole[0].title.toLowerCase().contains(_searchString.toLowerCase()) ||
                      message.userRole[1].title
                          .toLowerCase()
                          .contains(_searchString.toLowerCase()) ||
                      message.userRole[2].title
                          .toLowerCase()
                          .contains(_searchString.toLowerCase()) ||
                      message.title
                          .toString()
                          .toLowerCase()
                          .contains(_searchString.toString().toLowerCase()) ||
                      message.moq
                          .toString()
                          .toLowerCase()
                          .contains(_searchString.toString().toLowerCase()) ||
                      message.price.toString().contains(_searchString)
                  : message.userRole.length == 4
                      ? message.userRole[0].title.toLowerCase().contains(_searchString.toLowerCase()) ||
                          message.userRole[1].title.toLowerCase().contains(_searchString.toLowerCase()) ||
                          message.userRole[2].title.toLowerCase().contains(_searchString.toLowerCase()) ||
                          message.userRole[3].title.toLowerCase().contains(_searchString.toLowerCase()) ||
                          message.title.toString().toLowerCase().contains(_searchString.toString().toLowerCase()) ||
                          message.moq.toString().toLowerCase().contains(_searchString.toString().toLowerCase()) ||
                          message.price.toString().contains(_searchString)
                      : message.price.toString().contains(_searchString) || message.moq.toString().toLowerCase().contains(_searchString.toString().toLowerCase())));

  void changeSearchString(String searchString) {
    _searchString = searchString;
    print(_searchString);
    notifyListeners();
  }
}
