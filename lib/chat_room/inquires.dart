import 'package:biznes/chat_room/chat_inquiries.dart';
import 'package:biznes/repeatedWigets/buyerseller.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import '../services/register_logic.dart';
import 'package:provider/provider.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/color.dart' as fontfaimly;
import '../helper/login_helper.dart';
import '../model/inquiresmodel.dart';
import './Inquiresaccepted.dart';

class Inquires extends StatefulWidget {
  @override
  _InquiresState createState() => _InquiresState();
}

class _InquiresState extends State<Inquires> {
  @override
  Widget build(BuildContext context) {
    String userid = Provider.of<LoginHelper>(context).user.sId;
    var sendIquires = Provider.of<LoginHelper>(context);
    return Scaffold(body: OrientationBuilder(builder: (context, orientation) {
      return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage("assets/images/newback.jpg"),
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                SizedBox(
                  width: 5,
                ),
                Text(
                  "Check all your filtered inquries",
                  style: style.MontserratMedium(
                      fontSize: size.convert(context, 12)),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                BuyerSellerContainer(
                    context: context,
                    isselected: sendIquires.getisselectedSendInquires,
                    onTap: () {
                      sendIquires.setIsselectedSendInquires(true);
                      sendIquires.setIsselectedRecieveInquires(false);

                      print(sendIquires.getisselectedSendInquires);
                    },
                    title: 'Sent Inquries'),
                BuyerSellerContainer(
                    context: context,
                    isselected: sendIquires.getisselectedRecieveInquires,
                    onTap: () {
                      sendIquires.setIsselectedSendInquires(false);
                      sendIquires.setIsselectedRecieveInquires(true);
                    },
                    title: 'Receive Inquries'),
              ],
            ),
            FutureBuilder(
              future: getallinquires(userid, context),
              builder:
                  (BuildContext context, AsyncSnapshot<AllInquiries> snapshot) {
                AllInquiries data = snapshot.data;
                return snapshot.hasData
                    ? Container(
                        width: double.infinity,
                        height: orientation == Orientation.portrait
                            ? 60.5 * SizeConfig.heightMultiplier
                            : 34.5 * SizeConfig.widthMultiplier,
                        child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          itemCount: sendIquires.getisselectedSendInquires
                              ? data.allInquiriesSend.length
                              : data.allRecieve.length,
                          itemBuilder: (context, index) {
                            return Container(
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          left: orientation ==
                                                  Orientation.portrait
                                              ? 2.0 *
                                                  SizeConfig.heightMultiplier
                                              : 2.0 *
                                                  SizeConfig.widthMultiplier,
                                          top: orientation ==
                                                  Orientation.portrait
                                              ? 1.0 *
                                                  SizeConfig.heightMultiplier
                                              : 2.0 *
                                                  SizeConfig.widthMultiplier),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            sendIquires
                                                    .getisselectedSendInquires
                                                ? data.allInquiriesSend[index]
                                                    .category
                                                : data
                                                    .allRecieve[index].category,
                                            style: TextStyle(
                                              fontFamily: fontfaimly.fontfaimly,
                                              color: Color(0xff394C81),
                                              fontSize: orientation ==
                                                      Orientation.portrait
                                                  ? 1.7 *
                                                      SizeConfig.textMultiplier
                                                  : 1.7 *
                                                      SizeConfig.textMultiplier,
                                            ),
                                          ),
                                          Text(
                                            "Given results for send inquiries",
                                            style: TextStyle(
                                              color: Color(0xff5E5C5C),
                                              fontFamily:
                                                  fontfaimly.fontfaimlyregular,
                                              fontSize: orientation ==
                                                      Orientation.portrait
                                                  ? 1.7 *
                                                      SizeConfig.textMultiplier
                                                  : 1.7 *
                                                      SizeConfig.textMultiplier,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    decoration: BoxDecoration(
                                      color: Color(0xffebf3fa),
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10),
                                      ),
                                    ),
                                    margin: EdgeInsets.only(
                                        left: 10, right: 10, top: 10),
                                    width: double.infinity,
                                    height: orientation == Orientation.portrait
                                        ? 6 * SizeConfig.heightMultiplier
                                        : 12 * SizeConfig.widthMultiplier,
                                  ),
                                  Container(
                                    width: double.infinity,
                                    height: orientation == Orientation.portrait
                                        ? 7 * SizeConfig.heightMultiplier
                                        : 9 * SizeConfig.widthMultiplier,
                                    decoration: BoxDecoration(
                                      color: Color(0xffebf3fa),
                                      borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(10),
                                        bottomRight: Radius.circular(10),
                                      ),
                                    ),
                                    margin:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: Column(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                            left: MediaQuery.of(context)
                                                        .orientation ==
                                                    Orientation.portrait
                                                ? (40 / 8.148314082864863) *
                                                    SizeConfig.heightMultiplier
                                                : (40 / 8.148314082864863) *
                                                    SizeConfig.widthMultiplier,
                                            right: MediaQuery.of(context)
                                                        .orientation ==
                                                    Orientation.portrait
                                                ? (40 / 8.148314082864863) *
                                                    SizeConfig.heightMultiplier
                                                : (40 / 8.148314082864863) *
                                                    SizeConfig.widthMultiplier,
                                          ),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                      sendIquires
                                                              .getisselectedSendInquires
                                                          ? data
                                                              .allInquiriesSend[
                                                                  index]
                                                              .acceptedMessages
                                                              .toString()
                                                          : data
                                                              .allRecieve[index]
                                                              .acceptedMessages
                                                              .toString(),
                                                      style: TextStyle(
                                                        fontFamily: fontfaimly
                                                            .fontfaimly,
                                                        color: Colors.black,
                                                        fontSize: MediaQuery.of(
                                                                        context)
                                                                    .orientation ==
                                                                Orientation
                                                                    .portrait
                                                            ? (20 / 8.148314082864863) *
                                                                SizeConfig
                                                                    .heightMultiplier
                                                            : (20 / 8.148314082864863) *
                                                                SizeConfig
                                                                    .widthMultiplier,
                                                      )),
                                                  InkWell(
                                                    onTap: () {
                                                      print("hello");
                                                      Navigator.of(context).push(
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                                      ChatTile(
                                                                        categoryId: sendIquires.getisselectedSendInquires
                                                                            ? data.allInquiriesSend[index].categoryId
                                                                            : data.allRecieve[index].categoryId,
                                                                        page:
                                                                            "inquiries",
                                                                        status:
                                                                            "Accepted",
                                                                        userId:
                                                                            userid,
                                                                        roleName: sendIquires.getisselectedSendInquires
                                                                            ? "Buyer"
                                                                            : "Seller",
                                                                      )));
                                                      // Navigator.of(context)
                                                      //     .push(MaterialPageRoute(
                                                      //   builder: (_) => InquiresAccepted(
                                                      //       sendIquires
                                                      //               .getisselectedSendInquires
                                                      //           ? data
                                                      //               .allInquiriesSend[
                                                      //                   index]
                                                      //               .category
                                                      //           : data
                                                      //               .allRecieve[
                                                      //                   index]
                                                      //               .category,
                                                      //       "Accepted",
                                                      //       userid),
                                                      // ));
                                                    },
                                                    child: Container(
                                                      child: Text(
                                                        "ACCEPTED",
                                                        style: TextStyle(
                                                          color:
                                                              Color(0xff51D289),
                                                          fontFamily: fontfaimly
                                                              .fontfaimly,
                                                          fontSize: MediaQuery.of(
                                                                          context)
                                                                      .orientation ==
                                                                  Orientation
                                                                      .portrait
                                                              ? (15 / 8.148314082864863) *
                                                                  SizeConfig
                                                                      .heightMultiplier
                                                              : (25 / 8.148314082864863) *
                                                                  SizeConfig
                                                                      .widthMultiplier,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    sendIquires
                                                            .getisselectedSendInquires
                                                        ? data
                                                            .allInquiriesSend[
                                                                index]
                                                            .rejectedMessages
                                                            .toString()
                                                        : data.allRecieve[index]
                                                            .rejectedMessages
                                                            .toString(),
                                                    style: TextStyle(
                                                      color: Colors.black,
                                                      fontFamily:
                                                          fontfaimly.fontfaimly,
                                                      fontSize: MediaQuery.of(
                                                                      context)
                                                                  .orientation ==
                                                              Orientation
                                                                  .portrait
                                                          ? (20 / 8.148314082864863) *
                                                              SizeConfig
                                                                  .heightMultiplier
                                                          : (20 / 8.148314082864863) *
                                                              SizeConfig
                                                                  .widthMultiplier,
                                                    ),
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      Navigator.of(context).push(
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                                      ChatTile(
                                                                        categoryId: sendIquires.getisselectedSendInquires
                                                                            ? data.allInquiriesSend[index].categoryId
                                                                            : data.allRecieve[index].categoryId,
                                                                        page:
                                                                            "inquiries",
                                                                        status:
                                                                            "Rejected",
                                                                        userId:
                                                                            userid,
                                                                        roleName: sendIquires.getisselectedSendInquires
                                                                            ? "Buyer"
                                                                            : "Seller",
                                                                      )));
                                                      // Navigator.of(context).push(MaterialPageRoute(
                                                      //     builder: (_) => InquiresAccepted(
                                                      //         sendIquires
                                                      //                 .getisselectedSendInquires
                                                      //             ? data
                                                      //                 .allInquiriesSend[
                                                      //                     index]
                                                      //                 .category
                                                      //             : data
                                                      //                 .allRecieve[
                                                      //                     index]
                                                      //                 .category,
                                                      //         "Rejected",
                                                      //         userid)));
                                                    },
                                                    child: Container(
                                                      child: Text(
                                                        "REJECTED",
                                                        style: TextStyle(
                                                          fontFamily: fontfaimly
                                                              .fontfaimly,
                                                          color:
                                                              Color(0xffFD5E5F),
                                                          fontSize: MediaQuery.of(
                                                                          context)
                                                                      .orientation ==
                                                                  Orientation
                                                                      .portrait
                                                              ? (15 / 8.148314082864863) *
                                                                  SizeConfig
                                                                      .heightMultiplier
                                                              : (25 / 8.148314082864863) *
                                                                  SizeConfig
                                                                      .widthMultiplier,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    sendIquires
                                                            .getisselectedSendInquires
                                                        ? data
                                                            .allInquiriesSend[
                                                                index]
                                                            .pendingMessages
                                                            .toString()
                                                        : data.allRecieve[index]
                                                            .pendingMessages
                                                            .toString(),
                                                    style: TextStyle(
                                                      fontFamily:
                                                          fontfaimly.fontfaimly,
                                                      color: Colors.black,
                                                      fontSize: MediaQuery.of(
                                                                      context)
                                                                  .orientation ==
                                                              Orientation
                                                                  .portrait
                                                          ? (20 / 8.148314082864863) *
                                                              SizeConfig
                                                                  .heightMultiplier
                                                          : (20 / 8.148314082864863) *
                                                              SizeConfig
                                                                  .widthMultiplier,
                                                    ),
                                                  ),
                                                  InkWell(
                                                    onTap: () {
                                                      Navigator.of(context).push(
                                                          MaterialPageRoute(
                                                              builder:
                                                                  (context) =>
                                                                      ChatTile(
                                                                        categoryId: sendIquires.getisselectedSendInquires
                                                                            ? data.allInquiriesSend[index].categoryId
                                                                            : data.allRecieve[index].categoryId,
                                                                        page:
                                                                            "inquiries",
                                                                        status:
                                                                            "Pending",
                                                                        userId:
                                                                            userid,
                                                                        roleName: sendIquires.getisselectedSendInquires
                                                                            ? "Buyer"
                                                                            : "Seller",
                                                                      )));
                                                      // Navigator.of(context).push(MaterialPageRoute(
                                                      //     builder: (_) => InquiresAccepted(
                                                      //         sendIquires
                                                      //                 .getisselectedSendInquires
                                                      //             ? data
                                                      //                 .allInquiriesSend[
                                                      //                     index]
                                                      //                 .category
                                                      //             : data
                                                      //                 .allRecieve[
                                                      //                     index]
                                                      //                 .category,
                                                      //         "Pending",
                                                      //         userid)));
                                                    },
                                                    child: Container(
                                                      child: Text(
                                                        "PENDING",
                                                        style: TextStyle(
                                                          fontFamily: fontfaimly
                                                              .fontfaimly,
                                                          color:
                                                              Color(0xff3D5083),
                                                          fontSize: MediaQuery.of(
                                                                          context)
                                                                      .orientation ==
                                                                  Orientation
                                                                      .portrait
                                                              ? (15 / 8.148314082864863) *
                                                                  SizeConfig
                                                                      .heightMultiplier
                                                              : (25 / 8.148314082864863) *
                                                                  SizeConfig
                                                                      .widthMultiplier,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      )
                    : Container(
                        height: orientation == Orientation.portrait
                            ? 60.5 * SizeConfig.heightMultiplier
                            : 34.5 * SizeConfig.widthMultiplier,
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      );
              },
            ),
          ],
        ),
      );
    }));
  }
}
