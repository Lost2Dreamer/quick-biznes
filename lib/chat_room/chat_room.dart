import 'dart:convert';
import 'dart:io' as Io;
import 'dart:ui';
import 'package:biznes/Backendservices/api.dart';
import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/chat_room/globals.dart';
import 'package:biznes/helper/conversationhelper.dart';
import 'package:biznes/memberShip/dilouge.dart';
import 'package:biznes/products_Detail/GetAllProductsIndividual.dart';
import 'package:biznes/products_Detail/Largeimage.dart';
import 'package:biznes/repeatedWigets/CustomTextField.dart';
import 'package:biznes/repeatedWigets/customTextField2.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:biznes/helper/login_helper.dart';
import 'dart:async';
import 'package:biznes/services/server.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import '../products_Detail/extra.dart';
import 'package:biznes/model/messagesmodelnew.dart';
import 'package:biznes/res/color.dart' as color;
import 'package:biznes/model/conversationmodel.dart';
import 'package:biznes/model/single_message_model.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart' as intl;
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../model/messagetyping.dart';
import 'package:image_picker/image_picker.dart';
import 'package:biznes/model/typingmodel.dart';

enum Reportdata {
  fraud,
  illegalDeal,
  offensiveContent,
  PrivacyOffensive,
  other
}

class ChatRoom extends StatefulWidget {
  final String conversationid;
  final String createdforid;
  final String categoryid;
  final String username;
  final String subcategoryid;
  final String userid;
  final String status;
  final String image;
  final String createdbyid;
  final String messageusername;
  final String onlinestatus;
  final String organizationname;
  final String description;
  final String city;
  final String country;
  final String rolename;

  ChatRoom({
    this.conversationid,
    this.createdforid,
    this.categoryid,
    this.subcategoryid,
    this.userid,
    this.status,
    this.username,
    this.image,
    this.createdbyid,
    this.messageusername,
    this.onlinestatus,
    this.organizationname,
    this.description,
    this.city,
    this.country,
    this.rolename,
  });

  @override
  _ChatRoomState createState() => _ChatRoomState();
}

class _ChatRoomState extends State<ChatRoom> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController proposed_price = TextEditingController();
  TextEditingController other_description = TextEditingController();
  bool isshowLoadingforblockUser = false;
  Reportdata reportdata = Reportdata.fraud;
  Box unreadsBox;
  String _testString = "Write your message...";
  String _reportUserMessage = "Fraud";
  String _reportUserComments = "";
  bool iskeyboardvisiblity = true;
  String typingname;
  List<String> messages;
  bool isShowOffer = false;
  List<NewMessages> newmessageN;
  List<CreatedBy> createdby;
  TypingStatus typingStatus;
  MessagesModel messagesModel = MessagesModel();
  String status;
  bool isloading = false;
  String _userNameTyping;
  Timer _typingTimer;
  bool _isTyping = false;
  List idsfordeleting = [];
  var formKey = GlobalKey<FormState>();
  var volume = Icons.volume_up;
  String testing = "testingdata";
  TextEditingController _messagecontroller = TextEditingController();
  SingleMessageModel singleMessageModel;
  ScrollController _scrollController;
  var image;
  Io.File sendimage;

  Future pickGallery() async {
    sendimage = await ImagePicker.pickImage(source: ImageSource.gallery);
    // final bytes = Io.File(sendimage.path).readAsBytesSync();

    setState(() {
      // image = base64Encode(bytes);
      image = sendimage.path;
      print(sendimage.path);
    });
  }

  bool subscriptionstatus = false;
  var data;

  gettingsubscribtionstatus() async {
    print(widget.userid);
    print(widget.conversationid);
    data = await getsubscriptionstatus(
        widget.conversationid, widget.userid, context);

    setState(() {
      subscriptionstatus = data["canMessage"];
    });
  }

// @override
// void dispose() {
//   super.dispose();
//   _socketIO.onDisconnect((d) {
//     print(d + "disconnect");
//   });
// }

  @override
  void initState() {
    super.initState();
    print(widget.createdbyid + "created by");
    print(widget.userid + "user id");
    unreadsBox = Hive.box('conversationsGeneration');
    newmessageN = List<NewMessages>();
//    _socketIOManager = SocketIOManager();
    _scrollController = ScrollController();
    typingStatus = TypingStatus(userName: 'umair', conversationId: '');
    status = widget.status;
    createdby = List<CreatedBy>();

    Future.delayed(Duration.zero, () {
      gettingsubscribtionstatus();
    });

    retriveMessagesFromLocalStorage(context);
    // getconversation(widget.conversationid, widget.userid, context)
    //     .then((value) {
    //   Provider.of<NewMessagesHlper>(context, listen: false)
    //       .setNewMessages(value);
    //   setState(() {
    //     conversation_id = widget.conversationid;
    //     newmessageN = value;
    //     isloading = true;
    //   });
    // });
//    this.init();
  }

  retriveMessagesFromLocalStorage(context) async {
    final bool isPresent = unreadsBox.containsKey(widget.conversationid);
    if (!isPresent) {
      getconversation(widget.conversationid, widget.userid, context)
          .then((value) {
        print(value[0].sId);
        Provider.of<NewMessagesHlper>(context, listen: false)
            .setNewMessages(value);
        unreadsBox.put(widget.conversationid, value);
        setState(() {
          conversation_id = widget.conversationid;
          newmessageN = value;
          isloading = true;
        });
      });
    } else {
      getconversation(widget.conversationid, widget.userid, context)
          .then((value) {
        print(value[0].sId);
        Provider.of<NewMessagesHlper>(context, listen: false)
            .setNewMessages(value);
        unreadsBox.put(widget.conversationid, value);
        setState(() {
          conversation_id = widget.conversationid;
          newmessageN = value;
          isloading = true;
        });
      });
      Provider.of<NewMessagesHlper>(context, listen: false)
          .setNewMessages(unreadsBox.get(widget.conversationid));
      setState(() {
        isloading = true;
      });
    }
  }

//  void init() async {
//    final Map<String, String> userMap = {
//      'userid': widget.userid,
//    };
////    _socketIO = await _socketIOManager.createInstance(SocketOptions(
//      "$base_url",
//      nameSpace: '/',
//      path: '/socket.io',
//      query: userMap,
//      enableLogging: true,
//      transports: [Transports.WEB_SOCKET],
//    ));
//    _socketIO.connect();
//    _socketIO.onConnect((data) {
//      print(data.toString() + "1++++++++++++++++++");
//    });
//    _socketIO.onError((data) {
//      print(data);
//    });
////   _socketIO.isConnected();
////   _socketIO.on("listeningNewFuntion", (data) {
////     this.setState(() {
////       _userNameTyping = data['userName'];
////     });
////   });
////   _socketIO.on('messageRecieved' + widget.conversationid, (data) {
////     print(data);
////     setState(() {
////       CreatedBy createdBy = CreatedBy(sId: data["createdBy"]);
////       newmessage.insert(
////           0,
////           NewMessages(
////               message: data["message"],
////               createdBy: createdBy,
////               attachment: data['attachment'],
////               createdAt: data["createdAt"]));
////     });
////     _scrollController.animateTo(
////       0.0,
////       duration: Duration(milliseconds: 200),
////       curve: Curves.bounceInOut,
////     );
////   });
////   _socketIO.on('messageRecievedNew' + widget.conversationid, (data) {
////     setState(() {
////       CreatedBy createdBy = CreatedBy(sId: data["createdBy"]);
////       newmessage.insert(
////           0,
////           NewMessages(
////               message: data["message"],
////               createdBy: createdBy,
////               attachment: data['attachment'],
////               createdAt: data["createdAt"]));
////     });
////     _scrollController.animateTo(
////       0.0,
////       duration: Duration(milliseconds: 200),
////       curve: Curves.bounceInOut,
////     );
////   });
////   // _socketIO.on('messageRecieved' + widget.conversationid, (jsondata) {
////   //   print(jsondata.toString()+"This is consoling data");
////   //   setState(() {
////   //     _testString=jsondata["createdBy"];
////   //   });
////   //   Map<String, dynamic> data = json.decode(jsondata);
////   //   CreatedBy createdBy = CreatedBy(sId: jsondata["createdBy"]);
////   //   print(jsondata['message']+"I am printing this message");
////   //   this.setState(() {
////   //     CreatedBy createdBy = CreatedBy(sId: jsondata["createdBy"]);
////   //     newmessage.insert(
////   //         0,
////   //         NewMessages(
////   //             message: jsondata["message"],
////   //             // createdBy: createdBy,
////   //             attachment: jsondata['attachment'],
////   //             createdAt: jsondata["createdAt"]));
////   //   });
////   //   _scrollController.animateTo(
////   //     0.0,
////   //     duration: Duration(milliseconds: 200),
////   //     curve: Curves.bounceInOut,
////   //   );
////   // });
////   // _socketIO.on('messageRecievedNew' + widget.conversationid,
////   //     (jsondata) {
////   //       setState(() {
////   //     _testString=jsondata["createdBy"];
////   //   });
////   //       print(jsondata['message'].toString()+"I am printing this message");
////   //   print(jsondata.toString()+"This is consoling data from second events");
////   //   Map<String, dynamic> data = json.decode(jsondata);
////   //   print(data.toString()+"this is data");
////   //   CreatedBy createdBy = CreatedBy(sId: jsondata["createdBy"].toString());
////
////   // setState(() {
////   //   newmessage.insert(
////   //       0,
////   //       NewMessages(
////   //           message: jsondata["message"].toString(),
////   //           // createdBy: createdBy,
////   //           attachment: jsondata['attachment'].toString(),
////   //           createdAt: jsondata["createdAt"].toString()));
////   // });
////   // _scrollController.animateTo(
////   //   0.0,
////   //   duration: Duration(milliseconds: 200),
////   //   curve: Curves.bounceInOut,
////   // );
////   // });
//  }

  Widget buildMessagebeforestatus(orientation, context) {
    var userid = Provider.of<LoginHelper>(context).user;
    String profilepic = Provider.of<LoginHelper>(context).user.profilePic;
    var newmessage = Provider.of<NewMessagesHlper>(context).newmessages;
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            height: orientation == Orientation.portrait
                ? 30 * SizeConfig.heightMultiplier
                : 10 * SizeConfig.heightMultiplier,
            child: isloading
                ? ListView.builder(
                    physics: BouncingScrollPhysics(),
                    controller: _scrollController,
                    itemCount: newmessage.length,
                    itemBuilder: (BuildContext context, int index) {
                      DateTime dateTime1 =
                          DateTime.parse(newmessage[index].createdAt);
                      String formattedTime1 = intl.DateFormat()
                          .format(dateTime1.add(Duration(hours: 5)));
                      return messageTiles(
                          formattedTime1: formattedTime1,
                          profilepic: profilepic,
                          context: context,
                          userid: userid,
                          index: index);
                    },
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _BLockAndAllowContainer(
                    mainTitle: widget.messageusername,
                    text1:
                        widget.status == "Rejected" ? 'Accept Again' : 'Accept',
                    isRejected: widget.status == "Rejected" ? true : false,
                    text2: widget.status == "Rejected" ? '' : 'Reject',
                    imageCircle: widget.image,
                    functionTwo: () {
                      conversationstatus(widget.conversationid.toString(),
                          "Rejected", context);
                      Navigator.of(context).pop();
                    },
                    functionOne: () {
                      conversationstatus(widget.conversationid.toString(),
                          "Accepted", context);
                      setState(() {
                        status = "Accepted";
                      });
                    },
                    title:
                        'Accept message will Allow you to start conversation'),
              ],
            ),
          ),
// Container(
//   height: orientation==Orientation.portrait?20*SizeConfig.heightMultiplier:
//   5*SizeConfig.heightMultiplier,
// ),
        ],
      ),
    );
  }

  adddatafordeleting(index, context) {
    if (newmessageN.any((element) => element.isselectedchat)) {
      setState(() {
        newmessageN[index].isselectedchat = !newmessageN[index].isselectedchat;

        if (newmessageN[index].isselectedchat == true) {
          setState(() {
            idsfordeleting.add(newmessageN[index].sId);
            print(idsfordeleting);
          });
        } else {
          setState(() {
            idsfordeleting.remove(newmessageN[index].sId);
            print(idsfordeleting);
          });
        }
      });
    }
  }

  addingdat(index, context) {
    newmessageN[index].isselectedchat = !newmessageN[index].isselectedchat;
    idsfordeleting.add(newmessageN[index].sId);
  }

  Widget messageTiles({index, userid, formattedTime1, profilepic, context}) {
    var newmessage = Provider.of<NewMessagesHlper>(context).newmessages;
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            textDirection: newmessage[index].createdBy.sId == userid.sId
                ? TextDirection.rtl
                : TextDirection.ltr,
            children: [
              newmessage[index].createdBy.sId == userid.sId
                  ? SizedBox()
                  : Card(
                      elevation: 10,
                      color: Colors.green,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(
                              newmessage[index].createdBy.sId == userid.sId
                                  ? 20
                                  : 0),
                          bottomLeft: Radius.circular(
                              newmessage[index].createdBy.sId == userid.sId
                                  ? 0
                                  : 20),
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                      ),
                    ),
              Column(
                crossAxisAlignment:
                    newmessage[index].createdBy.sId == userid.sId
                        ? CrossAxisAlignment.end
                        : CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(
                    height: 3,
                  ),
                  newmessage[index].createdBy.sId == userid.sId
                      ? Row(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width * .60,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Card(
                                  elevation: 10,
                                  clipBehavior: Clip.antiAlias,
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                  ),
                                  child: newmessage[index].attachment != "" &&
                                          newmessage[index].attachment !=
                                              null &&
                                          newmessage[index].message != ""
                                      ? GestureDetector(
                                          onLongPress: () {
                                            setState(() {
                                              addingdat(index, context);
                                            });
                                          },
                                          onTap: () {
                                            idsfordeleting.length == 0
                                                ? Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            LargeImage(
                                                                "$base_url/${newmessage[index].attachment}")))
                                                : setState(() {
                                                    adddatafordeleting(
                                                        index, context);
                                                  });
                                          },
                                          child: Container(
                                            color:
                                                newmessage[index].isselectedchat
                                                    ? color.selectedcolor
                                                    : Colors.white,
                                            child: Column(
                                              children: [
                                                Container(
                                                  height: 20 *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                  width: 60 *
                                                      SizeConfig
                                                          .widthMultiplier,
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 4,
                                                      horizontal: 4),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    child: CachedNetworkImage(
                                                      height: 18 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                      width: 60 *
                                                          SizeConfig
                                                              .widthMultiplier,
                                                      imageUrl:
                                                          '$base_url/${newmessage[index].attachment}',
                                                      placeholder: (context,
                                                              url) =>
                                                          Center(
                                                              child:
                                                                  CircularProgressIndicator(
                                                        backgroundColor:
                                                            Colors.white,
                                                      )),
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                                Divider(),
                                                Container(
                                                  color: newmessage[index]
                                                          .isselectedchat
                                                      ? color.selectedcolor
                                                      : Colors.white,
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 5,
                                                      horizontal: 15),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: <Widget>[
                                                      textinchat(
                                                          newmessage[index]
                                                              .message),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            "$formattedTime1",
                                                            style: TextStyle(
                                                                fontSize: (8 /
                                                                        8.148314082864863) *
                                                                    SizeConfig
                                                                        .textMultiplier,
                                                                color: Colors
                                                                    .blueGrey),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      : newmessage[index].attachment == null &&
                                                  newmessage[index].message !=
                                                      null ||
                                              newmessage[index].message != ""
                                          ? GestureDetector(
                                              onLongPress: () {
                                                this.setState(() {
                                                  addingdat(index, context);
                                                });
                                              },
                                              onTap: () {
                                                adddatafordeleting(
                                                    index, context);
                                              },
                                              child: Container(
                                                color: newmessage[index]
                                                        .isselectedchat
                                                    ? color.selectedcolor
                                                    : newmessage[index].offer ==
                                                            "true"
                                                        ? color
                                                            .offerContainerColor
                                                        : color.tabbarcolor,
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 10,
                                                        horizontal: 15),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      newmessage[index].offer ==
                                                              "true"
                                                          ? CrossAxisAlignment
                                                              .start
                                                          : CrossAxisAlignment
                                                              .end,
                                                  children: <Widget>[
                                                    newmessage[index].offer ==
                                                            "true"
                                                        ? offerText(context,
                                                            "you sent an offer !")
                                                        : Container(),
                                                    textforsender(
                                                        newmessage[index]
                                                            .message,
                                                        isoffer: newmessage[
                                                                        index]
                                                                    .offer ==
                                                                "true"
                                                            ? true
                                                            : false),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 8.0),
                                                          child: Text(
                                                            "$formattedTime1",
                                                            textAlign:
                                                                TextAlign.right,
                                                            style: TextStyle(
                                                                fontSize: (8 /
                                                                        8.148314082864863) *
                                                                    SizeConfig
                                                                        .textMultiplier,
                                                                color: newmessage[index]
                                                                            .offer ==
                                                                        "true"
                                                                    ? color
                                                                        .appbarfontandiconcolor
                                                                    : Colors
                                                                        .white),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          : newmessage[index].message == null &&
                                                  newmessage[index].message ==
                                                      "" &&
                                                  newmessage[index]
                                                          .attachment !=
                                                      null &&
                                                  newmessage[index]
                                                          .attachment !=
                                                      ""
                                              ? Container(
                                                  color: newmessage[index]
                                                              .createdBy
                                                              .sId ==
                                                          userid.sId
                                                      ? Colors.green
                                                      : Colors.white,
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 10,
                                                      horizontal: 15),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        newmessage[index]
                                                            .message,
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontFamily:
                                                              'MontserratRegular',
                                                          fontSize: 16,
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              : GestureDetector(
                                                  onTap: () {
                                                    idsfordeleting.length == 0
                                                        ? Navigator.of(context)
                                                            .push(MaterialPageRoute(
                                                                builder: (context) =>
                                                                    LargeImage(
                                                                        "$base_url/${newmessage[index].attachment}")))
                                                        : setState(() {
                                                            adddatafordeleting(
                                                                index, context);
                                                          });
                                                  },
                                                  onLongPress: () {
                                                    setState(() {
                                                      addingdat(index, context);
                                                    });
                                                  },
                                                  child: Container(
                                                    color: newmessage[index]
                                                            .isselectedchat
                                                        ? color.selectedcolor
                                                        : Colors.white,
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          height: 20 *
                                                              SizeConfig
                                                                  .heightMultiplier,
                                                          width: 60 *
                                                              SizeConfig
                                                                  .widthMultiplier,
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  vertical: 4,
                                                                  horizontal:
                                                                      4),
                                                          child: ClipRRect(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                            child:
                                                                CachedNetworkImage(
                                                              height: 18 *
                                                                  SizeConfig
                                                                      .heightMultiplier,
                                                              width: 60 *
                                                                  SizeConfig
                                                                      .widthMultiplier,
                                                              imageUrl:
                                                                  '$base_url/${newmessage[index].attachment}',
                                                              placeholder: (context,
                                                                      url) =>
                                                                  Center(
                                                                      child:
                                                                          CircularProgressIndicator(
                                                                backgroundColor:
                                                                    Colors
                                                                        .white,
                                                              )),
                                                              fit: BoxFit.cover,
                                                            ),
                                                          ),
                                                        ),
                                                        Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .end,
                                                          children: [
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          8.0),
                                                              child: Text(
                                                                "$formattedTime1",
                                                                style: TextStyle(
                                                                    fontSize: (8 /
                                                                            8.148314082864863) *
                                                                        SizeConfig
                                                                            .textMultiplier,
                                                                    color: Colors
                                                                        .blueGrey),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 4,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                ),
                              ),
                            ),
                            newmessage[index].createdBy.sId == userid.sId
                                ? CircleAvatar(
                                    backgroundImage: CachedNetworkImageProvider(
                                      '$base_url/$profilepic',
                                    ),
                                  )
                                : Container(),
                          ],
                        )
                      : Row(
                          children: [
                            CircleAvatar(
                              backgroundImage:
                                  CachedNetworkImageProvider(widget.image),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width * .60,
                              child: Align(
                                alignment: newmessage[index].createdBy.sId ==
                                        userid.sId
                                    ? Alignment.centerRight
                                    : Alignment.centerLeft,
                                child: Card(
                                  elevation: 10,
                                  clipBehavior: Clip.antiAlias,
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                  ),
                                  child: newmessage[index].attachment != null &&
                                          newmessage[index].attachment != "" &&
                                          newmessage[index].message != null &&
                                          newmessage[index].message != ""
                                      ? GestureDetector(
                                          onLongPress: () {
                                            this.setState(() {
                                              addingdat(index, context);
                                            });
                                          },
                                          onTap: () {
                                            idsfordeleting.length == 0
                                                ? Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            LargeImage(
                                                                "$base_url/${newmessage[index].attachment}")))
                                                : adddatafordeleting(
                                                    index, context);
                                          },
                                          child: Container(
                                            color:
                                                newmessage[index].isselectedchat
                                                    ? color.selectedcolor
                                                    : Colors.white,
                                            child: Column(
                                              children: [
                                                Container(
                                                  height: 20 *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                  width: 60 *
                                                      SizeConfig
                                                          .widthMultiplier,
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 4,
                                                      horizontal: 4),
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    child: CachedNetworkImage(
                                                      height: 18 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                      width: 60 *
                                                          SizeConfig
                                                              .widthMultiplier,
                                                      imageUrl:
                                                          '$base_url/${newmessage[index].attachment}',
                                                      placeholder: (context,
                                                              url) =>
                                                          Center(
                                                              child:
                                                                  CircularProgressIndicator(
                                                        backgroundColor:
                                                            Colors.white,
                                                      )),
                                                      fit: BoxFit.cover,
                                                    ),
                                                  ),
                                                ),
                                                Divider(),
                                                Container(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 5,
                                                      horizontal: 15),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .start,
                                                        children: [
                                                          Expanded(
                                                            child: textinchat(
                                                                newmessage[
                                                                        index]
                                                                    .message),
                                                          ),
                                                        ],
                                                      ),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        children: [
                                                          Text(
                                                            "$formattedTime1",
                                                            style: TextStyle(
                                                                fontSize: (8 /
                                                                        8.148314082864863) *
                                                                    SizeConfig
                                                                        .textMultiplier,
                                                                color: Colors
                                                                    .blueGrey),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      : newmessage[index].attachment == null &&
                                              newmessage[index].message != null
                                          ? GestureDetector(
                                              onLongPress: () {
                                                this.setState(() {
                                                  addingdat(index, context);
                                                });
                                              },
                                              onTap: () {
                                                adddatafordeleting(
                                                    index, context);
                                              },
                                              child: Container(
                                                color: newmessage[index]
                                                        .isselectedchat
                                                    ? color.selectedcolor
                                                    : newmessage[index].offer ==
                                                            "true"
                                                        ? color
                                                            .offerContainerColor
                                                        : Colors.white,
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 10,
                                                        horizontal: 15),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    newmessage[index].offer ==
                                                            "true"
                                                        ? offerText(context,
                                                            "${widget.messageusername} sent an offer !")
                                                        : Container(),
                                                    textinchat(
                                                      newmessage[index].message,
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.end,
                                                      children: [
                                                        Text(
                                                          "$formattedTime1",
                                                          style: TextStyle(
                                                              fontSize: (8 /
                                                                      8.148314082864863) *
                                                                  SizeConfig
                                                                      .textMultiplier,
                                                              color: Colors
                                                                  .blueGrey),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          : GestureDetector(
                                              onLongPress: () {
                                                this.setState(() {
                                                  addingdat(index, context);
                                                });
                                              },
                                              onTap: () {
                                                idsfordeleting.length == 0
                                                    ? Navigator.of(context)
                                                        .push(MaterialPageRoute(
                                                            builder: (context) =>
                                                                LargeImage(
                                                                    "$base_url/${newmessage[index].attachment}")))
                                                    : adddatafordeleting(
                                                        index, context);
                                              },
                                              child: Container(
                                                color: newmessage[index]
                                                        .isselectedchat
                                                    ? color.selectedcolor
                                                    : Colors.white,
                                                child: Column(
                                                  children: [
                                                    Container(
                                                      height: 20 *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                      width: 60 *
                                                          SizeConfig
                                                              .widthMultiplier,
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          vertical: 4,
                                                          horizontal: 4),
                                                      child: ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                        child:
                                                            CachedNetworkImage(
                                                          height: 18 *
                                                              SizeConfig
                                                                  .heightMultiplier,
                                                          width: 60 *
                                                              SizeConfig
                                                                  .widthMultiplier,
                                                          imageUrl:
                                                              '$base_url/${newmessage[index].attachment}',
                                                          placeholder: (context,
                                                                  url) =>
                                                              Center(
                                                                  child:
                                                                      CircularProgressIndicator(
                                                            backgroundColor:
                                                                Colors.white,
                                                          )),
                                                          fit: BoxFit.cover,
                                                        ),
                                                      ),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment.end,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  right: 8.0),
                                                          child: Text(
                                                            "$formattedTime1",
                                                            style: TextStyle(
                                                                fontSize: (8 /
                                                                        8.148314082864863) *
                                                                    SizeConfig
                                                                        .textMultiplier,
                                                                color: Colors
                                                                    .blueGrey),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 4,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                ),
                              ),
                            ),
                          ],
                        ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget messagedata(context) {
    var newmessage = Provider.of<NewMessagesHlper>(context).newmessages;
    var loading = Provider.of<NewMessagesHlper>(context).isloading;
    var userid = Provider.of<LoginHelper>(context).user;
    String profilepic = Provider.of<LoginHelper>(context).user.profilePic;
    return isloading
        ? ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: newmessage.length,
            reverse: true,
            controller: _scrollController,
            itemBuilder: (BuildContext context, int index) {
              DateTime dateTime1 = DateTime.parse(newmessage[index].createdAt);
              String formattedTime1 =
                  intl.DateFormat().format(dateTime1.add(Duration(hours: 5)));
              return messageTiles(
                  index: index,
                  formattedTime1: formattedTime1,
                  userid: userid,
                  context: context,
                  profilepic: profilepic);
            })
        : Center(
            child: CircularProgressIndicator(),
          );
  }

//  void sendnewmessagetouser() {
//    _socketIO.emit('sendMessage', [
//      json.encode({
//        'createdBy': widget.userid,
//        'createdFor': widget.createdforid,
//        'categoryId': widget.categoryid,
//        'subCategoryId': widget.subcategoryid,
//        'message': _messagecontroller.text,
//        'conversationId': widget.conversationid,
//        'attachment': image,
//      }),
//    ]);
//  }

  Widget buildComposer(context) {
    var username = Provider.of<LoginHelper>(context).user;
//    void _sendtypingstatus() {
//      _socketIO.emit('typingNewFunction', [
//        json.encode({
//          'userName': widget.username,
//          'conversationId': widget.conversationid,
//        })
//      ]);
//    }

//    void _sendstoptypingstatus() {
//      _socketIO.emit('stopTyping', [
//        json.encode({
//          'userName': widget.username,
//          'conversationId': widget.conversationid,
//        })
//      ]);
//    }

    void _runTimer() {
      if (_typingTimer != null && _typingTimer.isActive) _typingTimer.cancel();

      _typingTimer = Timer(Duration(milliseconds: 600), () {
        if (!_isTyping) return;
        _isTyping = false;
//        _sendstoptypingstatus();
      });
      _isTyping = true;
//      _sendtypingstatus();
    }

    return Container(
      child: Row(
        children: <Widget>[
          Column(
            children: [
              CupertinoButton(
                  minSize: double.minPositive,
                  padding: EdgeInsets.zero,
                  child: Icon(Icons.camera_alt),
                  onPressed: () {
                    pickGallery();
                  }),
              // IconButton(
              //     icon: Icon(
              //       Icons.camera_alt,
              //       color: Colors.grey,
              //       size: 28,
              //     ),
              //     onPressed: () {
              //       pickGallery();
              //     }),
              InkWell(
                onTap: () {
                  FocusScopeNode currentFocus = FocusScope.of(context);
                  if (!currentFocus.hasPrimaryFocus) {
                    currentFocus.unfocus();
                  }
                  setState(() {
                    isShowOffer = true;
                  });
                },
                child: Container(
                    padding: EdgeInsets.only(left: 2),
                    color: Colors.white,
                    child: Text(
                      'Make offer',
                      style: style.MontserratMedium(
                        fontSize: size.convert(context, 8),
                        color: color.gradientStartColor,
                      ),
                    )),
              ),
            ],
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 6.0),
                decoration: BoxDecoration(
                  border: Border.all(color: Color(0xff69bef7)),
                  borderRadius: BorderRadius.circular(30.0),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        enabled: isloading,
                        minLines: 1,
                        maxLines: 4,
                        controller: _messagecontroller,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          hintText: 'Write your message...',
                          hintStyle: TextStyle(
                            fontFamily: 'MontserratRegular',
                            fontSize: (15 / 8.148314082864863) *
                                SizeConfig.textMultiplier,
                          ),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                    CircleAvatar(
                      backgroundColor: Color(0xff7595f0),
                      radius: size.convert(context, 20),
                      child: IconButton(
                          icon: Icon(
                            Icons.send,
                            size: (28 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            if (image == null &&
                                _messagecontroller.text.isEmpty) {
                              return;
                            } else {
                              singlemessage(
                                createdby: widget.userid,
                                createdfor: widget.createdforid,
                                categoryid: widget.categoryid,
                                subcategoryid: widget.subcategoryid,
                                message: _messagecontroller.text ?? "",
                                context: context,
                                image: image,
                                offer: "false",
                              ).then((d) {
                                getconversation(widget.conversationid,
                                        widget.userid, context)
                                    .then((value) {
                                  unreadsBox.put(widget.conversationid, value);
                                  Provider.of<NewMessagesHlper>(context,
                                          listen: false)
                                      .setNewMessages(value);
                                  setState(() {
                                    conversation_id = widget.conversationid;
                                    newmessageN = value;
                                    isloading = true;
                                  });
                                });
                              });
                              _messagecontroller.clear();
                              setState(() {
                                sendimage = null;
                                image = null;
                              });
                            }
                            if (status == "Pending" ||
                                status == "Rejected" &&
                                    widget.createdbyid != username.sId) {
                              setState(() {
                                conversationstatus(
                                    widget.conversationid.toString(),
                                    "Accepted",
                                    context);
                                status = "Accepted";
                              });
                            }
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var userid = Provider.of<LoginHelper>(context).user;
    var isShowDrop = Provider.of<LoginHelper>(context);
    var newmessage = Provider.of<NewMessagesHlper>(context).newmessages;
    return OrientationBuilder(
      builder: (context, orientation) {
        return WillPopScope(
          onWillPop: () async {
            conversation_id = "";
            Navigator.of(context)
                .pop(status == null ? status = "Pending" : status);
            if (isShowDrop.isshowDropdown) {
              isShowDrop.setIsShowDropDown(false);
            }
            return Future.value(false);
          },
          child: Scaffold(
            key: _scaffoldKey,
            body: SafeArea(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    isShowOffer = false;
                  });
                  isShowDrop.setIsShowDropDown(false);
                },
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          color: Colors.white,
                          height: 10 * SizeConfig.heightMultiplier,
                          width: double.infinity,
                          child: Row(
                            children: [
                              Row(
                                children: [
                                  SizedBox(
                                    width: 4.0,
                                  ),
                                  CupertinoButton(
                                    minSize: double.minPositive,
                                    padding:
                                        EdgeInsets.only(right: 3.0, left: 3.0),
                                    onPressed: () {
                                      conversation_id = "";
                                      Navigator.of(context).pop(status == null
                                          ? status = "Pending"
                                          : status);
                                      if (isShowDrop.isshowDropdown) {
                                        isShowDrop.setIsShowDropDown(false);
                                      }
                                    },
                                    child: Icon(Icons.arrow_back,
                                        color: color.buttonColor),
                                  ),
                                  SizedBox(
                                    width: 4.0,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                      Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  GetAllProducts(
                                                    username: "chatuser",
                                                    userid:
                                                        widget.createdforid ??
                                                            "",
                                                    city: widget.city ?? "",
                                                    country:
                                                        widget.country ?? "",
                                                    description:
                                                        widget.description ??
                                                            "",
                                                    organizationname: widget
                                                            .organizationname ??
                                                        "",
                                                    profilepic:
                                                        widget.image ?? "",
                                                    subscribtion: true,
                                                  )));
                                    },
                                    child: CircleAvatar(
                                      backgroundImage:
                                          CachedNetworkImageProvider(
                                              widget.image),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: InkWell(
                                  onTap: () {
                                    FocusScope.of(context)
                                        .requestFocus(new FocusNode());
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                GetAllProducts(
                                                  username: "chatuser",
                                                  userid: widget.createdforid ??
                                                      "d",
                                                  city: widget.city ?? "dd",
                                                  country:
                                                      widget.country ?? "dd",
                                                  description:
                                                      widget.description ??
                                                          "dd",
                                                  organizationname:
                                                      widget.organizationname ??
                                                          "dd",
                                                  profilepic:
                                                      widget.image ?? "dd",
                                                  subscribtion: true,
                                                  roleName:
                                                      widget.rolename ?? "dd",
                                                )));
                                  },
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        widget.messageusername,
                                        style: TextStyle(
                                          color: Color(0xff394c81),
                                          fontFamily: 'MontserratMedium',
                                          fontSize:
                                              2.5 * SizeConfig.textMultiplier,
                                        ),
                                      ),
                                      _isTyping == false
                                          ? Text(
                                              widget.onlinestatus,
                                              style: TextStyle(
                                                  fontSize:
                                                      (14 / 8.148314082864863) *
                                                          SizeConfig
                                                              .textMultiplier),
                                            )
                                          : Visibility(
                                              visible: _isTyping,
                                              child: _userNameTyping == null
                                                  ? Text(
                                                      _userNameTyping,
                                                      style: TextStyle(
                                                          fontFamily:
                                                              'MontserratRegular',
                                                          fontSize: (14 /
                                                                  8.148314082864863) *
                                                              SizeConfig
                                                                  .textMultiplier),
                                                    )
                                                  : Text(
                                                      "$_userNameTyping" +
                                                          "...",
                                                      style: TextStyle(
                                                          fontSize: (14 /
                                                                  8.148314082864863) *
                                                              SizeConfig
                                                                  .textMultiplier),
                                                    ),
                                            ),
                                    ],
                                  ),
                                ),
                              ),
                              idsfordeleting.length != 0
                                  ? Expanded(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "${idsfordeleting.length} Selected",
                                            style: TextStyle(
                                                color: color.buttonColor,
                                                fontSize: (20 /
                                                        8.148314082864863) *
                                                    SizeConfig.textMultiplier),
                                          ),
                                          IconButton(
                                            icon: Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 8.0),
                                              child: Icon(
                                                Icons.delete,
                                                color: color.buttonColor,
                                              ),
                                            ),
                                            onPressed: () {
                                              showdialogfordeleting(
                                                  context: context,
                                                  onPressed: () {
                                                    deleteMessagebySelecting(
                                                            userid.sId,
                                                            idsfordeleting,
                                                            context)
                                                        .then((value) {
                                                      setState(() {
                                                        idsfordeleting.clear();
                                                        getconversation(
                                                                widget
                                                                    .conversationid,
                                                                widget.userid,
                                                                context)
                                                            .then((value) {
                                                          unreadsBox.put(
                                                              widget
                                                                  .conversationid,
                                                              value);
                                                          Provider.of<NewMessagesHlper>(
                                                                  context,
                                                                  listen: false)
                                                              .setNewMessages(
                                                                  value);
                                                        });
                                                      });
                                                    });
                                                  },
                                                  title: "Delete message!",
                                                  text:
                                                      "Are you sure to delete message ?");
                                            },
                                            alignment: Alignment.centerRight,
                                          ),
                                        ],
                                      ),
                                    )
                                  : Expanded(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Container(
                                              child: IconButton(
                                                  onPressed: () {
                                                    // newmessage[index].
                                                    isShowDrop.setIsShowDropDown(
                                                        !isShowDrop
                                                            .isshowDropdown);
                                                  },
                                                  icon: Icon(
                                                    Icons.more_vert,
                                                    color: Colors.grey,
                                                  ))),
                                        ],
                                      ),
                                    ),
                            ],
                          ),
                        ),
                        Divider(
                          height: 0,
                        ),
                        Expanded(
                          child: status == "Pending" &&
                                  widget.createdbyid != userid.sId
                              ? buildMessagebeforestatus(orientation, context)
                              : status == "Rejected" &&
                                      widget.createdbyid != userid.sId
                                  ? buildMessagebeforestatus(
                                      orientation, context)
                                  : messagedata(context),
                        ),
                        sendimage == null
                            ? Container()
                            : orientation == Orientation.landscape
                                ? Container()
                                : Container(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          CupertinoButton(
                                            minSize: double.minPositive,
                                            padding: EdgeInsets.only(
                                                right: 3.0, left: 3.0),
                                            onPressed: () {
                                              setState(() {
                                                sendimage = null;
                                                image = null;
                                              });
                                            },
                                            child: Icon(
                                              Icons.cancel,
                                              color: Colors.black,
                                            ),
                                          ),
                                          SingleChildScrollView(
                                            child: Container(
                                              child: Image.file(sendimage,
                                                  fit: BoxFit.fill),
                                              width: 30 *
                                                  SizeConfig.widthMultiplier,
                                              height: 24 *
                                                  SizeConfig.heightMultiplier,
// child: Text("hello"),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                        isloading
                            ? newmessage[0]?.conversation?.conBlock == false
                                ? buildComposer(context)
                                : Container(
                                    color: Colors.red,
                                    width: double.infinity,
                                    height: 7 * SizeConfig.heightMultiplier,
                                    child: Center(
                                        child: newmessage == null
                                            ? Container()
                                            : Text(
                                                newmessage[0]
                                                            ?.conversation
                                                            ?.blockedBy ==
                                                        userid.sId
                                                    ? "You block ${widget.messageusername} click menu to unblock"
                                                    : "you have been block by ${widget.messageusername}",
                                                style: TextStyle(
                                                  color: Colors.white,
                                                ),
                                              )),
                                  )
                            : buildComposer(context),
                      ],
                    ),
                    isShowDrop.isshowDropdown
                        ? _dropDownContainer()
                        : Container(),
                    isShowOffer
                        ? Align(
                            alignment: Alignment.bottomCenter,
                            child: make_an_offer())
                        : Container(),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  _dropDownContainer() {
    var newmessage = Provider.of<NewMessagesHlper>(context).newmessages;
    return Positioned(
      right: 28,
      top: 35,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(offset: Offset(3, 3), color: Colors.grey, blurRadius: 5),
          ],
        ),
        width: size.convertWidth(context, 150),
        height: size.convert(context, 100),
        child: Center(
          child: Column(
            children: [
              Divider(
                height: 1,
              ),
              _DropDownContainer(
                  text: newmessage[0].conversation.blockedBy == widget.userid &&
                          newmessage[0].conversation.conBlock
                      ? "unblock"
                      : newmessage[0].conversation.blockedBy != widget.userid &&
                              newmessage[0].conversation.conBlock
                          ? 'you are blocked'
                          : 'Block',
                  onTap: () {
                    if (newmessage[0].conversation.blockedBy != widget.userid &&
                        newmessage[0].conversation.conBlock) {
                      return;
                    } else {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return StatefulBuilder(builder: (context, setStates) {
                            return Stack(
                              children: [
                                Dialog(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: SingleChildScrollView(
                                    child: _BLockAndAllowContainer(
                                        mainTitle:
                                            newmessage[0].conversation.conBlock
                                                ? 'Unblock User'
                                                : 'Block User',
                                        icon: Icon(
                                          Icons.block,
                                          color: Colors.red,
                                          size: size.convert(context, 50),
                                        ),
                                        functionOne: () {
                                          setStates(() {
                                            isshowLoadingforblockUser = true;
                                          });
                                          blockUserinChatConversation(
                                            userId: widget.userid,
                                            conversationId:
                                                widget.conversationid,
                                            userTwo: widget.createdforid,
                                          ).then((value) {
                                            getconversation(
                                                    widget.conversationid,
                                                    widget.userid,
                                                    context)
                                                .then((value) {
                                              unreadsBox.put(
                                                  widget.conversationid, value);
                                              Provider.of<NewMessagesHlper>(
                                                      context,
                                                      listen: false)
                                                  .setNewMessages(value);
                                              isshowLoadingforblockUser = false;
                                              Navigator.of(context).pop();
                                            });
                                          });
                                        },
                                        functionTwo: () {
                                          Navigator.of(context).pop();
                                        },
                                        imageCircle: null,
                                        title: newmessage[0]
                                                .conversation
                                                .conBlock
                                            ? 'Are you confirm to unblock the user ?'
                                            : 'Are you confirm to block the user ?',
                                        text2: 'Cancel',
                                        text1:
                                            newmessage[0].conversation.conBlock
                                                ? 'UNBLOCK'
                                                : 'BLOCK'),
                                  ),
                                ),
                                isshowLoadingforblockUser
                                    ? stack()
                                    : Container(),
                              ],
                            );
                          });
                        },
                      );
                    }
                  }),
              Divider(
                height: 1,
              ),
              _DropDownContainer(
                  text: 'Report User',
                  onTap: () {
                    _ReportContainer();
                  }),
              Divider(
                height: 1,
              ),
              _DropDownContainer(
                  text: 'Safety Tips',
                  onTap: () {
                    _SafetyTipsContainer();
                  }),
            ],
          ),
        ),
      ),
    );
  }

  _DropDownContainer({Function onTap, String text}) {
    var isShowDrop = Provider.of<LoginHelper>(context);
    return GestureDetector(
      onTap: () {
        onTap();
        isShowDrop.setIsShowDropDown(false);
      },
      child: Container(
        color: Colors.white,
        height: size.convert(context, 30.5),
        width: size.convertWidth(context, 150),
        child: Center(
          child: Text(
            text ?? "",
            style: style.MontserratMedium(fontSize: size.convert(context, 12)),
          ),
        ),
      ),
    );
  }

  _ReportContainer() {
    return showDialog(
        context: context,
        builder: (context) {
          return StatefulBuilder(builder: (context, setStated) {
            return Stack(
              children: [
                Dialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.error_outline,
                            size: size.convert(context, 50),
                            color: Colors.red,
                          ),
                          Text("Report User"),
                          Text(
                            "Please report user with your comment",
                            style: style.MontserratMedium(
                              color: color.footerfontcolor,
                            ),
                          ),
                          Center(
                            child: Container(
                              padding: EdgeInsets.zero,
                              width: size.convert(context, 250),
                              child: Row(
                                children: [
                                  Radio(
                                    groupValue: reportdata,
                                    value: Reportdata.fraud,
                                    onChanged: (Reportdata val) {
                                      setStated(() {
                                        _reportUserMessage = "Fraud";
                                        reportdata = val;
                                      });
                                    },
                                  ),
                                  Text(
                                    "Fraud",
                                    style: style.MontserratMedium(
                                      fontSize: size.convert(context, 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Center(
                            child: Container(
                              width: size.convert(context, 250),
                              child: Row(
                                children: [
                                  Radio(
                                    groupValue: reportdata,
                                    value: Reportdata.illegalDeal,
                                    onChanged: (Reportdata val) {
                                      setStated(() {
                                        print(val);
                                        _reportUserMessage = "illegal Deal";
                                        reportdata = val;
                                      });
                                    },
                                  ),
                                  Text(
                                    "illegal Deal",
                                    style: style.MontserratMedium(
                                      fontSize: size.convert(context, 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Center(
                            child: Container(
                              width: size.convert(context, 250),
                              child: Row(
                                children: [
                                  Radio(
                                    groupValue: reportdata,
                                    value: Reportdata.offensiveContent,
                                    onChanged: (Reportdata val) {
                                      setStated(() {
                                        print(val);
                                        _reportUserMessage =
                                            "Offensive Content";
                                        reportdata = val;
                                      });
                                    },
                                  ),
                                  Text(
                                    "Offensive Content",
                                    style: style.MontserratMedium(
                                      fontSize: size.convert(context, 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Center(
                            child: Container(
                              width: size.convert(context, 250),
                              child: Row(
                                children: [
                                  Radio(
                                    groupValue: reportdata,
                                    value: Reportdata.PrivacyOffensive,
                                    onChanged: (Reportdata val) {
                                      setStated(() {
                                        print(val);
                                        reportdata = val;
                                      });
                                    },
                                  ),
                                  Text(
                                    "Privacy Offensive",
                                    style: style.MontserratMedium(
                                      fontSize: size.convert(context, 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Center(
                            child: Container(
                              width: size.convert(context, 250),
                              child: Row(
                                children: [
                                  Radio(
                                    groupValue: reportdata,
                                    value: Reportdata.other,
                                    onChanged: (Reportdata val) {
                                      setStated(() {
                                        _reportUserMessage = "Other";
                                        reportdata = val;
                                      });
                                    },
                                  ),
                                  Text(
                                    "Other",
                                    style: style.MontserratMedium(
                                      fontSize: size.convert(context, 14),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: size.convertWidth(context, 250),
                            height: size.convert(context, 100),
                            child: TextField(
                              onChanged: (value) {
                                _reportUserComments = value;
                              },
                              decoration: InputDecoration(
                                hintText: "Type your message here....",
                                hintStyle: style.MontserratRegular(
                                    fontSize: size.convert(context, 12)),
                                border: InputBorder.none,
                              ),
                            ),
                            padding: EdgeInsets.only(
                                left: 8.0, right: 8.0, bottom: 10.0),
                            decoration: BoxDecoration(
                                color: Color(0xffEbf3fa),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(25.0))),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              InkWell(
                                  child: Text(
                                    "REPORT",
                                    style: style.MontserratMedium(
                                        color: Colors.red,
                                        fontSize: size.convert(context, 14)),
                                  ),
                                  onTap: () {
                                    setStated(() {
                                      isshowLoadingforblockUser = true;
                                    });
                                    if (_reportUserComments != null) {
                                      reportUserInConversation(
                                        createdBy: widget.userid,
                                        reportedUser: widget.createdforid,
                                        reportOption: _reportUserMessage,
                                        comments: _reportUserComments,
                                      ).then((value) {
                                        isshowLoadingforblockUser = false;
                                        print(value['Success']);
                                        if (value["Success"] == true) {
                                          Navigator.pop(context);
                                          showFloatingFlushbarSuccess(
                                              _scaffoldKey,
                                              title:
                                                  "Report submitted successfully");
                                        } else {
                                          Navigator.pop(context);
                                          showFloatingFlushbarForError(
                                              _scaffoldKey,
                                              title: value['message']);
                                        }
                                        // ? showFloatingFlushbarSuccess(
                                        //     context: context,
                                        //     title:
                                        //         "Report Submitted Successfully")
                                        // : showFloatingFlushbarForError()
                                      });
                                    }
                                  }),
                              InkWell(
                                child: Text(
                                  "Cancel",
                                  style: style.MontserratMedium(
                                      fontSize: size.convert(context, 14)),
                                ),
                                onTap: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                isshowLoadingforblockUser ? stack() : Container(),
              ],
            );
          });
        });
  }

  Widget make_an_offer() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
          topRight: Radius.circular(40),
        ),
        boxShadow: [
          BoxShadow(
            color: color.gradientStartColor,
            spreadRadius: 2,
            blurRadius: 10,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
        color: Colors.white,
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 15,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 5,
              ),
              SvgPicture.asset(
                "assets/images/offer.svg",
                height: size.convert(context, 70),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 5,
                ),
                child: Text(
                  'Make an offer',
                  style: TextStyle(fontSize: 22),
                ),
              ),
              Text(
                'Make an offer to exchange your product',
                style: TextStyle(fontSize: 9),
              ),
              SizedBox(
                height: 3,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Send offer for the proposed Price *',
                    style: TextStyle(fontSize: 14),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 10,
                      horizontal: 10,
                    ),
                    child: CustomTextFieldSecond(
                      hints: '250,000 Rs',
                      //maxLength: 70,
                      textInputType: TextInputType.number,
                      textEditingController: proposed_price,
                    ),
                  ),
                  Text(
                    'Description',
                    style: TextStyle(fontSize: 14),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 10,
                      horizontal: 10,
                    ),
                    child: CustomTextFieldSecond(
                      hints: 'Optional Description',
                      //maxLength: 70,
                      textInputType: TextInputType.text,
                      textEditingController: other_description,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 5,
                ),
                child: filledButton(
                  txt: "Send Offer",
                  onTap: () {
                    if (proposed_price.text.isEmpty &&
                        other_description.text.isEmpty) {
                      return;
                    } else {
                      singlemessage(
                        createdby: widget.userid,
                        createdfor: widget.createdforid,
                        categoryid: widget.categoryid,
                        subcategoryid: widget.subcategoryid,
                        message: "Price: ${proposed_price.text}\n" +
                            "${other_description.text}",
                        context: context,
                        image: image,
                        offer: "true",
                      ).then((d) {
                        getconversation(
                                widget.conversationid, widget.userid, context)
                            .then((value) {
                          unreadsBox.put(widget.conversationid, value);
                          Provider.of<NewMessagesHlper>(context, listen: false)
                              .setNewMessages(value);
                          setState(() {
                            conversation_id = widget.conversationid;
                            newmessageN = value;
                            isloading = true;
                          });
                        });
                        setState(() {
                          isShowOffer = false;
                        });
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _SafetyTipsContainer() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            insetPadding: EdgeInsets.all(20),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            child: SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 8,
                    ),
                    SvgPicture.asset(
                      "assets/images/shield.svg",
                      height: size.convert(context, 50),
                    ),
                    Text(
                      'Safety Tips',
                      style: style.MontserratMedium(
                          color: Colors.black,
                          fontSize: size.convert(context, 15)),
                    ),
                    Text(
                      'We Strong Recommend you to follow these tips',
                      style: style.MontserratMedium(
                          color: color.borderColor,
                          fontSize: size.convert(context, 12)),
                    ),
                    Column(
                      children: [
                        SafetyTipsContainer(
                          image: "assets/images/insurance.svg",
                          text:
                              "Ways to use your Google Account Storage Back up your phone, Upload photos, and more",
                        ),
                        SafetyTipsContainer(
                          image: "assets/images/satisfaction.svg",
                          text:
                              "Ways to use your Google Account Storage Back up your phone, Upload photos, and more",
                        ),
                        SafetyTipsContainer(
                          image: "assets/images/trust.svg",
                          text:
                              "Ways to use your Google Account Storage Back up your phone, Upload photos, and more",
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Colors.white,
                            ),
                            alignment: Alignment.centerRight,
                            padding: EdgeInsets.symmetric(
                                vertical: 20, horizontal: 15),
                            child: Text(
                              "Cancel",
                              style: style.MontserratMedium(
                                  color: color.gradientEndColor,
                                  fontSize: size.convert(context, 14)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                width: 500,
              ),
            ),
          );
        });
  }

  SafetyTipsContainer({String image, String text}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      padding: EdgeInsets.all(10.0),
      height: 60,
      width: 400,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 3),
            color: Colors.grey.withOpacity(0.3),
            blurRadius: 6,
          )
        ],
      ),
      child: Row(
        children: [
          SvgPicture.asset(
            "$image",
            height: size.convert(context, 40),
          ),
          Flexible(
            fit: FlexFit.loose,
            child: Container(
              margin: EdgeInsets.all(2),
              child: RichText(
                text: TextSpan(
                  text: '$text',
                  style: style.MontserratMedium(color: color.borderColor),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _BLockAndAllowContainer(
      {String mainTitle,
      String title,
      String text1,
      String text2,
      String imageCircle,
      Icon icon,
      Function functionOne,
      bool isRejected = false,
      Function functionTwo}) {
    var orientation = MediaQuery.of(context).orientation;
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                offset: Offset(-3, -3),
                color: color.buttonColor.withOpacity(0.3),
                blurRadius: 5),
            BoxShadow(
                offset: Offset(3, 3),
                color: color.buttonColor.withOpacity(0.3),
                blurRadius: 5),
          ],
          color: Colors.white,
        ),
        width: orientation == Orientation.portrait
            ? 80 * SizeConfig.widthMultiplier
            : 140 * SizeConfig.widthMultiplier,
        child: Column(
          children: [
            SizedBox(
              height: orientation == Orientation.portrait
                  ? 0.2 * SizeConfig.heightMultiplier
                  : 0.5 * SizeConfig.heightMultiplier,
            ),
            CircleAvatar(
                backgroundColor: Colors.white,
                radius: orientation == Orientation.portrait
                    ? 5 * SizeConfig.heightMultiplier
                    : 3 * SizeConfig.heightMultiplier,
                child: imageCircle == null ? icon : Container(),
                backgroundImage: imageCircle == null
                    ? null
                    : CachedNetworkImageProvider("$imageCircle")),
            Text("$mainTitle"),
            Text(
              "$title",
              style: style.MontserratMedium(
                  color: Colors.grey, fontSize: size.convert(context, 10)),
            ),
            SizedBox(
              height: orientation == Orientation.portrait
                  ? 1 * SizeConfig.heightMultiplier
                  : 0.5 * SizeConfig.heightMultiplier,
            ),
            Row(
              mainAxisAlignment: isRejected
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  child: Text(
                    "$text1",
                    style: style.MontserratMedium(
                        color: Colors.red, fontSize: size.convert(context, 12)),
                  ),
                  onTap: () => functionOne(),
                ),
                InkWell(
                  child: Text(
                    "$text2",
                    style: style.MontserratMedium(
                        fontSize: size.convert(context, 12)),
                  ),
                  onTap: () {
                    functionTwo();
                  },
                ),
              ],
            ),
            SizedBox(
              height: orientation == Orientation.portrait
                  ? 1.8 * SizeConfig.heightMultiplier
                  : 1 * SizeConfig.heightMultiplier,
            ),
          ],
        ));
  }
}
