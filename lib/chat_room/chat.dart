// import 'package:biznes/chat_room/chat_inbox.dart';
// import 'package:biznes/chat_room/chat_inquiries.dart';
// import 'package:biznes/chat_room/inquires.dart';
// import 'package:biznes/helper/login_helper.dart';
// import 'package:flutter/material.dart';
// import 'package:biznes/size_config.dart';
// import 'package:provider/provider.dart';

// import 'chatTileCategories.dart';

// class ChatClass extends StatefulWidget {
//   @override
//   _ChatClassState createState() => _ChatClassState();
// }

// class _ChatClassState extends State<ChatClass> {
//   TabController tabController;
//   int length = 3;
//   int length2 = 2;
//   int index=0;
//   @override
//   Widget build(BuildContext context) {
//     List<Widget> childernforbroker = [
//       ChatGuest(),
//       ChatCategoriesTile(),
//       Inquires(),
//     ];
//     List<Widget> childern = [
//       ChatCategoriesTile(),
//       Inquires(),
//     ];
//     List<Widget> tabs1 = [
//       Tab(
//         child: Row(
//           children: <Widget>[
//             Icon(
//               Icons.email_outlined,
//               color: index==0?Colors.green:Colors.black
//             ),
//             SizedBox(
//               width: 10,
//             ),
//             Expanded(
//               child: Text(
//                 "Guest Message",
//                 style: TextStyle(
//                     fontSize:
//                         (16 / 8.148314082864863) * SizeConfig.heightMultiplier),
//               ),
//             ),
//           ],
//         ),
//       ),
//       Tab(
//         child: Row(
//           children: <Widget>[
//             Icon(
//               Icons.message,
//               color: index==1?Colors.green:Colors.black,
//             ),
//             SizedBox(
//               width: 10,
//             ),
//             Expanded(
//               child: Text(
//                 "Inbox",
//                 style: TextStyle(
//                     fontSize:
//                         (16 / 8.148314082864863) * SizeConfig.heightMultiplier),
//               ),
//             ),
//           ],
//         ),
//       ),
//       Tab(
//         child: Row(
//           children: <Widget>[
//             Icon(
//               Icons.check_circle_outline,
//               color: index==2?Colors.green:Colors.black,
//             ),
//             SizedBox(
//               width: 10,
//             ),
//             Expanded(
//               child: Text(
//                 "Sent Inquiries",
//                 style: TextStyle(
//                     fontSize:
//                         (16 / 8.148314082864863) * SizeConfig.heightMultiplier),
//               ),
//             ),
//           ],
//         ),
//       ),
//     ];
//     List<Widget> tab2 = [
//       InkWell(

//               child: Tab(
//           child: Row(
//             children: <Widget>[
//               Icon(
//                 Icons.inbox,
//                 color: Colors.black,
//               ),
//               SizedBox(
//                 width: 10,
//               ),
//               Expanded(
//                 child: Text(
//                   "Inbox",
//                   style: TextStyle(
//                       fontSize:
//                           (16 / 8.148314082864863) * SizeConfig.heightMultiplier),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//       Tab(
//         child: Row(
//           children: <Widget>[
//             Icon(
//               Icons.message,
//               color: Colors.black,
//             ),
//             SizedBox(
//               width: 10,
//             ),
//             Expanded(
//               child: Text(
//                 "Sent Inquiries",
//                 style: TextStyle(
//                     fontSize:
//                         (16 / 8.148314082864863) * SizeConfig.heightMultiplier),
//               ),
//             ),
//           ],
//         ),
//       ),
//     ];
//     var role = Provider.of<LoginHelper>(context).user.roleName;
    
//     return  DefaultTabController(
//         length: role == "Broker" ? length : length2,
//         child: Scaffold(
//             appBar: AppBar(
//               leading: Container(),
//               backgroundColor: Colors.white,
//               bottom: PreferredSize(
//                 preferredSize: Size(0,0),
//                           child: TabBar(
//                             onTap: (indexs){
//                              setState(() {
//                                index=indexs;
//                              });
//                             },
//                     indicatorColor: Colors.green,
//                     labelColor: Colors.black,
//                     controller: tabController,
//                     tabs: role == "Broker" ? tabs1 : tab2),
//               ),
//             ),
//             body:  TabBarView(
//                 controller: tabController,
//                 children: role == "Broker" ? childernforbroker : childern,
//               ),
//           ),
//     );
//   }
// }
