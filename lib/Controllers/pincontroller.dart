import 'package:biznes/Backendservices/api.dart';
import 'package:biznes/Backendservices/endpoints.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/product_UI/product_home.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

verifyPincode(String userid, String pincode, context) {
  var loginHelper = Provider.of<LoginHelper>(context, listen: false);
  loginHelper.setLoadingIndicator(true);
  if (pincode == null || pincode == "") {
    // return showFloatingFlushbarForError(
    //     context: context,
    //     title: "Warning",
    //     subtitle: "Please enter your pin-code");
  }
  Map verifyCode = {
    'userId': userid,
    'userPinCode': pincode,
  };
  BackEndApi.postFunction(url: verifiyCode, body: verifyCode).then((response) {
    if (response == "no_connection") {
      loginHelper.setLoadingIndicator(false);
      // showFloatingFlushbarForError(
      //     context: context,
      //     title: "No internet connection",
      //     subtitle: "Please check your internet connection");
    } else {
      print(response);
      if (response["Success"] == true) {
        loginHelper.setLoadingIndicator(false);
        Navigator.pushAndRemoveUntil(
            context,
            PageTransition(
              child: ProductHome(),
              type: PageTransitionType.fade,
              duration: Duration(milliseconds: 500),
            ),
            (route) => false);
      } else {
        loginHelper.setLoadingIndicator(false);
        // showFloatingFlushbarForError(
        //     context: context,
        //     title: "Warning",
        //     subtitle: "Please enter a valid pin-code");
      }
    }
  });
}
