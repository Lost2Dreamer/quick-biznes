import 'dart:convert';
import 'package:biznes/Backendservices/api.dart';
import 'package:biznes/Backendservices/endpoints.dart';
import 'package:biznes/model/membershitpdetailmodel.dart';
import 'package:biznes/model/searchmodel.dart';
import 'package:biznes/model/categoryinbox.dart';
import 'package:biznes/model/inboxlistmessages.dart';
import 'package:biznes/model/send_to_all_message_model.dart';
import 'package:biznes/model/single_Product.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

Future<ConversationModel> gettingData(
    String userid, String catId, String roleName) async {
  var response = await BackEndApi.get(
      url: "${allMessages + userid + "/" + catId + "/" + roleName}");
  print(catId);
  print(roleName);
  print(response);
  ConversationModel conversationModel = ConversationModel.fromJson(response);
  // List<AllMessages> allmessages = [];
  // response["AllMessages"].forEach((value) {
  //   allmessages.add(AllMessages.fromJson(value));
  // });
  return conversationModel;
}

Future<List<Membershipdetailmodel>> getmembershipdetails(String userid) async {
  String url = "$getCurrentUserMemberShip$userid";
  var request = await BackEndApi.get(url: url);
  print(request);
  List<Membershipdetailmodel> details;
  details =
      (request as List).map((i) => Membershipdetailmodel.fromJson(i)).toList();
  return details;
}

Future<CategoryInbox> getAllInboxMessagesByCategories(String userid) async {
  print(userid);
  var request = await BackEndApi.get(url: "${inboxByCategories + userid}");
  print(request);
  CategoryInbox inbox = CategoryInbox.fromJson(request);
  return inbox;
}

Future<List<ProductSearchModel>> searchProductFunction(
    {String categoryId,
    String subcategoryId,
    String countryId,
    String cityId,
    String roleId,
    String userId}) async {
  print("calling");
  Map searchMapData = {
    'category': categoryId,
    'subcategory': subcategoryId,
    'country': countryId,
    'city': cityId,
    'roleId': roleId,
    'userId': userId,
  };
  print(searchMapData);

  var response =
      await BackEndApi.postFunction(url: "$searchProduct", body: searchMapData);
  print("calling3");
  print(response);
  print(response['products']);
  List<ProductSearchModel> getProducts = [];

  print("calling4");
  response["products"].forEach((value) {
    getProducts.add(ProductSearchModel.fromJson(value));
  });
  print("dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
  print(response['products']);
  return getProducts;
}

Future<SingleProductdetails> getSingleProductDetails(
    {BuildContext context, String productId, String userid}) async {
  String url = '$singleProductDetails$productId/$userid';
  var response = await BackEndApi.get(url: url);
  SingleProductdetails singleProductdetails =
      SingleProductdetails.fromJson(response);
  return singleProductdetails;
}

Future<bool> sendToAllMessage(
    SendMessageToAllMessageModel sendMessageToAllMessage,
    String userid,
    context) async {
  String getAllSendMessage = '$messageSendToAll';
  FormData data = FormData.fromMap({
    'userId': userid,
    'categoryId': sendMessageToAllMessage.categoryId,
    'subCategoryId': sendMessageToAllMessage.subCategoryId,
    'message': sendMessageToAllMessage.message,
    'countryId': sendMessageToAllMessage.countryId,
    'cityId': sendMessageToAllMessage.cityId,
    'roleId': sendMessageToAllMessage.userRole,
  });
  if (sendMessageToAllMessage.image != null) {
    data.files.add(MapEntry('attachment',
        MultipartFile.fromFileSync(sendMessageToAllMessage.image)));
  }

  var response =
      await BackEndApi.postFunction(url: getAllSendMessage, body: data);
  var getData = response;
  print(getData);
  if (getData['success'] == false) {
    Toast.show(getData['message'].toString(), context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    return false;
  } else {
    Toast.show(getData['message'].toString(), context,
        duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    return true;
  }
}

Future<bool> singlemessage({
  String createdby,
  String createdfor,
  String categoryid,
  String subcategoryid,
  String message,
  String offer,
  context,
  image,
}) async {
  String sendSingleMessageUrl = "$sendSingleMessage";
  FormData formData = FormData.fromMap({
    "createdBy": createdby,
    "createdFor": createdfor,
    "categoryId": categoryid,
    "subCategoryId": subcategoryid,
    "offer": offer,
    "message": message ?? "",
    "offerPrice": "222",
    "offerDescription": "this is description"
  });
  if (image != null) {
    formData.files.add(MapEntry('image', MultipartFile.fromFileSync(image)));
  }
  var response =
      await BackEndApi.postFunction(url: sendSingleMessageUrl, body: formData);
  return response['success'];
}

Future reportUserInConversation(
    {String createdBy,
    String reportOption,
    String comments,
    String reportedUser}) async {
  String reportUrl = "$reportUserinConversation";
  Map data = {
    "createdBy": createdBy,
    "reportOption": reportOption,
    "comments": comments,
    "reportedUser": reportedUser,
  };
  var response = await BackEndApi.postFunction(url: reportUrl, body: data);
  print(response);
  return response;
}

Future blockUserinChatConversation({userId, conversationId, userTwo}) async {
  String url = "$blockUserInConversation";
  Map userData = {
    "userId": userId,
    "conversationId": conversationId,
    "userTwo": userTwo,
  };
  var response = await BackEndApi.postFunction(url: url, body: userData);
  print(response);
}

Future<Map> gettingratingsbyuserid([String userid]) async {
  String url = "$getUserRatingsbyId$userid";
  var response = await BackEndApi.get(url: url);
  return response;
}
