import 'package:hive/hive.dart';
part 'subCategoryModel.g.dart';
@HiveType(typeId: 2)
class SubCategories {
  @HiveField(0)
  String sId;
  @HiveField(1)
  String title;
  @HiveField(2)
  String category;
  @HiveField(3)
  String createdAt;
  @HiveField(4)
  String updatedAt;
  @HiveField(5)
  int iV;
  bool isSelected;

  SubCategories(
      {this.sId,
        this.title,
        this.category,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.isSelected
      });

  SubCategories.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    category = json['category'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['category'] = this.category;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}