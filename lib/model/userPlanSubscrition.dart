class UserPlanSubscription {
  String planId;
  String userId;
  String categoryId;
  List<String> subcategories;
  UserPlanSubscription({
    this.planId,
    this.userId,
    this.categoryId,
    this.subcategories,
  });
UserPlanSubscription.fromJson(Map<String, dynamic> json) {
 planId=json['planId'];
 userId=json['userId'];
 categoryId=json['categoryId'];
 subcategories=json['subcategories'].cast<String>();
}
Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
  data['planId']=this.planId;
  data['userId']=this.userId;
  data['categoryId']=this.categoryId;
  data['subcategories']=this.subcategories;
  return data;
}
}