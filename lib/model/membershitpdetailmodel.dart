class Membershipdetailmodel {
  String type;
  List<Subcategories> subcategories;
  String subscriptionDate;
  String sId;
  PlanId planId;
  String userId;
  CategoryId categoryId;
  String subscriptionExpiryDate;
  String createdAt;
  String updatedAt;
  int iV;

  Membershipdetailmodel(
      {this.type,
      this.subcategories,
      this.subscriptionDate,
      this.sId,
      this.planId,
      this.userId,
      this.categoryId,
      this.subscriptionExpiryDate,
      this.createdAt,
      this.updatedAt,
      this.iV});

  Membershipdetailmodel.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    if (json['subcategories'] != null) {
      subcategories = new List<Subcategories>();
      json['subcategories'].forEach((v) {
        subcategories.add(new Subcategories.fromJson(v));
      });
    }
    subscriptionDate = json['subscriptionDate'];
    sId = json['_id'];
    planId =
        json['planId'] != null ? new PlanId.fromJson(json['planId']) : null;
    userId = json['userId'];
    categoryId = json['categoryId'] != null
        ? new CategoryId.fromJson(json['categoryId'])
        : null;
    subscriptionExpiryDate = json['subscriptionExpiryDate'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    if (this.subcategories != null) {
      data['subcategories'] =
          this.subcategories.map((v) => v.toJson()).toList();
    }
    data['subscriptionDate'] = this.subscriptionDate;
    data['_id'] = this.sId;
    if (this.planId != null) {
      data['planId'] = this.planId.toJson();
    }
    data['userId'] = this.userId;
    if (this.categoryId != null) {
      data['categoryId'] = this.categoryId.toJson();
    }
    data['subscriptionExpiryDate'] = this.subscriptionExpiryDate;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class Subcategories {
  String sId;
  String title;
  String category;
  String createdAt;
  String updatedAt;
  int iV;

  Subcategories(
      {this.sId,
      this.title,
      this.category,
      this.createdAt,
      this.updatedAt,
      this.iV});

  Subcategories.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    category = json['category'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['category'] = this.category;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class PlanId {
  String type;
  String description;
  String sId;
  String title;
  int duration;
  int price;
  int totalParent;
  int totalSubcategory;
  String createdAt;
  String updatedAt;
  int iV;

  PlanId(
      {this.type,
      this.description,
      this.sId,
      this.title,
      this.duration,
      this.price,
      this.totalParent,
      this.totalSubcategory,
      this.createdAt,
      this.updatedAt,
      this.iV});

  PlanId.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    description = json['description'];
    sId = json['_id'];
    title = json['title'];
    duration = json['duration'];
    price = json['price'];
    totalParent = json['totalParent'];
    totalSubcategory = json['totalSubcategory'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['description'] = this.description;
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['duration'] = this.duration;
    data['price'] = this.price;
    data['totalParent'] = this.totalParent;
    data['totalSubcategory'] = this.totalSubcategory;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class CategoryId {
  String sId;
  String title;
  String createdAt;
  String updatedAt;
  int iV;

  CategoryId({this.sId, this.title, this.createdAt, this.updatedAt, this.iV});

  CategoryId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}