// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'subCategoryModel.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SubCategoriesAdapter extends TypeAdapter<SubCategories> {
  @override
  final int typeId = 2;

  @override
  SubCategories read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SubCategories(
      sId: fields[0] as String,
      title: fields[1] as String,
      category: fields[2] as String,
      createdAt: fields[3] as String,
      updatedAt: fields[4] as String,
      iV: fields[5] as int,
    );
  }

  @override
  void write(BinaryWriter writer, SubCategories obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.sId)
      ..writeByte(1)
      ..write(obj.title)
      ..writeByte(2)
      ..write(obj.category)
      ..writeByte(3)
      ..write(obj.createdAt)
      ..writeByte(4)
      ..write(obj.updatedAt)
      ..writeByte(5)
      ..write(obj.iV);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SubCategoriesAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
