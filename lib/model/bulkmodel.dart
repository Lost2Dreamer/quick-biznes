class Bulk {
  List<String> createdbyid;
  Bulk({this.createdbyid});
  Bulk.fromJson(Map<String ,dynamic> json){
  createdbyid=json['createdBy'].cast<String>();
  }
  Map<String,dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy']=this.createdbyid;
    return data;
  }
}