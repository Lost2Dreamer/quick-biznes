class CommentsReviews {
  String currentUserId;
  String productId;
  String comment;
  String rating;

  CommentsReviews(
      {this.currentUserId, this.productId, this.comment, this.rating});

  CommentsReviews.fromJson(Map<String, dynamic> data) {
    currentUserId = data['commentedBy'];
    comment = data['comments'];
    rating = data['ratings'];
    productId = data['productId'];
  }
}
