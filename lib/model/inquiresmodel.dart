class AllInquiries {
  bool success;
  List<AllInquiriesSend> allInquiriesSend;
  List<AllRecieve> allRecieve;

  AllInquiries({this.success, this.allInquiriesSend, this.allRecieve});

  AllInquiries.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    if (json['sendInquiriesArray'] != null) {
      allInquiriesSend = new List<AllInquiriesSend>();
      json['sendInquiriesArray'].forEach((v) {
        allInquiriesSend.add(new AllInquiriesSend.fromJson(v));
      });
    }
    if (json['allRecieve'] != null) {
      allRecieve = new List<AllRecieve>();
      json['allRecieve'].forEach((v) {
        allRecieve.add(new AllRecieve.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.allInquiriesSend != null) {
      data['sendInquiriesArray'] =
          this.allInquiriesSend.map((v) => v.toJson()).toList();
    }
    if (this.allRecieve != null) {
      data['allRecieve'] = this.allRecieve.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AllInquiriesSend {
  String category;
  int pendingMessages;
  int acceptedMessages;
  int rejectedMessages;
  String categoryId;
  AllInquiriesSend(
      {this.category,
      this.categoryId,
      this.pendingMessages,
      this.acceptedMessages,
      this.rejectedMessages});

  AllInquiriesSend.fromJson(Map<String, dynamic> json) {
    category = json['category'];
    categoryId = json['categoryId'];
    pendingMessages = json['pendingMessages'];
    acceptedMessages = json['acceptedMessages'];
    rejectedMessages = json['rejectedMessages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category'] = this.category;
    data['categoryId'] = this.categoryId;
    data['pendingMessages'] = this.pendingMessages;
    data['acceptedMessages'] = this.acceptedMessages;
    data['rejectedMessages'] = this.rejectedMessages;
    return data;
  }
}

class AllRecieve {
  String category;
  String categoryId;
  int pendingMessages;
  int acceptedMessages;
  int rejectedMessages;

  AllRecieve(
      {this.category,
      this.categoryId,
      this.pendingMessages,
      this.acceptedMessages,
      this.rejectedMessages});

  AllRecieve.fromJson(Map<String, dynamic> json) {
    category = json['category'];
    categoryId = json['categoryId'];
    pendingMessages = json['pendingMessages'];
    acceptedMessages = json['acceptedMessages'];
    rejectedMessages = json['rejectedMessages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category'] = this.category;
    data['categoryId'] = this.categoryId;
    data['pendingMessages'] = this.pendingMessages;
    data['acceptedMessages'] = this.acceptedMessages;
    data['rejectedMessages'] = this.rejectedMessages;
    return data;
  }
}
