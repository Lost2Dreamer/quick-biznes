class RoleModel {
  String id;
  String title;
  bool selected = false;
  RoleModel(this.id, this.title, this.selected);
  RoleModel.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    title = json['title'];
  }
}
