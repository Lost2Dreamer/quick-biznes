class ProductSearchModel {
  String sId;
  String importer = "importer";
  String exporter = "exporter";
  String title;
  int moq;
  String description;
  String organizationName;
  String price;
  int quantity;
  String quantityUnit;
  List<String> images;
  String createdAt;
  String country;
  String city;
  String category;
  String categoryId;
  String subCategory;
  String subCategoryId;
  String createdBy;
  String createdById;
  List<UserRole> userRole;
  String createdByDescription;
  int ratings;
  bool subscriptionStatus;
  bool isselected = false;
  ProductSearchModel(
      {this.sId,
      this.title,
      this.moq,
      this.exporter,
      this.quantityUnit,
      this.importer,
      this.description,
      this.organizationName,
      this.price,
      this.quantity,
      this.images,
      this.createdAt,
      this.country,
      this.city,
      this.category,
      this.categoryId,
      this.subCategory,
      this.subCategoryId,
      this.createdBy,
      this.createdById,
      this.userRole,
      this.createdByDescription,
      this.ratings,
      this.isselected,
      this.subscriptionStatus});

  ProductSearchModel.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    moq = json['moq'];
    description = json['description'];
    organizationName = json['organizationName'];
    price = json['price'];
    quantity = json['quantity'];
    quantityUnit = json['quantityUnit'];
    images = json['images'].cast<String>();
    createdAt = json['createdAt'];
    country = json['country'];
    city = json['city'];
    category = json['category'];
    categoryId = json['categoryId'];
    subCategory = json['subCategory'];
    subCategoryId = json['subCategoryId'];
    createdBy = json['createdBy'];
    createdById = json['createdById'];
    if (json['userRole'] != null) {
      userRole = new List<UserRole>();
      json['userRole'].forEach((v) {
        userRole.add(new UserRole.fromJson(v));
      });
    }
    createdByDescription = json['createdByDescription'];
    ratings = json['ratings'];
    subscriptionStatus = json['subscriptionStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['moq'] = this.moq;
    data['description'] = this.description;
    data['quantityUnit'] = this.quantityUnit;
    data['organizationName'] = this.organizationName;
    data['price'] = this.price;
    data['quantity'] = this.quantity;
    data['images'] = this.images;
    data['createdAt'] = this.createdAt;
    data['country'] = this.country;
    data['city'] = this.city;
    data['category'] = this.category;
    data['categoryId'] = this.categoryId;
    data['subCategory'] = this.subCategory;
    data['subCategoryId'] = this.subCategoryId;
    data['createdBy'] = this.createdBy;
    data['createdById'] = this.createdById;
    if (this.userRole != null) {
      data['userRole'] = this.userRole.map((v) => v.toJson()).toList();
    }
    data['createdByDescription'] = this.createdByDescription;
    data['ratings'] = this.ratings;
    data['subscriptionStatus'] = this.subscriptionStatus;
    return data;
  }
}

class UserRole {
  String sId;
  String title;
  String createdAt;
  String updatedAt;
  int iV;

  UserRole({this.sId, this.title, this.createdAt, this.updatedAt, this.iV});

  UserRole.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}
