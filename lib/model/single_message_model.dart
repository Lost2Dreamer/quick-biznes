class SingleMessageModel{
  String categoryId;
  String subCategoryId;
  String image;
  String message;
  String createdBy;
  String createdFor;

  SingleMessageModel({this.categoryId, this.subCategoryId, this.image,
    this.message, this.createdBy, this.createdFor});


}