class RegisterModel {
  String fullName;
  String username;
  String organizationName;
  String description;
  String email;
  int contactNumber;
  String countryCode;
  String countryId;
  String cityId;
  String roleName;
  String roleidforbroker;
  String password;
  List<String> role;
  String cnicCopy;
  String homeBill;
  String mobileToken;
  int userPinCode;
  RegisterModel(
      {this.fullName,
      this.username,
      this.organizationName,
      this.description,
      this.email,
      this.contactNumber,
      this.countryId,
      this.cityId,
      this.roleName,
      this.roleidforbroker,
      this.password,
      this.role,
        this.countryCode,
      this.cnicCopy,
      this.homeBill,
      this.mobileToken,
      this.userPinCode,
      });

  RegisterModel.fromJson(Map<String, dynamic> json) {
    fullName = json['fullName'];
    username = json['username'];
    organizationName = json['organizationName'];
    organizationName = json['description'];
    email = json['email'];
    contactNumber = json['contactNumber'];
    countryId = json['countryId'];
    cityId = json['cityId'];
    roleName = json['roleName'];
    password = json['password'];
    role = json['role'].cast<String>();
    cnicCopy=json['cnicCopy'];
    homeBill=json['homeBill'];
    mobileToken=json['mobileToken'];
    userPinCode=json['userPinCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fullName'] = this.fullName;
    data['username'] = this.username;
    data['organizationName'] = this.organizationName;
    data['description'] = this.organizationName;
    data['email'] = this.email;
    data['contactNumber'] = this.contactNumber;
    data['countryId'] = this.countryId;
    data['cityId'] = this.cityId;
    data['roleName'] = this.roleName;
    data['password'] = this.password;
    data['role'] = this.role;
    data['mobileToken']=this.mobileToken;
    data['userPinCode']=this.userPinCode;
    return data;
  }
}
