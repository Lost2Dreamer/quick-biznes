class Banners {
  String sId;
  String title;
  String banner;
  Banners({this.sId, this.title, this.banner});

  Banners.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    banner = json['banner'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['banner'] = this.banner;
    return data;
  }
}