class ConversationModel {
  bool success;
  List<AllMessages> allMessages;

  ConversationModel({this.success, this.allMessages});

  ConversationModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    if (json['AllMessages'] != null) {
      allMessages = new List<AllMessages>();
      json['AllMessages'].forEach((v) {
        allMessages.add(new AllMessages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.allMessages != null) {
      data['AllMessages'] = this.allMessages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AllMessages {
  String status;
  String conversation;
  String conversationCreatedBy;
  String message;
  CreatedBy createdBy;
  CreatedBy createdFor;
  CountryId subCategoryId;
  CountryId categoryId;
  int unRead;
  String createdAt;

  AllMessages(
      {this.status,
      this.conversation,
      this.conversationCreatedBy,
      this.message,
      this.createdBy,
      this.createdFor,
      this.subCategoryId,
      this.categoryId,
      this.unRead,
      this.createdAt});

  AllMessages.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    conversation = json['conversation'];
    conversationCreatedBy = json['conversationCreatedBy'];
    message = json['message'];
    createdBy = json['createdBy'] != null
        ? new CreatedBy.fromJson(json['createdBy'])
        : null;
    createdFor = json['createdFor'] != null
        ? new CreatedBy.fromJson(json['createdFor'])
        : null;
    subCategoryId = json['subCategoryId'] != null
        ? new CountryId.fromJson(json['subCategoryId'])
        : null;
    categoryId = json['categoryId'] != null
        ? new CountryId.fromJson(json['categoryId'])
        : null;
    unRead = json['unRead'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['conversation'] = this.conversation;
    data['conversationCreatedBy'] = this.conversationCreatedBy;
    data['message'] = this.message;
    if (this.createdBy != null) {
      data['createdBy'] = this.createdBy.toJson();
    }
    if (this.createdFor != null) {
      data['createdFor'] = this.createdFor.toJson();
    }
    if (this.subCategoryId != null) {
      data['subCategoryId'] = this.subCategoryId.toJson();
    }
    if (this.categoryId != null) {
      data['categoryId'] = this.categoryId.toJson();
    }
    data['unRead'] = this.unRead;
    data['createdAt'] = this.createdAt;
    return data;
  }
}

class CreatedBy {
  String description;
  bool onlineStatus;
  String profilePic;
  String sId;
  String fullName;
  List<RoleId> roleId;
  String username;
  String organizationName;
  String email;
  CountryId countryId;
  CountryId cityId;

  CreatedBy(
      {this.description,
      this.onlineStatus,
      this.profilePic,
      this.sId,
      this.fullName,
      this.roleId,
      this.username,
      this.organizationName,
      this.email,
      this.countryId,
      this.cityId});

  CreatedBy.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    if (json['roleId'] != null) {
      roleId = new List<RoleId>();
      json['roleId'].forEach((v) {
        roleId.add(new RoleId.fromJson(v));
      });
    }
    onlineStatus = json['onlineStatus'];
    profilePic = json['profilePic'];
    sId = json['_id'];
    fullName = json['fullName'];
    username = json['username'];
    organizationName = json['organizationName'];
    email = json['email'];
    countryId = json['countryId'] != null
        ? new CountryId.fromJson(json['countryId'])
        : null;
    cityId =
        json['cityId'] != null ? new CountryId.fromJson(json['cityId']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    if (this.roleId != null) {
      data['roleId'] = this.roleId.map((v) => v.toJson()).toList();
    }
    data['onlineStatus'] = this.onlineStatus;
    data['profilePic'] = this.profilePic;
    data['_id'] = this.sId;
    data['fullName'] = this.fullName;
    data['username'] = this.username;
    data['organizationName'] = this.organizationName;
    data['email'] = this.email;
    if (this.countryId != null) {
      data['countryId'] = this.countryId.toJson();
    }
    if (this.cityId != null) {
      data['cityId'] = this.cityId.toJson();
    }
    return data;
  }
}

class CountryId {
  String sId;
  String title;

  CountryId({this.sId, this.title});

  CountryId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    return data;
  }
}

class RoleId {
  String sId;
  String title;

  RoleId({this.sId, this.title});

  RoleId.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    return data;
  }
}
