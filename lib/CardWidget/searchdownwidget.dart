import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/color.dart' as font;

Widget SearchDown(
    {Function onTap,
    Function onchangedFirst,
    Function onChangedSecond,
    Function onTapSecond}) {
  return Column(
    children: <Widget>[
      Container(
          margin: EdgeInsets.only(left: 20, right: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(),
              InkWell(onTap: onTap, child: Icon(Icons.cancel_outlined)),
            ],
          )),
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "Price",
            style: TextStyle(
              fontFamily: font.fontfaimly,
              fontSize: 2 * SizeConfig.textMultiplier,
            ),
          ),
        ],
      ),
      SizedBox(
        height: 5,
      ),
      Material(
        shadowColor: Color(0xff69BEF7),
        elevation: 5,
        borderRadius: BorderRadius.circular(25),
        child: TextFormField(
          onChanged: onchangedFirst,
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25),
              borderSide: BorderSide(color: Color(0xff69BEF7)),
            ),
            focusColor: Color(0xff69BEF7),
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xffc7c7c7)),
              borderRadius: BorderRadius.circular(30.0),
            ),
            hintStyle: TextStyle(
              fontFamily: font.fontfaimlyregular,
              fontSize: 2 * SizeConfig.textMultiplier,
            ),
            hintText: 'Enter Your Price',
          ),
        ),
      ),
      SizedBox(
        height: 8,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            "M.O.Q",
            style: TextStyle(
              fontFamily: font.fontfaimly,
              fontSize: 2 * SizeConfig.textMultiplier,
            ),
          ),
        ],
      ),
      SizedBox(
        height: 8,
      ),
      Material(
        shadowColor: Color(0xff69BEF7),
        elevation: 5,
        borderRadius: BorderRadius.circular(25),
        child: TextFormField(
          onChanged: onChangedSecond,
          textInputAction: TextInputAction.done,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(25),
              borderSide: BorderSide(color: Color(0xff69BEF7)),
            ),
            focusColor: Color(0xff69BEF7),
            contentPadding:
                EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xffc7c7c7)),
              borderRadius: BorderRadius.circular(30.0),
            ),
            hintStyle: TextStyle(
              fontFamily: font.fontfaimlyregular,
              fontSize: 2 * SizeConfig.textMultiplier,
            ),
            hintText: 'Enter Your M.O.Q',
          ),
        ),
      ),
      SizedBox(
        height: 5.0,
      ),
      Padding(
        padding: const EdgeInsets.all(5.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            filledButton(
              txt: "SEARCH",
              onTap: onTapSecond,
            ),
          ],
        ),
      ),
    ],
  );
}
