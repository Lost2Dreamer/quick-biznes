import 'package:biznes/services/server.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:biznes/res/color.dart' as font;
import 'package:biznes/res/size.dart';
import '../size_config.dart';

Widget GridCardTile(
    {context,
    Function onTap,
    String image,
    String title,
    String price,
    String moq,
    String quantity,
    String ratings,
    String ratingsdata,
    List userRole,
    Widget checkbox}) {
  return Stack(
    children: [
      Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Color(0xfff8f8f8),
        ),
        child: GestureDetector(
          onTap: onTap,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: (170 / 8.148314082864863) * SizeConfig.heightMultiplier,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: image != null
                        ? CachedNetworkImage(
                            width: double.infinity,
                            imageUrl: '$base_url/${image}',
                            fit: BoxFit.cover,
                            placeholder: (context, url) =>
                                Center(child: CircularProgressIndicator()),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          )
                        : Container(),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "${title ?? "Title of Product"}",
                    maxLines: 2,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff394c81),
                      fontFamily: font.fontfaimlyquicksandbold,
                      fontWeight: FontWeight.bold,
                      fontSize: (14 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                  ),
                  SizedBox(
                    height:
                        (2 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "${ratings ?? "0"}.0",
                        style: TextStyle(
                          fontFamily: font.fontfaimly,
                          fontSize: (14 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                          color: Color(0xff707070),
                        ),
                      ),
                      Center(
                          child: SmoothStarRating(
                        isReadOnly: true,
                        borderColor: Color(0xffffb900),
                        rating: ratingsdata == null
                            ? 0.0
                            : double.parse(ratingsdata),
                        size: (20 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        color: Color(0xffffb900),
                        filledIconData: Icons.star,
                        defaultIconData: Icons.star_border,
                        starCount: 5,
                        allowHalfRating: true,
                        spacing: 0.5,
                      )),
                    ],
                  ),
                  Text("Price: ${price ?? "Not Avialable"}",
                      style: TextStyle(
                          fontFamily: font.fontfaimlyregular,
                          color: Colors.black,
                          fontSize: (14 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                          letterSpacing: .3)),
                  Text("Quantity: ${quantity ?? "Not Avialable"}",
                      style: TextStyle(
                          fontFamily: font.fontfaimlyregular,
                          color: Colors.black,
                          fontSize: (14 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                          letterSpacing: .3)),
                  Text("M.O.Q: ${moq ?? "Not Avialable"}",
                      style: TextStyle(
                          fontFamily: font.fontfaimlyregular,
                          color: Colors.black,
                          fontSize: (14 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                          letterSpacing: .3)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Pakistan",
                          style: TextStyle(
                              fontFamily: font.fontfaimly,
                              color: Colors.green,
                              fontSize: (14 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                              letterSpacing: .3)),
                      SizedBox(
                        width: (5 / 4.853932272197492) *
                            SizeConfig.widthMultiplier,
                      ),
                      Image.asset(
                        'assets/images/flag2.png',
                        height: 10,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // Text("Role: ",
                      //     overflow: TextOverflow.clip,
                      //     textAlign: TextAlign.left,
                      //     style: TextStyle(
                      //       letterSpacing: .3,
                      //       fontWeight: FontWeight.w400,
                      //       fontFamily: font.fontfaimly,
                      //       color: Color(0xff394C81),
                      //       fontSize: size.convert(context, 7),
                      //     )),
                      Wrap(
                        children: userRole
                            .map((role) => Text(
                                "${role.title.toString() ?? "Not Available"} ",
                                textAlign: TextAlign.left,
                                overflow: TextOverflow.clip,
                                style: TextStyle(
                                  letterSpacing: .3,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: font.fontfaimly,
                                  color: Color(0xff394C81),
                                  fontSize: size.convert(context, 7),
                                )))
                            .toList(),
                      ),
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
      Align(
        alignment: Alignment.topRight,
        child: Container(
          child: checkbox,
        ),
      ),
    ],
  );
}
