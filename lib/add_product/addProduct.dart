
import 'package:biznes/add_product/addProductNext.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/product_UI/bottomBar_UI/DropDownWidget.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/model/productModel.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/model/productsunits.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class AddProduct extends StatefulWidget {
  final String title;

  AddProduct(this.title);

  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  bool isdescription = false;
  String descriptionlength="0";
  bool isdescriptionlength=false;
  String selectedUnit;
  var formKey = GlobalKey<FormState>();
  ProductModel productModel;
  TextEditingController _description = TextEditingController();
  ProductUnits productUnits;
  @override
  initState() {
    productUnits=ProductUnits(units: []);
    getProductUnits(context).then((value) {
      setState(() {
        productUnits=value;
      });
    });
    productModel = ProductModel(
      title: '',
      quantityUnit:'',
      description: '',
      price: '',
      quantity: null,
      moq: null,
      category: '',
      subCategory: '',
      createdBy: '',
      images: [],
    );

    super.initState();
  }

  Widget buildForm() {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25.9),
          bottomRight: Radius.circular(25.9),
        )),
        child: Padding(
          padding: EdgeInsets.only(left: 16.2, right: 16.2),
          child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  TextFieldAndText(
                    hintText: "Enter your product name",
                    context: context,
                    onSaved: (value) {
                      productModel.title = value;
                    },
                    title: "Title of the Product",
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Enter title/brand name of your product";
                      } else if (value.length > 60) {
                        return "Length must be less than 60";
                      }
                      return null;
                    },
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                    child: Text(
                      "Description",
                      style: TextStyle(
                          fontFamily: fontandcolor.fontfaimly,
                          fontSize: (18.9 / 8.148314082864863) *
                              SizeConfig.textMultiplier),
                    ),
                  ),
                  Material(
                    shadowColor: Color(0xffd4d4d4),
                    elevation: 5,
                    borderRadius: BorderRadius.circular(30),
                    child: TextFormField(
                      controller: _description,
                      onChanged: (value){
                        setState(() {
                          descriptionlength=value.length.toString();
                        });
                      },
                      decoration: InputDecoration(
                        errorStyle: TextStyle(color: Colors.red),
                        errorBorder: outlineInputBorder(),
                        focusedErrorBorder: outlineInputBorder(),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                              color: isdescription
                                  ? Colors.red
                                  : Color(0xff69BEF7)),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                          borderSide: BorderSide(
                              color: isdescription ? Colors.red : Colors.grey,
                              width: 1.0),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        hintStyle: style.MontserratRegular(
                            fontSize: size.convert(context, 12)),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        hintText: 'Enter your product description',
                      ),
                      onSaved: (value) {
                        productModel.description = value;
                      },
                      maxLines: 5,
                    ),
                  ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                       isdescription
                           ? Padding(
                             padding: const EdgeInsets.all(8.0),
                             child: Text(
                         "Enter brief explaination about your product",
                         style:style.MontserratRegular(
                               color: Colors.red, fontSize: size.convert(context, 12)),
                       ),
                           ):isdescriptionlength? Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: Text(
                           "Description length must be 1000 words",
                           style:style.MontserratRegular(
                               color: Colors.red, fontSize: size.convert(context, 12)),
                         ),
                       ): Container(),
                       Padding(
                         padding: const EdgeInsets.all(8.0),
                         child: Text("$descriptionlength/1000",style: TextStyle(
                           color: Colors.grey,
                           fontSize:size.convert(context, 12),
                         ),),
                       ),
                     ],
                   ),
                  TextFieldAndText(
                   context: context,
                    inputtype: TextInputType.numberWithOptions(),
                    hintText: "Enter Product Price",
                    onSaved: (value) {
                      productModel.price = value;
                    },
                    title: "Price",
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Enter product price";
                      }
                      return null;
                    },
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: TextFieldAndText(
                          context: context,
                          inputtype: TextInputType.numberWithOptions(),
                          hintText: "Enter Quantity",
                          onSaved: (value) {
                            productModel.quantity = int.parse(value);
                          },
                          title: "Quantity",
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Enter product quantity";
                            }
                            return null;
                          },
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SizedBox(height: size.convert(context, 22),),
                            DropDownWidget(
                                image: "assets/images/units.svg",
                                context: context,
                                hintext: "Select QTY",
                                categories: productUnits.units,
                                value: selectedUnit,
                                selectedCategory: selectedUnit,
                                onChanged: (value){
                                  setState(() {
                                    selectedUnit=value;
                                    productModel.quantityUnit=value;
                                  });
                                }
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  TextFieldAndText(
                    context: context,
                    inputtype: TextInputType.numberWithOptions(),
                    hintText: "M.O.Q",
                    onSaved: (value) {
                      productModel.moq = int.parse(value);
                    },
                    title: "Minimum Order Quantity",
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Add minimum order quantity";
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                      height:
                          (10 / 8.148314082864863) * SizeConfig.textMultiplier),
                  Center(
                    child: filledButton(
                      onTap: () {
                        if (validateAndSave()) {
                          validateAndSubmit(context);
                        }
                      },
                      txt: "Next",
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              )),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            CustomAppBar(
              icon: Icon(Icons.arrow_back),
              IconPressed: () {
                Navigator.of(context).pop();
              },
              context: context,
              onTap: () {
                counterset.zerocunter();
                Navigator.of(context).push(PageTransition(
                    child: MyNotification(),
                    duration: Duration(milliseconds: 700),
                    type: PageTransitionType.leftToRightWithFade));
              },
              length: counters.toString(),
              text: "Add Product",
            ),
            Expanded(
              child: Container(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    SizedBox(
                      height: (20 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                    buildForm()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else if (_description.text.isEmpty) {
      setState(() {
        isdescription = true;
      });
    } else if(_description.text.length>1000){
      setState(() {
        isdescriptionlength=true;
      });
    }else{
      return false;
    }
  }

  validateAndSubmit(context) {
    if (validateAndSave()) {
      Navigator.push(
          context,
          PageTransition(
              type: PageTransitionType.bottomToTop,
              duration: Duration(milliseconds: 500),
              child: AddProductNext(
                title: widget.title,
                productModel: productModel,
              )));
    }
  }
}
