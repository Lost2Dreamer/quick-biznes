import 'dart:convert';
import 'dart:io';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/repeatedWigets/CustomTextField.dart';
import 'package:biznes/repeatedWigets/customTextField2.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:biznes/services/server.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

// import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:page_transition/page_transition.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class ContactUs extends StatefulWidget {
  final bool ispagecontact;

  ContactUs(this.ispagecontact);

  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  bool isDone = false;
  String userid_new = "";
  String fullName = "";
  String emailAddress = "";
  int contact = 0;
  String queryEmpty;
  String nameEmpty;
  String contactEmpty;
  String emailEmpty;
  String remarksEmpty;
  String imageEmpty;
  bool loading = false;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _fullNameController = TextEditingController();
  TextEditingController _contactController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _remarksController = TextEditingController();
  String selectedItem;
  TextEditingController _messagecontroller = TextEditingController();
  String ismessagelength = "0";
  bool messagelength = false;
  String errorMessage = 'No Error';
  String subject;
  String message;
  bool ismessageempty = false;
  List<Asset> images;
  List<Asset> resultList = List<Asset>();

  List<File> imagesFiles = [];
  List _query = [
    'Safety Issue',
    'Account Issue',
    'Fraud Victims',
    'Featured Ads',
    'General Information'
  ];

  int maxImageNo = 10;
  bool isUploading = false;
  String selectedCategory;
  String selectedSubCategory;
  bool selectSingleImage = false;

  reportPicture() async {
    setState(() {
      images = [];
      errorMessage = 'No Error';
    });
    String error;
    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: maxImageNo,
        enableCamera: true,
        selectedAssets: images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
      images = resultList;
      getImageFileFromAssets(resultList);
      // setState(() {});
    } on PlatformException catch (e) {
      error = e.message;
      print(error);
    }

    if (!mounted) return;
  }

  getImageFileFromAssets(List<Asset> assets) async {
    // print('assets length ${assets.length}');
    List<File> _files = [];
    assets.forEach((f) async {
      var byteData = await f.getByteData();
      final file = File('${(await getTemporaryDirectory()).path}/${f.name}');
      await file.writeAsBytes(byteData.buffer
          .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
      _files.add(file);
    });

    imagesFiles = _files;

    Future.delayed(Duration(milliseconds: 2000)).then((t) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user;
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    bool isvisible = Provider.of<LoginHelper>(context).isvisible;
    return Stack(
      children: [
        Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          body: Column(
            children: <Widget>[
              CustomAppBar(
                  icon: Icon(Icons.arrow_back),
                  IconPressed: () {
                    Navigator.of(context).pop();
                  },
                  context: context,
                  text: widget.ispagecontact ? "Customer Support" : "Report",
                  length: counters.toString(),
                  onTap: () {
                    counterset.zerocunter();
                    Navigator.of(context).push(PageTransition(
                        child: MyNotification(),
                        duration: Duration(milliseconds: 700),
                        type: PageTransitionType.leftToRightWithFade));
                  }),
              Expanded(
                child: Container(
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 15.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            height: size.convert(context, 10),
                          ),
                          Container(
                              child: Text(
                            "How can we help",
                            style: style.QuicksandRegular(
                                color: Color(0xff1d2226),
                                fontSize: size.convert(context, 14)),
                          )),
                          SizedBox(
                            height: size.convert(context, 10),
                          ),
                          Container(
                            //margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 16)),
                            padding: EdgeInsets.symmetric(
                                horizontal: size.convert(context, 10)),
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(0xff76b9fd),
                                    width: size.convert(context, 1.5)),
                                borderRadius: BorderRadius.circular(
                                    size.convert(context, 15))),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    // margin: EdgeInsets.symmetric(horizontal: size.convertWidth(context, 16)),
                                    child: DropdownButton(
                                      hint: Row(
                                        children: [
                                          Container(
                                              child: Text(
                                            "Select your query here",
                                            style: style.QuicksandRegular(
                                                color: Color(0xff1d2226),
                                                fontSize:
                                                    size.convert(context, 14)),
                                          )),
                                        ],
                                      ),
                                      value: selectedItem,
                                      items: _query.map((val) {
                                        return DropdownMenuItem(
                                          child: Text(
                                            val ?? "",
                                            style: style.QuicksandRegular(
                                                color: Color(0xff1d2226),
                                                fontSize:
                                                    size.convert(context, 14)),
                                          ),
                                          value: val,
                                        );
                                      }).toList(),
                                      onChanged: (newValue) {
                                        setState(() {
                                          // queryEmpty = "";
                                          selectedItem = newValue;
                                        });
                                      },
                                      //dropdownColor: Colors.pinkAccent,
                                      focusColor: Colors.pinkAccent,
                                      icon: Container(),
                                      underline: Container(),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: Icon(
                                    Icons.arrow_drop_down,
                                    color: Color(0xff1d2226),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Text(
                              queryEmpty ?? "",
                              style: style.PoppinsRegular(color: Colors.red),
                            ),
                          ),
                          SizedBox(
                            height: size.convert(context, 10),
                          ),
                          Container(
                            child: Text(
                              "Full Name *",
                              style: style.QuicksandRegular(
                                  fontSize: size.convert(context, 14)),
                            ),
                          ),
                          CustomTextFieldSecond(
                            textEditingController: _fullNameController,
                          ),
                          Container(
                            child: Text(
                              nameEmpty ?? "",
                              style: style.PoppinsRegular(color: Colors.red),
                            ),
                          ),
                          SizedBox(
                            height: size.convert(context, 10),
                          ),
                          Container(
                            child: Text(
                              "Contact *",
                              style: style.QuicksandRegular(
                                  fontSize: size.convert(context, 14)),
                            ),
                          ),
                          CustomTextFieldSecond(
                            textEditingController: _contactController,
                          ),
                          Container(
                            child: Text(
                              contactEmpty ?? "",
                              style: style.PoppinsRegular(color: Colors.red),
                            ),
                          ),
                          SizedBox(
                            height: size.convert(context, 10),
                          ),
                          Container(
                            child: Text(
                              "Email Address",
                              style: style.QuicksandRegular(
                                  fontSize: size.convert(context, 14)),
                            ),
                          ),
                          CustomTextFieldSecond(
                            textEditingController: _emailController,
                          ),
                          Container(
                            child: Text(
                              emailEmpty ?? "",
                              style: style.PoppinsRegular(color: Colors.red),
                            ),
                          ),
                          SizedBox(
                            height: size.convert(context, 10),
                          ),
                          Container(
                            child: Text(
                              "Remarks",
                              style: style.QuicksandRegular(
                                  fontSize: size.convert(context, 14)),
                            ),
                          ),
                          CustomTextFieldSecond(
                            maxLine: 4,
                            maxLength: 1500,
                            textEditingController: _remarksController,
                          ),
                          Container(
                            child: Text(
                              remarksEmpty ?? "",
                              style: style.PoppinsRegular(color: Colors.red),
                            ),
                          ),
                          SizedBox(
                            height: size.convert(context, 10),
                          ),
                          Container(
                            child: Text(
                              "Attachment",
                              style: style.QuicksandRegular(
                                  fontSize: size.convert(context, 14)),
                            ),
                          ),
                          SizedBox(
                            height: size.convert(context, 10),
                          ),
                          images == null || images.length == 0
                              ? GestureDetector(
                                  onTap: () => reportPicture(),
                                  child: Container(
                                    width: size.convertWidth(context, 106),
                                    height: size.convert(context, 92),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color:

                                                // error.length != 0
                                                //     ? error[2] == "image"
                                                //         ? Colors.red
                                                //         : Colors.transparent
                                                //     :
                                                //

                                                Colors.transparent,
                                            width: size.convert(context, 1.5)),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Color(0xff8e7b65d6),
                                            blurRadius: 6,
                                            offset: Offset(0, 3),
                                          )
                                        ],
                                        borderRadius: BorderRadius.circular(
                                            size.convert(context, 13))),
                                    child: Center(
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset(
                                              "assets/images/addphotos.svg"),
                                          SizedBox(
                                            height: size.convert(context, 10),
                                          ),
                                          Container(
                                            child: Text(
                                              "Add image *",
                                              style: style.QuicksandRegular(
                                                  fontSize: size.convert(
                                                      context, 10)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : Container(
                                  height: (200 / 8.148314082864863) *
                                      SizeConfig.heightMultiplier,
                                  width: MediaQuery.of(context).size.width,
                                  child: new ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder:
                                        (BuildContext context, int index) =>
                                            new Container(
                                      margin: EdgeInsets.all(2.0),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: Colors.black),
                                        borderRadius: BorderRadius.circular(25),
                                        shape: BoxShape.rectangle,
                                      ),
                                      padding: const EdgeInsets.all(10.0),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(25),
                                        child: Image.file(
                                          imagesFiles[index],
                                          height: (200 / 8.148314082864863) *
                                              SizeConfig.heightMultiplier,
                                          width: (150 / 4.853932272197492) *
                                              SizeConfig.widthMultiplier,
                                        ),
                                      ),
                                    ),
                                    itemCount: imagesFiles.length,
                                  ),
                                ),
                          SizedBox(height: 10 * SizeConfig.heightMultiplier),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              filledButton(
                                radius: 10,
                                txt: "Submit",
                                onTap: () {
                                  if (validationForm()) {
                                    setState(() {
                                      loading = true;
                                    });
                                    customerSupport(user.sId);
                                  }
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: 10 * SizeConfig.heightMultiplier),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        loading
            ? Stack(
                children: <Widget>[
                  Container(
                    color: Color(0xffffffff).withOpacity(0.4),
                    height: double.infinity,
                    width: double.infinity,
                    child: Opacity(
                      opacity: 1.0,
                      child: Center(
                          child: Image.asset(
                        "assets/images/logo2.gif",
                        height: 200,
                        width: 100,
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }

  bool validationForm() {
    if (selectedItem == "" || selectedItem == null) {
      setState(() {
        queryEmpty = "please select query";
      });
      return false;
    }
    if (_fullNameController.text.isEmpty) {
      print('nmae');
      setState(() {
        nameEmpty = "please enter your full name";
      });
      return false;
    }

    if (_contactController.text.isEmpty) {
      setState(() {
        nameEmpty = "";
        contactEmpty = "please enter your contact";
      });
      return false;
    }
    if (_emailController.text.isEmpty) {
      setState(() {
        contactEmpty = "";
        emailEmpty = "please enter your email address";
      });
      return false;
    }
    if (_remarksController.text.isEmpty) {
      setState(() {
        contactEmpty = "";
        remarksEmpty = "please enter your remarks";
      });
      return false;
    } else {
      queryEmpty = "";
      return true;
    }
  }

  Future<Map> customerSupport(String userid) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String token = _prefs.getString("token");
    List<String> images = [];
    imagesFiles.forEach((element) {
      images.add(element.path);
    });
    String url = "$base_url/contact/support";
    var request = http.MultipartRequest('POST', Uri.parse(url))
      ..fields['createdBy'] = userid
      ..fields['remarks'] = _remarksController.text
      ..fields['fullName'] = _fullNameController.text
      ..fields['contactNumber'] = _contactController.text
      ..fields['email'] = _emailController.text;
    request.headers['authorization'] = token;
    images.forEach((f) async {
      request.files.add(await http.MultipartFile.fromPath('images', f));
    });
    http.Response response =
        await http.Response.fromStream(await request.send());
    var data = jsonDecode(response.body);
    print(data);
    if (data["Success"] == true) {
      loading = false;
      Toast.show("Report Sent", context, gravity: Toast.TOP);
      Navigator.of(context).pop();
      print("posted");
    } else {
      print("Error");
    }
    return {"data": data};
  }
}
