import 'package:biznes/chat_room/chat_room.dart';
import 'package:biznes/helper/allmessageshelper.dart';
import 'package:biznes/product_UI/bottomBar_UI/newprofile.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:biznes/helper/notificationhelper.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:biznes/res/color.dart' as fontfaimly;
import 'package:biznes/services/register_logic.dart';
import 'package:intl/intl.dart' as intl;
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import '../helper/login_helper.dart';
import 'package:biznes/services/server.dart';

class MyNotification extends StatefulWidget {
  @override
  _MyNotificationState createState() => _MyNotificationState();
}

class _MyNotificationState extends State<MyNotification> {
  bool isloading = true;
  showdialogfornew() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            content: SingleChildScrollView(
              child: Container(
                // height: 55 * SizeConfig.heightMultiplier,
                width: 200,
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      "assets/images/logo.png",
                      height: (80 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                      width: (220 / 4.853932272197492) *
                          SizeConfig.widthMultiplier,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Text(
                            "Welcome to Quick Biznes",
                            style: TextStyle(
                              color: Color(0xff394c81),
                              fontFamily: fontfaimly.fontfaimly,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      """Quick Biznes is a platform that will give you an opportunity to send message and chat with businessmen whether if it's about buying or selling a product, a platform that primarily focuses on saving your time, money and efforts. 
Add your product into your profile. You can search for products and go to user profile and chat individually, or you can also go to send message section which is on first left hand side of the home button, where you can send a message to the entire sub category. Wait for message receiver approval into order to chat with him or her. Sent Inquires in chat section will show you statistics about your sent messages. 

We wish you best of luck. 

CEO Quick Biznes""",
                      style: TextStyle(
                        fontFamily: fontfaimly.fontfaimlyregular,
                        fontSize: 11,
                        color: Color(0xff5e5c5c),
                      ),
                    ),
                    SizedBox(
                      height: (10 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                    filledButton(
                      txt: "Get Started",
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  gettingnotificationdata() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String userid = _prefs.getString("userid");
    String token = _prefs.getString("token");
    gettingnotification(userid, token, context).then((value) {
      Provider.of<Notificationgetting>(context, listen: false)
          .gettingNotificationData(value);
      setState(() {
        isloading = false;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    gettingnotificationdata();
  }

  @override
  Widget build(BuildContext context) {
    var userid = Provider.of<LoginHelper>(context).user;
    var token = Provider.of<LoginHelper>(context).token;
    var data = Provider.of<Notificationgetting>(context).newnotification;
    var bottomnavbar = Provider.of<Notificationgetting>(context, listen: false);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          title: Text("Notifications",
              style: TextStyle(
                  fontFamily: fontfaimly.fontfaimlyquicksandbold,
                  color: fontfaimly.buttonColor)),
          centerTitle: true,
          leading: Column(
            children: <Widget>[
              IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: fontfaimly.buttonColor,
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ),
        body: isloading
            ? Container(
                child: Center(
                  child: Image.asset(
                    "assets/images/logo2.gif",
                    height: 100,
                    width: 100,
                  ),
                ),
              )
            : ListView.builder(
                itemCount: data.notifications.length,
                itemBuilder: (BuildContext context, int index) {
                  DateTime dateTime1 =
                      DateTime.parse(data.notifications[index].createdAt);
                  String formattedTime1 = intl.DateFormat()
                      .format(dateTime1.add(Duration(hours: 5)));
                  // DateTime dateTime1 = DateTime.parse(data[index].createdAt);
                  // String formattedTime = DateFormat('dd.MM.yyyy').format(dateTime1);
                  return data.notifications[index].info == "chat"
                      ? Container()
                      : Padding(
                          padding: EdgeInsets.only(
                              right: 10, left: 10, top: 3, bottom: 3),
                          child: GestureDetector(
                            onLongPress: () {
                              showdialogfordeleting(
                                  text: "Are you sure to delete notification ?",
                                  title: "Delete Notification!",
                                  context: context,
                                  onPressed: () {
                                    deletenotification(
                                            data.notifications[index].sId,
                                            context)
                                        .then((value) {
                                      gettingnotificationdata();
                                    });
                                  });
                            },
                            onTap: () {
                              if (data.notifications[index].info ==
                                  "membership") {
                                Navigator.of(context).pop();
                                bottomnavbar.updatenotification("Page4", 3);
                              } else if (data.notifications[index].info ==
                                  "welcome") {
                                showdialogfornew();
                              } else if (data.notifications[index].info ==
                                  "productUnderReview") {
                                bottomnavbar.updatenotification("Page4", 3);
                              } else if (data.notifications[index].info ==
                                  "productPublished") {
                                bottomnavbar.updatenotification("Page4", 3);
                              } else if (data.notifications[index].info ==
                                  "productReviewed") {
                                bottomnavbar.updatenotification("Page4", 3);
                              } else if (data.notifications[index].info ==
                                  "chat") {
                                getconversation(
                                        data.notifications[index].conversation,
                                        userid.sId,
                                        context)
                                    .then((value) {
                                  if (value != null) {
                                    print(value[0].createdBy.fullName);
                                    Navigator.of(context, rootNavigator: true)
                                        .push(MaterialPageRoute(
                                            builder: (context) => ChatRoom(
                                                  conversationid: data
                                                      .notifications[index]
                                                      .conversation,
                                                  createdforid: value[0]
                                                              .createdFor
                                                              .sId ==
                                                          userid.sId
                                                      ? value[0].createdBy.sId
                                                      : value[0].createdFor.sId,
                                                  categoryid:
                                                      value[0].categoryId.sId,
                                                  userid: userid.sId,
                                                  username: userid.fullName,
                                                  subcategoryid: value[0]
                                                      .subCategoryId
                                                      .sId,
                                                  image: value[0]
                                                              .createdFor
                                                              .sId ==
                                                          userid.sId
                                                      ? '$base_url/${value[0].createdBy.profilePic}'
                                                      : '$base_url/${value[0].createdFor.profilePic}',
                                                  createdbyid:
                                                      value[0].createdBy.sId,
                                                  status: value[0].status,
                                                  messageusername:
                                                      value[0].createdFor.sId ==
                                                              userid.sId
                                                          ? value[0]
                                                              .createdBy
                                                              .username
                                                          : value[0]
                                                              .createdFor
                                                              .username,
                                                  onlinestatus: value[0]
                                                              .createdBy
                                                              .sId ==
                                                          userid.sId
                                                      ? value[0]
                                                                  .createdFor
                                                                  .onlineStatus ==
                                                              true
                                                          ? "online"
                                                          : "offline"
                                                      : value[0]
                                                                  .createdBy
                                                                  .onlineStatus ==
                                                              true
                                                          ? "online"
                                                          : "offline",
                                                )));
                                    // setState(() {
                                    //   getAllinboxmessagesfrommodel(userid:userid.sId)
                                    //       .then((value) {
                                    //     Provider.of<AllmessagesHelper>(context,
                                    //             listen: false)
                                    //         .onmessagesrecieved(value);
                                    //   });
                                    // });
                                  }
                                });
                              }
                              print(data.notifications[index].info);
                            },
                            child: Container(
                              padding: EdgeInsets.only(
                                  right: 10, left: 8, top: 10, bottom: 10),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: Colors.grey[200],
                                      blurRadius: 3.0,
                                      offset: Offset(0.0, 5.0),
                                      spreadRadius: 0.2),
                                ],
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Row(
                                children: [
                                  Container(
                                      child: SvgPicture.asset(
                                    'assets/images/notificationsvgicon.svg',
                                    height: 60,
                                  )),
                                  SizedBox(
                                    width: 4,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width:
                                                70 * SizeConfig.widthMultiplier,
                                            child: Text(
                                              data.notifications[index].type,
                                              overflow: TextOverflow.clip,
                                              textAlign: TextAlign.start,
                                              maxLines: 2,
                                              style: TextStyle(
                                                  fontSize:
                                                      (18 / 8.148314082864863) *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                  color: Color(0xff394c81),
                                                  fontFamily:
                                                      fontfaimly.fontfaimly,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            width:
                                                70 * SizeConfig.widthMultiplier,
                                            child: Text(
                                              data.notifications[index].message,
                                              overflow: TextOverflow.clip,
                                              style: TextStyle(
                                                fontFamily:
                                                    fontfaimly.fontfaimly,
                                                color: Colors.blueGrey,
                                                fontSize: (16 /
                                                        8.148314082864863) *
                                                    SizeConfig.heightMultiplier,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "$formattedTime1",
                                            style: TextStyle(
                                              color: Colors.blueGrey,
                                              fontFamily:
                                                  fontfaimly.fontfaimlyregular,
                                              fontSize: (11 /
                                                      8.148314082864863) *
                                                  SizeConfig.heightMultiplier,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                },
              ));
  }
}
