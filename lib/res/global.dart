
List<String> cityList = [
  'kabul',
  'badakhshan',
  'badghiz',
  'baghlan',
  'balkh',
  'bamiyan',
  'daikundi',
  'faryab',
  'ghazni',
  'ghor',
  'helmand',
  'herat',
  'jowzjan',
  'kandahar',
  'kapisa',
  'khost',
  'kunar',
  'kunduz',
  'laghman',
  'logar',
  'maidan  wardak',
  'nangarhar',
  'paktia',
  'paktika',
  'panjshir',
  'parwan',
  'samangaan',
  'sar-e-pul',
  'takhar',
  'zabul',
  'nimroz',
  'farah',
  'nuristan',
  'uruzgan',
];
List <String> titleList = [
  'Doctor',
'Dr.',
'Prof.',
'Assist. Prof.',
'Assist. Prof. Dr',
'Assoc. Prof.',
//'Brig. Dr.',
//'Brig. (R) Dr.',
//'Brig. (R) Prof. Dr',
//'Col. Dr.',
//'Col. (R) Dr.',
//'Lft. Col. Dr.',
//'Lft. (R) Col. Dr',
//'Cap. Dr.',
//'Cap. (R) Dr.',
];


String initialCapital(String text) {
  return text != null
      ? text.substring(0, 1).toUpperCase() + text.substring(1)
      : text;
}

class Util {
  static double remap(
      double value,
      double originalMinValue,
      double originalMaxValue,
      double translatedMinValue,
      double translatedMaxValue) {
    if (originalMaxValue - originalMinValue == 0) return 0;

    return (value - originalMinValue) /
            (originalMaxValue - originalMinValue) *
            (translatedMaxValue - translatedMinValue) +
        translatedMinValue;
  }
}

double uploadProgressPercentage(int sentBytes, int totalBytes) {
  double __progressValue =
      Util.remap(sentBytes.toDouble(), 0, totalBytes.toDouble(), 0, 1);

  __progressValue = double.parse(__progressValue.toStringAsFixed(2));

  return __progressValue;
}
