import 'package:biznes/bloc/inbox/bloc.dart';
import 'package:biznes/bloc/inbox/events.dart';
import 'package:biznes/chat_room/globals.dart';
import 'package:biznes/helper/categoryInboxHelper.dart';
import 'package:biznes/helper/conversationhelper.dart';
import 'package:biznes/helper/notificationhelper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/memberShip/dilouge.dart';
import 'package:biznes/product_UI/bottomBar_UI/chat.dart';
import 'package:biznes/product_UI/bottomBar_UI/newprofile.dart';
import 'package:biznes/services/BackEndApis.dart';
import 'package:biznes/services/endpoints.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/product_UI/bottomBar_UI/product_home_page.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/product_UI/bottomBar_UI/settings.dart';
import 'package:biznes/product_UI/bottomBar_UI/write_message.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/size_config.dart';
import 'package:biznes/res/size.dart';
import 'package:biznes/res/color.dart' as fontandcolor;
import 'package:biznes/res/style.dart';
import 'package:flutter/material.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/chat_room/chat_room.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:biznes/helper/allmessageshelper.dart';
import 'package:hive/hive.dart';

String roleValue;

class ProductHome extends StatefulWidget {
  @override
  _ProductHomeState createState() => _ProductHomeState();
}

class _ProductHomeState extends State<ProductHome> {
  Box unreadsBox;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  Future<void> cancelNotification() async {
    await flutterLocalNotificationsPlugin.cancelAll();
  }

  AndroidInitializationSettings androidInitializationSettings;
  IOSInitializationSettings iosInitializationSettings;
  InitializationSettings initializationSettings;
  final fln = FlutterLocalNotificationsPlugin();
  void initilizing() async {
    androidInitializationSettings = AndroidInitializationSettings('app_icon');
    iosInitializationSettings = IOSInitializationSettings(
      onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );
    initializationSettings = InitializationSettings(
        androidInitializationSettings, iosInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  }

  void _shownotification(String title, String body, String payload) async {
    await notification(title: title, body: body, payloads: payload);
  }

  void _updatecount2(BuildContext context) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.getString("userid" ?? "");
    gettingnotificationcount(_prefs.getString("userid") ?? "",
            _prefs.getString("token"), context)
        .then((value) {
      value == null
          ? Provider.of<CountHelper>(context, listen: false).counter = 0
          : Provider.of<CountHelper>(context, listen: false).counter =
              value['notifications'] ?? 0;
    });
  }

  void _updatemessage() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    // getAllinboxmessagesfrommodel(
    //         userid: _prefs.getString("userid"), context: context)
    //     .then((value) {
    //   Provider.of<AllmessagesHelper>(context, listen: false)
    //       .onmessagesrecieved(value);
    // });
    // getCategoriesForInbox(_prefs.getString("userid"),context).then((value) {
    //   Provider.of<CategoryHelper>(context,listen: false).setCategoryInbox(value);
    // });
  }

  Future<void> notification(
      {String title, String body, String payloads}) async {
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails(
      "Channel ID",
      "Channel title",
      "Channel body",
      priority: Priority.High,
      importance: Importance.High,
      playSound: true,
    );
    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();
    NotificationDetails notificationDetails = NotificationDetails(
      androidNotificationDetails,
      iosNotificationDetails,
    );
    await flutterLocalNotificationsPlugin
        .show(0, title, body, notificationDetails, payload: payloads);
  }

  Future onSelectNotification(String payloads) {
    var data = Provider.of<Notificationgetting>(context);
    var currentPage = data.currentPage;
    var selectedIndex = data.selectIndex;
    var counterset2 = Provider.of<CountHelper>(context, listen: false);
    if (payloads == "/chatTile") {
      setState(() {
        currentPage = "Page1";
        selectedIndex = 0;
        counterset2.zerocunter();
      });
    } else if (payloads == "/notifications") {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => MyNotification()));
    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    return CupertinoAlertDialog(
      title: Text(title),
      content: Text(body),
      actions: [
        CupertinoDialogAction(
          child: Text("okay"),
          onPressed: () {
            print("");
          },
        ),
      ],
    );
  }

  gettingdata(message) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    var counterset2 = Provider.of<CountHelper>(context, listen: false);
    print("onResume: $message");
    _updatemessage();
    //api calling

    if (message['data']['status'] == "/chatTile") {
      getconversation(message['data']['conversation_id'],
              _prefs.getString("userid"), context)
          .then((value) {
        counterset2.zerocunter();
        if (value != null) {
          print(value[0].createdBy.fullName);
          Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
              builder: (context) => ChatRoom(
                    conversationid: message['data']['conversation_id'],
                    createdforid:
                        value[0].createdFor.sId == _prefs.getString("userid")
                            ? value[0].createdBy.sId
                            : value[0].createdFor.sId,
                    categoryid: value[0].categoryId.sId,
                    userid: _prefs.getString("userid"),
                    subcategoryid: value[0].subCategoryId.sId,
                    image: value[0].createdFor.sId == _prefs.getString("userid")
                        ? '$base_url/${value[0].createdBy.profilePic}'
                        : '$base_url/${value[0].createdFor.profilePic}',
                    createdbyid: value[0].createdBy.sId,
                    status: value[0].status,
                    messageusername:
                        value[0].createdFor.sId == _prefs.getString("userid")
                            ? value[0].createdBy.username
                            : value[0].createdFor.username,
                    onlinestatus:
                        value[0].createdBy.sId == _prefs.getString("userid")
                            ? value[0].createdFor.onlineStatus == true
                                ? "online"
                                : "offline"
                            : value[0].createdBy.onlineStatus == true
                                ? "online"
                                : "offline",
                  )));
          // setState(() {
          //   getAllinboxmessagesfrommodel(userid: _prefs.getString("userid"))
          //       .then((value) {
          //     Provider.of<AllmessagesHelper>(context, listen: false)
          //         .onmessagesrecieved(value);
          //   });
          // });
        }
      });
    } else if (message['data']['status'] == "/notifications") {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => MyNotification()));
      counterset2.zerocunter();
    }
  }

  // String currentPage = "Page3";
  // int selectedIndex = 2;
  getIconMessageCount() async {
    var data = Provider.of<Notificationgetting>(context, listen: false);
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    BackEndApi(endpoint: getUserInboxCount + _prefs.getString("userid"))
        .getData()
        .then((value) {
      data.setunreadcount(value["unReadCount"]);
    });
  }

  getting() async {
    WidgetsFlutterBinding.ensureInitialized();
    await FlutterDownloader.initialize(
      debug: true,
    );
  }

  @override
  void initState() {
    getting();
    unreadsBox = Hive.box('conversationsGeneration');
    _updatecount2(context);
    getIconMessageCount();
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(conversation_id + "this is conversation id");
        SharedPreferences _prefs = await SharedPreferences.getInstance();
        if (message['data']['conversation_id'] == conversation_id) {
          print("is this printing");
          getconversation(message['data']['conversation_id'],
                  _prefs.getString("userid"), context)
              .then((value) {
            unreadsBox.put(message['data']['conversation_id'], value);
            print("calling methods for count");
            Provider.of<NewMessagesHlper>(context, listen: false)
                .setNewMessages(value);
          });
          context.bloc<AlbumBloc>().add(FetchMessage(
              userId: _prefs.getString("userid"),
              catId: message['data']['conversation_id'],
              query: "",
              roleName: ""));
        } else {
          _shownotification(message['notification']['title'],
              message['notification']['body'], message['data']['status']);
        }
        if (message['data']['status'] != "/chatTile") {
          print("error");
          Provider.of<CountHelper>(context, listen: false).increment();
        }
        _updatemessage();
      },
      onLaunch: (Map<String, dynamic> message) async {
        gettingdata(message);
      },
      onResume: (Map<String, dynamic> message) async {
        // _fcm.deleteInstanceID();
        fln.toString();
        gettingdata(message);
      },
    );
    _fcm.requestNotificationPermissions(
      IosNotificationSettings(sound: true, badge: true, alert: true),
    );
    initilizing();
    super.initState();
  }

  List<String> pageKeys = ["Page1", "Page2", "Page3", "Page4", "Page5"];
  Map<String, GlobalKey<NavigatorState>> _navigatorKeys = {
    "Page1": GlobalKey<NavigatorState>(),
    "Page2": GlobalKey<NavigatorState>(),
    "Page3": GlobalKey<NavigatorState>(),
    "Page4": GlobalKey<NavigatorState>(),
    "Page5": GlobalKey<NavigatorState>(),
  };
  final PageStorageBucket bucket = PageStorageBucket();

  Widget _buildOffstageNavigator(String tabItem) {
    var data = Provider.of<Notificationgetting>(context);
    var currentPage = data.currentPage;
    var selectedIndex = data.selectIndex;
    var user = Provider.of<LoginHelper>(context).user;
    return Offstage(
      offstage: currentPage != tabItem,
      child: TabNavigator(
        navigatorKey: _navigatorKeys[tabItem],
        tabItem: tabItem,
        roleName: user.roleName,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var data = Provider.of<Notificationgetting>(context, listen: false);
    var currentPage = data.currentPage;
    var selectedIndex = data.selectIndex;
    bool keyboardIsOpened = MediaQuery.of(context).viewInsets.bottom != 0.0;
    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab =
            !await _navigatorKeys[currentPage].currentState.maybePop();
        if (isFirstRouteInCurrentTab) {
          if (currentPage != "Page3") {
            data.updatenotification(pageKeys[2], 2);
            return false;
          }
        }
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Visibility(
          visible: !keyboardIsOpened,
          child: Padding(
            padding: EdgeInsets.only(top: 31),
            child: SizedBox(
              height: size.convert(context, 70),
              width: 70,
              child: FloatingActionButton(
                backgroundColor: Colors.transparent,
                elevation: 0,
                onPressed: () {},
                child: Container(
                  height: size.convert(context, 70),
                  width: 68,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.white, width: 4),
                    shape: BoxShape.circle,
                    gradient: currentPage == "Page3"
                        ? LinearGradient(
                            begin: const Alignment(0.7, -0.5),
                            end: const Alignment(0.6, 0.5),
                            colors: [
                              Color(0xff69bef7),
                              Color(0xff7595f4),
                            ],
                          )
                        : LinearGradient(
                            begin: const Alignment(0.7, -0.5),
                            end: const Alignment(0.6, 0.5),
                            colors: [
                              Color(0xffDDE2E3),
                              Color(0xffDDE2E3),
                            ],
                          ),
                  ),
                  child: IconButton(
                    icon: Icon(Icons.home),
                    onPressed: () {
                      data.updatenotification(pageKeys[2], 2);
                      _navigatorKeys[currentPage]
                          .currentState
                          .popUntil((route) => route.isFirst);
                      selectedIndex = 2;
                      // _selectTab(pageKeys[2], 2);
                    },
                  ),
                ),
              ),
            ),
          ),
        ),
        body: PageStorage(
          bucket: bucket,
          child: Stack(children: <Widget>[
            _buildOffstageNavigator("Page1"),
            _buildOffstageNavigator("Page2"),
            _buildOffstageNavigator("Page3"),
            _buildOffstageNavigator("Page4"),
            _buildOffstageNavigator("Page5"),
          ]),
        ),
        // handleAndroidBackButtonPress: true,
        bottomNavigationBar: BottomAppBar(
          // shape: CircularNotchedRectangle(),
          child: Container(
            color: Colors.white,
            height: 8.8 * SizeConfig.heightMultiplier,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: size.convertWidth(context, 20)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            _navigatorKeys[currentPage]
                                .currentState
                                .popUntil((route) => route.isFirst);
                            data.updatenotification(pageKeys[0], 0);
                            selectedIndex = 0;
                            data.setunreadcount(0);
                          },
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                selectedIndex == 0
                                    ? SvgPicture.asset(
                                        'assets/bottomBarIcons/activeChat.svg')
                                    : data.unreadcount == 0
                                        ? SvgPicture.asset(
                                            "assets/bottomBarIcons/Chat.svg")
                                        : Stack(
                                            alignment: Alignment.topRight,
                                            children: [
                                              Container(
                                                  padding: EdgeInsets.only(
                                                      top: 5.0, right: 3.0),
                                                  child: SvgPicture.asset(
                                                      "assets/bottomBarIcons/Chat.svg")),
                                              Container(
                                                height:
                                                    size.convert(context, 15),
                                                width: size.convertWidth(
                                                    context, 15),
                                                decoration: BoxDecoration(
                                                  color:
                                                      fontandcolor.tabbarcolor,
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    data.unreadcount
                                                            .toString() ??
                                                        "1",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: size.convert(
                                                          context, 7),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                // SizedBox(
                                //   height: size.convert(context, 5),
                                // ),
                                Text(
                                  "Chat",
                                  style: style.MontserratRegular(
                                    fontSize: size.convert(context, 8),
                                    color: selectedIndex == 0
                                        ? Color(0xff458BFC)
                                        : fontandcolor.footerfontcolor,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            cancelNotification();
                            _navigatorKeys[currentPage]
                                .currentState
                                .popUntil((route) => route.isFirst);
                            data.updatenotification(pageKeys[1], 1);
                          },
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                selectedIndex == 1
                                    ? SvgPicture.asset(
                                        'assets/bottomBarIcons/activeMassage.svg')
                                    : SvgPicture.asset(
                                        'assets/bottomBarIcons/Message.svg',
                                      ),
                                SizedBox(
                                  height: size.convert(context, 5),
                                ),
                                Text(
                                  "Bulk Messages",
                                  style: style.MontserratRegular(
                                    fontSize: size.convert(context, 8),
                                    color: selectedIndex == 1
                                        ? Color(0xFF458BFC)
                                        : fontandcolor.footerfontcolor,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: size.convertWidth(context, 70),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: size.convertWidth(context, 20)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            _navigatorKeys[currentPage]
                                .currentState
                                .popUntil((route) => route.isFirst);
                            data.updatenotification(pageKeys[3], 3);
                          },
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                selectedIndex == 3
                                    ? SvgPicture.asset(
                                        'assets/bottomBarIcons/activeProfile.svg')
                                    : SvgPicture.asset(
                                        'assets/bottomBarIcons/Profile.svg'),
                                SizedBox(
                                  height: size.convert(context, 5),
                                ),
                                Text(
                                  "Profile",
                                  style: style.MontserratRegular(
                                    fontSize: size.convert(context, 8),
                                    color: selectedIndex == 3
                                        ? Color(0xff458BFC)
                                        : fontandcolor.footerfontcolor,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            _navigatorKeys[currentPage]
                                .currentState
                                .popUntil((route) => route.isFirst);
                            data.updatenotification(pageKeys[4], 4);
                          },
                          child: Container(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                selectedIndex == 4
                                    ? SvgPicture.asset(
                                        'assets/bottomBarIcons/activeSetting.svg')
                                    : SvgPicture.asset(
                                        'assets/bottomBarIcons/setting.svg'),
                                SizedBox(
                                  height: size.convert(context, 5),
                                ),
                                Text(
                                  "Settings",
                                  style: style.MontserratRegular(
                                    fontSize: size.convert(context, 8),
                                    color: selectedIndex == 4
                                        ? Color(0xff458BFC)
                                        : fontandcolor.footerfontcolor,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class TabNavigator extends StatelessWidget {
  TabNavigator({this.navigatorKey, this.tabItem, this.roleName});

  final GlobalKey<NavigatorState> navigatorKey;
  final String tabItem;
  final String roleName;
  @override
  Widget build(BuildContext context) {
    Widget child;
    if (tabItem == "Page1")
      child = roleName == "Guest" ? Dilouge() : Chat();
    else if (tabItem == "Page2")
      child = roleName == "Guest" ? Dilouge() : WriteMessage();
    else if (tabItem == "Page3")
      child = ProductMain();
    else if (tabItem == "Page4")
      child = roleName == "Guest" ? Dilouge() : Newprofile();
    else if (tabItem == "Page5") child = Setting();
    return Navigator(
      key: navigatorKey,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(builder: (context) => child);
      },
    );
  }
}
