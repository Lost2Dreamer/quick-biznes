import 'package:biznes/chat_room/chatTileCategories.dart';
import 'package:biznes/chat_room/chat_inbox.dart';
import 'package:biznes/chat_room/inquires.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/res/color.dart' as color;
import 'package:biznes/res/size.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:biznes/setting/notification.dart';
import 'package:provider/provider.dart';
import 'package:biznes/helper/counthelper.dart';

class Chat extends StatefulWidget {
  const Chat({Key key}) : super(key: key);
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  var formKey = GlobalKey<FormState>();
  var volume = Icons.volume_up;
  var firstValue;
  var secondValue;
  TabController tabController;
  int length = 3;
  int length2 = 2;
  int index = 0;

  @override
  Widget build(BuildContext context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    List<Widget> childernforbroker = [
      ChatGuest(),
      ChatCategoriesTile(),
      Inquires(),
    ];
    List<Widget> childern = [
      ChatCategoriesTile(),
      Inquires(),
    ];
    List<Widget> tabs1 = [
      Tab(
        child: Row(
          children: <Widget>[
            Icon(
              Icons.inbox,
              color: index == 0 ? color.tabbarcolor : Color(0xff5e5c5c),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Text(
                "Requests",
                style: TextStyle(
                    fontFamily: 'MontserratRegular',
                    fontSize:
                        (14 / 8.148314082864863) * SizeConfig.heightMultiplier),
              ),
            ),
          ],
        ),
      ),
      Tab(
        child: Row(
          children: <Widget>[
            Icon(
              Icons.message,
              color: index == 1 ? color.tabbarcolor : Color(0xff5e5c5c),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Text(
                "Inbox",
                style: TextStyle(
                    fontFamily: 'MontserratRegular',
                    fontSize:
                        (14 / 8.148314082864863) * SizeConfig.heightMultiplier),
              ),
            ),
          ],
        ),
      ),
      Tab(
        child: Row(
          children: <Widget>[
            Icon(
              Icons.check_circle_outline,
              color: index == 2 ? color.tabbarcolor : Color(0xff5e5c5c),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Text(
                "Sent Inquiries",
                style: TextStyle(
                    fontFamily: 'MontserratRegular',
                    fontSize:
                        (14 / 8.148314082864863) * SizeConfig.heightMultiplier),
              ),
            ),
          ],
        ),
      ),
    ];
    List<Widget> tab2 = [
      InkWell(
        child: Tab(
          child: Row(
            children: <Widget>[
              Icon(
                Icons.inbox,
                color: index == 0 ? color.tabbarcolor : Color(0xff5e5c5c),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Text(
                  "Inbox",
                  style: TextStyle(
                      fontFamily: 'MontserratRegular',
                      fontSize: (16 / 8.148314082864863) *
                          SizeConfig.heightMultiplier),
                ),
              ),
            ],
          ),
        ),
      ),
      Tab(
        child: Row(
          children: <Widget>[
            Icon(
              Icons.message,
              color: index == 1 ? color.tabbarcolor : Color(0xff5e5c5c),
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Text(
                "Sent Inquiries",
                style: TextStyle(
                    fontFamily: 'MontserratRegular',
                    fontSize:
                        (16 / 8.148314082864863) * SizeConfig.heightMultiplier),
              ),
            ),
          ],
        ),
      ),
    ];
    var role = Provider.of<LoginHelper>(context).user.roleName;
    return DefaultTabController(
      length: role == "Broker" ? length : length2,
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            children: [
              CustomAppBar(
                  context: context,
                  text: "Chat",
                  length: counters.toString(),
                  isShowBackground: true,
                  onTap: () {
                    counterset.zerocunter();
                    Navigator.of(context).push(PageTransition(
                        child: MyNotification(),
                        duration: Duration(milliseconds: 700),
                        type: PageTransitionType.leftToRightWithFade));
                  }),
              Container(
                height: size.convert(context, 50),
                child: TabBar(
                    onTap: (indexs) {
                      setState(() {
                        FocusScope.of(context).requestFocus(FocusNode());
                        index = indexs;
                      });
                      print(indexs);
                    },
                    unselectedLabelColor: Color(0xff5e5c5c),
                    indicatorColor: color.tabbarcolor,
                    labelColor: Colors.black,
                    controller: tabController,
                    tabs: role == "Broker" ? tabs1 : tab2),
              ),
              Expanded(
                child: Container(
                  child: TabBarView(
                    controller: tabController,
                    children: role == "Broker" ? childernforbroker : childern,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
    // OrientationBuilder(builder: (context, orientation) {
    //   return Container(
    //     decoration: BoxDecoration(
    //         color: Colors.white,
    //         borderRadius: BorderRadius.only(
    //           bottomLeft: Radius.circular(20.5),
    //           bottomRight: Radius.circular(20.5),
    //         )),
    //     child: Column(
    //       children: <Widget>[
    //         Padding(
    //           padding:
    //           const EdgeInsets.only(left: 9.0, top: 8.0, right: 9.0),
    //           child: Row(
    //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //             children: <Widget>[
    //               IconButton(
    //                 color: Colors.white,
    //                   icon: Icon(Icons.arrow_back),
    //                   onPressed: () {
    //                     // Navigator.of(context).pop();
    //                     // Navigator.of(context).push(MaterialPageRoute(builder: (_)=>Profile()));
    //                   }),
    //               Text(
    //                 "Chat",
    //                 style: TextStyle(
    //                     fontSize: (20 / 8.148314082864863) *
    //                         SizeConfig.heightMultiplier,
    //                     fontWeight: FontWeight.w500),
    //               ),

    // IconButton(
    //     icon: Icon(volume),
    //     onPressed: () {
    //       if (volume == Icons.volume_up) {
    //         setState(() {
    //           volume = Icons.volume_off;
    //         });
    //       } else {
    //         setState(() {
    //           volume = Icons.volume_up;
    //         });
    //       }
    //     }),
    //             ],
    //           ),
    //         ),
    //       ],
    //     ),
    //   );
    // }),
  }
}
