import 'package:biznes/CardWidget/cardTile.dart';
import 'package:biznes/CardWidget/gridCardtile.dart';
import 'package:biznes/CardWidget/searchdownwidget.dart';
import 'package:biznes/bloc/Searchproduct/searchbloc.dart';
import 'package:biznes/bloc/Searchproduct/searchevents.dart';
import 'package:biznes/bloc/Searchproduct/searchstates.dart';
import 'package:biznes/helper/allmessageshelper.dart';
import 'package:biznes/model/searchmodel.dart';
import 'package:biznes/products_Detail/extra.dart';
import 'package:biznes/repeatedWigets/customdialog.dart';
import 'package:biznes/repeatedWigets/loadingWidget.dart';
import 'package:biznes/res/color.dart' as font;
import 'package:biznes/model/search_product.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/services/register_logic.dart';
import 'package:biznes/products_Detail/productDetails.dart';
import '../helper/searchhelper.dart';
import 'package:biznes/setting/notification.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/cupertino.dart';
import '../model/bulkmodel.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
// import 'package:circular_check_box/circular_check_box.dart';
import 'package:biznes/helper/productdetailshelper.dart';
import 'package:page_transition/page_transition.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:provider/provider.dart';
import 'package:biznes/helper/login_helper.dart';

class SearchedProducts extends StatefulWidget {
  final String userId;
  final SearchProductModel product;

  SearchedProducts({this.product, this.userId});

  _SearchedProductsState createState() => _SearchedProductsState();
}

class _SearchedProductsState extends State<SearchedProducts> {
  var formKey = GlobalKey<FormState>();
  final _controller = TextEditingController();
  String message;
  double rating = 0;
  final TextStyle dropdownMenuItem =
      TextStyle(color: Colors.black, fontSize: 18);
  final primary = Color(0xff696b9e);
  final secondary = Color(0xfff29a94);
  bool menuON = false;
  bool orientations = true;
  bool checkMessageProduct = false;
  bool isvisible = false;
  bool ismessagesend = false;
  bool isselectall = false;
  bool someBooleanValue = false;
  List<String> title;
  // List<ProductSearchModel> searchmodel = [];
  Bulk bulk;
  getSearchProducts() {
    context.bloc<SearchProductBloc>().add(FetchProduct(
          userId: widget.userId,
          categoryId: widget.product.category,
          subcategoryId: widget.product.subcategory,
          countryId: widget.product.country,
          cityId: widget.product.city,
          roleId: widget.product.userRole,
        ));
  }

  @override
  void initState() {
    getSearchProducts();
    super.initState();
    bulk = Bulk(createdbyid: []);
    title = [];
    // setState(() {
    //   searchmodel = searchmodel;
    // });
  }

  // checking() async {
  //   print(widget.product.country);
  //   SharedPreferences _prefs = await SharedPreferences.getInstance();
  //   Navigator.of(context).push(MaterialPageRoute(
  //       builder: (context) => Membership(
  //           userid: _prefs.getString("userid"),
  //           page: "Searchproductpage",
  //           searchProductModel: widget.product)));
  // }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user;
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    // var searchmodel = Provider.of<SearchHelper>(context).dogs;
    var productdetails = Provider.of<ProductDetailsHelperData>(context);
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Color(0xffE5E5E5),
                    blurRadius: 2,
                    offset: Offset(0.0, 6.0),
                    spreadRadius: 0.1)
              ],
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10.5),
                bottomRight: Radius.circular(10.5),
              )),
          //height: 17 * SizeConfig.heightMultiplier,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            physics: NeverScrollableScrollPhysics(),
            child: Column(
              children: <Widget>[
                CustomAppBar(
                    isShowBackground: true,
                    role: user.roleName,
                    icon: Icon(Icons.arrow_back),
                    IconPressed: () {
                      Navigator.of(context).pop();
                    },
                    context: context,
                    text: "All Results",
                    length: counters.toString(),
                    onTap: () {
                      counterset.zerocunter();
                      Navigator.of(context).push(PageTransition(
                          child: MyNotification(),
                          duration: Duration(milliseconds: 700),
                          type: PageTransitionType.leftToRightWithFade));
                    }),
                menuON
                    ? Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/newback.jpg"),
                          ),
                        ),
                        child: ControlledAnimation(
                          duration: Duration(seconds: (0.5.round())),
                          //curve: Curves.fastLinearToSlowEaseIn,
                          tween: Tween<double>(begin: 1, end: 450),
                          builder: (context, height) {
                            return Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image:
                                      AssetImage("assets/images/newback.jpg"),
                                ),
                              ),
                              child: Stack(
                                children: [
                                  ListView(
                                    shrinkWrap: true,
                                    physics: ScrollPhysics(),
                                    children: [
                                      Container(
                                        child: Row(
                                          children: [],
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(
                                            left: 10, right: 10, top: 5),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height:
                                            40 * SizeConfig.heightMultiplier,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: AssetImage(
                                                "assets/images/newback.jpg"),
                                          ),
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                            bottomLeft: Radius.circular(15),
                                            bottomRight: Radius.circular(15),
                                          ),
                                        ),
                                        child: height >= 100
                                            ? SearchDown(onTap: () {
                                                setState(() {
                                                  menuON = false;
                                                });
                                              }, onTapSecond: () {
                                                setState(() {
                                                  menuON = false;
                                                });
                                              }, onchangedFirst: (value) {
                                                setState(() {
                                                  _controller.text = value;
                                                });
                                                context
                                                    .bloc<SearchProductBloc>()
                                                    .add(FetchProduct(
                                                      userId: widget.userId,
                                                      categoryId: widget
                                                          .product.category,
                                                      subcategoryId: widget
                                                          .product.subcategory,
                                                      query: value,
                                                      countryId: widget
                                                          .product.country,
                                                      cityId:
                                                          widget.product.city,
                                                      roleId: widget
                                                          .product.userRole,
                                                    ));
                                              }, onChangedSecond: (value2) {
                                                setState(() {
                                                  _controller.text = value2;
                                                });
                                                context
                                                    .bloc<SearchProductBloc>()
                                                    .add(FetchProduct(
                                                      userId: widget.userId,
                                                      categoryId: widget
                                                          .product.category,
                                                      subcategoryId: widget
                                                          .product.subcategory,
                                                      query: value2,
                                                      countryId: widget
                                                          .product.country,
                                                      cityId:
                                                          widget.product.city,
                                                      roleId: widget
                                                          .product.userRole,
                                                    ));
                                              })
                                            : SizedBox(
                                                height: 0.0,
                                                width: 0.0,
                                              ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      )
                    : Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage("assets/images/newback.jpg"),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: TextField(
                                controller: _controller,
                                onChanged: (string) {
                                  context
                                      .bloc<SearchProductBloc>()
                                      .add(FetchProduct(
                                        userId: widget.userId,
                                        categoryId: widget.product.category,
                                        subcategoryId:
                                            widget.product.subcategory,
                                        query: string,
                                        countryId: widget.product.country,
                                        cityId: widget.product.city,
                                        roleId: widget.product.userRole,
                                      ));
                                },
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  border: InputBorder.none,
                                  hintStyle: TextStyle(
                                      fontFamily: font.fontfaimlyregular,
                                      fontSize: (16 / 8.148314082864863) *
                                          SizeConfig.heightMultiplier),
                                  focusedBorder: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  errorBorder: InputBorder.none,
                                  disabledBorder: InputBorder.none,
                                  hintText: 'Search by role or product name',
                                  prefixIcon: Icon(
                                    Icons.search,
                                    size: (18 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                MediaQuery.of(context).orientation ==
                                        Orientation.landscape
                                    ? Container()
                                    : menuON
                                        ? IconButton(
                                            icon: Icon(Icons.cancel_outlined),
                                            onPressed: () {
                                              setState(() {
                                                menuON = false;
                                              });
                                            })
                                        : IconButton(
                                            icon: SvgPicture.asset(
                                                "assets/images/filter.svg"),
                                            onPressed: () {
                                              setState(() {
                                                menuON = true;
                                              });
                                            }),
                              ],
                            ),
                          ],
                        ),
                      ),
              ],
            ),
          ),
        ),
        Expanded(
          child: BlocBuilder<SearchProductBloc, SearchProductStates>(
            builder: (BuildContext context,
                SearchProductStates searchProductStates) {
              if (searchProductStates is SearchProductErrorState) {
                final errror = searchProductStates.error;
                return Text(errror.message);
              }
              if (searchProductStates is SearchProductLoadedState) {
                List<ProductSearchModel> searchmodel =
                    searchProductStates.searchProduct;
                return Stack(
                  children: [
                    Scaffold(
                      backgroundColor: Color(0xffffffff),
                      body: OrientationBuilder(builder: (context, orientation) {
                        return Stack(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image:
                                      AssetImage("assets/images/newback.jpg"),
                                ),
                              ),
                              child: ListView(
                                padding: EdgeInsets.zero,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            2.0 * SizeConfig.heightMultiplier,
                                        vertical: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        InkWell(
                                          child: Text(
                                            '✓ Select all',
                                            style: TextStyle(
                                              fontFamily: font.fontfaimly,
                                              color: Colors.green,
                                            ),
                                          ),
                                          onTap: () {
                                            searchmodel.forEach((element) {
                                              final iscontain = bulk.createdbyid
                                                  .contains(
                                                      element.createdById);
                                              if (bulk.createdbyid.isEmpty ||
                                                  bulk.createdbyid.contains(
                                                      element.createdById)) {
                                                bulk.createdbyid.contains(
                                                        element.createdById)
                                                    ? print('add')
                                                    : this.setState(() {
                                                        bulk.createdbyid.add(
                                                            element
                                                                .createdById);
                                                      });
                                              } else if (!iscontain) {
                                                this.setState(() {
                                                  bulk.createdbyid
                                                      .add(element.createdById);
                                                });
                                              } else if (!iscontain) {
                                                this.setState(() {
                                                  bulk.createdbyid.clear();
                                                });
                                              } else {
                                                this.setState(() {
                                                  bulk.createdbyid.remove(
                                                      element.createdById);
                                                });
                                              }
                                            });
                                          },
                                        ),
                                        Text(
                                          '${searchmodel.length} users exist in this list',
                                          style: TextStyle(
                                            color: font.BlueColor,
                                            fontFamily: font.fontfaimlyregular,
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            CupertinoButton(
                                                minSize: double.minPositive,
                                                padding:
                                                    EdgeInsets.only(right: 3.0),
                                                child: Icon(
                                                  Icons.list,
                                                  color: orientations == false
                                                      ? Color(0xff2dcc71)
                                                      : Color(0xffc3cbce),
                                                ),
                                                onPressed: () {
                                                  setState(() {
                                                    orientations = false;
                                                  });
                                                }),
                                            CupertinoButton(
                                                minSize: double.minPositive,
                                                padding: EdgeInsets.zero,
                                                child: Icon(
                                                  Icons.grid_on,
                                                  size: 16,
                                                  color: orientations == false
                                                      ? Color(0xffc3cbce)
                                                      : Color(0xff2dcc71),
                                                ),
                                                onPressed: () {
                                                  setState(() {
                                                    orientations = true;
                                                  });
                                                }),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: orientations
                                        ? 100 * SizeConfig.heightMultiplier
                                        : 60.5 * SizeConfig.heightMultiplier,
                                    width: double.infinity,
                                    child: orientations
                                        ? ListView(
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: AssetImage(
                                                        "assets/images/newback.jpg"),
                                                  ),
                                                ),
                                                padding: EdgeInsets.only(
                                                  //i updated its padding
                                                  top: (8.3932272197492 /
                                                          4.853932272197492) *
                                                      SizeConfig
                                                          .heightMultiplier,
                                                ),
                                                height: 63 *
                                                    SizeConfig.heightMultiplier,
                                                width: double.infinity,
                                                child: searchmodel.isNotEmpty
                                                    ? GridView.builder(
                                                        padding:
                                                            EdgeInsets.zero,
                                                        itemCount:
                                                            searchmodel.length,
                                                        itemBuilder:
                                                            ((BuildContext
                                                                    context,
                                                                int index) {
                                                          return GridCardTile(
                                                              context: context,
                                                              price: searchmodel[
                                                                          index]
                                                                      ?.price
                                                                      ?.toString() ??
                                                                  "",
                                                              image: searchmodel[
                                                                      index]
                                                                  ?.images[0],
                                                              moq:
                                                                  searchmodel[
                                                                          index]
                                                                      ?.moq
                                                                      ?.toString(),
                                                              quantity:
                                                                  searchmodel[
                                                                          index]
                                                                      .quantity
                                                                      .toString(),
                                                              ratings:
                                                                  searchmodel[
                                                                          index]
                                                                      .ratings
                                                                      .toString(),
                                                              ratingsdata:
                                                                  searchmodel[index]
                                                                      .ratings
                                                                      .toString(),
                                                              title:
                                                                  searchmodel[index]
                                                                      .title
                                                                      .toString(),
                                                              userRole:
                                                                  searchmodel[index]
                                                                      .userRole,
                                                              // checkbox:
                                                              //     CircularCheckBox(
                                                              //         inactiveColor: Color(
                                                              //             0xffdfdfdf),
                                                              //         visualDensity:
                                                              //             VisualDensity(
                                                              //           vertical: 2,
                                                              //           horizontal: 2,
                                                              //         ),
                                                              //         materialTapTargetSize:
                                                              //             MaterialTapTargetSize
                                                              //                 .padded,
                                                              //         value: bulk
                                                              //             .createdbyid
                                                              //             .contains(searchmodel[index]
                                                              //                 .createdById),
                                                              //         onChanged: (bool selected) {
                                                              //           if (selected ==
                                                              //               true) {
                                                              //             this.setState(
                                                              //                 () {
                                                              //               bulk.createdbyid.add(
                                                              //                   searchmodel[index]
                                                              //                       .createdById);
                                                              //               print(bulk
                                                              //                   .createdbyid);
                                                              //               print(
                                                              //                   title);
                                                              //             });
                                                              //           } else {
                                                              //             this.setState(
                                                              //                 () {
                                                              //               title.remove(
                                                              //                   searchmodel[index]
                                                              //                       .title);
                                                              //               print(
                                                              //                   title);
                                                              //               bulk.createdbyid
                                                              //                   .remove(
                                                              //                       searchmodel[index].createdById);
                                                              //               print(bulk
                                                              //                   .createdbyid);
                                                              //             });
                                                              //           }
                                                              //         }),
                                                              onTap: () async {
                                                                setState(() {
                                                                  isvisible =
                                                                      true;
                                                                });

                                                                getProductDetails(
                                                                        context:
                                                                            context,
                                                                        productId:
                                                                            searchmodel[index]
                                                                                .sId,
                                                                        userid: user
                                                                            .sId)
                                                                    .then(
                                                                        (singleProductList) {
                                                                  setState(() {
                                                                    isvisible =
                                                                        false;
                                                                  });

                                                                  if (singleProductList !=
                                                                      null) {
                                                                    productdetails
                                                                        .productdetailsfunction(
                                                                            singleProductList);
                                                                    Navigator.push(
                                                                            context,
                                                                            PageTransition(
                                                                                child: ProductDetail(
                                                                                  product: singleProductList,
                                                                                  gettingproductid: searchmodel[index].sId,
                                                                                  userid: user.sId,
                                                                                ),
                                                                                type: PageTransitionType.fade,
                                                                                duration: Duration(milliseconds: 800)))
                                                                        .then((value) {
                                                                      setState(
                                                                          () {
                                                                        Provider.of<SearchHelper>(context,
                                                                                listen: false)
                                                                            .changeSearchString('');
                                                                      });
                                                                    });
                                                                  }
                                                                });
                                                              });
                                                        }),
                                                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                                            childAspectRatio: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width /
                                                                (MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .height /
                                                                    1.3),
                                                            mainAxisSpacing:
                                                                3.0,
                                                            crossAxisSpacing:
                                                                3.0,
                                                            crossAxisCount: 2))
                                                    : Center(
                                                        child: Container(
                                                          alignment:
                                                              Alignment.center,
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: [
                                                              ClipRRect(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              10),
                                                                  child:
                                                                      SvgPicture
                                                                          .asset(
                                                                    "assets/images/productnotfoundupdated.svg",
                                                                    fit: BoxFit
                                                                        .fill,
                                                                  )),
                                                              SizedBox(
                                                                height: 6,
                                                              ),
                                                              Text(
                                                                "Product Not found",
                                                                style:
                                                                    TextStyle(
                                                                  color: font
                                                                      .appbarfontandiconcolor,
                                                                  fontFamily: font
                                                                      .fontfaimlyquicksandbold,
                                                                  fontSize: size
                                                                      .convert(
                                                                          context,
                                                                          20),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                              ),
                                            ],
                                          )
                                        : Container(
                                            child: Container(
                                              color: Colors.white,
                                              child: searchmodel.isNotEmpty
                                                  ? ListView.builder(
                                                      padding: EdgeInsets.zero,
                                                      itemCount:
                                                          searchmodel.length,
                                                      itemBuilder:
                                                          (BuildContext context,
                                                              int index) {
                                                        return buildList(
                                                            context,
                                                            index,
                                                            searchmodel);
                                                      })
                                                  : Center(
                                                      child: Container(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            ClipRRect(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            10),
                                                                child:
                                                                    SvgPicture
                                                                        .asset(
                                                                  "assets/images/productnotfoundupdated.svg",
                                                                  fit: BoxFit
                                                                      .fill,
                                                                )),
                                                            SizedBox(
                                                              height: 6,
                                                            ),
                                                            Text(
                                                              "Product Not found",
                                                              style: TextStyle(
                                                                color: font
                                                                    .appbarfontandiconcolor,
                                                                fontFamily: font
                                                                    .fontfaimlyquicksandbold,
                                                                fontSize: size
                                                                    .convert(
                                                                        context,
                                                                        20),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                            ),
                                          ),
                                  ),
                                ],
                              ),
                            ),
                            bulk.createdbyid == [] || bulk.createdbyid.isEmpty
                                ? Container()
                                : CustomFloatingActionButton(onPressed: () {
                                    searchmodel[0].subscriptionStatus
                                        ? showDialog(
                                            context: context,
                                            builder: (context) {
                                              return CustomDialog(
                                                iscurrentpage: true,
                                                userId: user?.sId ?? "",
                                                categoryId: searchmodel[0]
                                                        ?.categoryId ??
                                                    "",
                                                subCategoryId: searchmodel[0]
                                                        ?.subCategoryId ??
                                                    "",
                                                bulk: bulk ?? [],
                                              );
                                            },
                                          )
                                        : membershipandproductdialog(
                                            contexts: context,
                                            text: "Get Subscription",
                                            page: "sub",
                                            searchProductModel: widget.product,
                                            userid: user.sId,
                                            subtitle:
                                                """Get membership to unblock chat and deal with your vendors for relevent category only""",
                                            image:
                                                "assets/images/NewMembership.svg",
                                          );
                                  }),
                          ],
                        );
                      }),
                      //TODO working

                      // floatingActionButton: bulk.createdbyid == [] ||
                      //         bulk.createdbyid.isEmpty
                      //     ? Container()
//               : FloatingActionButton(
//                   onPressed: () {
//                     print(searchmodel[0].subscriptionStatus);
                      // searchmodel[0].subscriptionStatus
                      //     ? showDialog(
//                             context: context,
//                             builder: (BuildContext context) {
//                               return StatefulBuilder(
//                                 builder: (context, setState) {
//                                   return Dialog(
//                                     shape: RoundedRectangleBorder(
//                                       borderRadius: BorderRadius.circular(12.0),
//                                     ),
//                                     child: Container(
//                                       height: 260.0,
//                                       width: 200.0,
//                                       child: Container(
//                                         margin: EdgeInsets.all(10),
//                                         padding: EdgeInsets.all(10),
//                                         child: Column(children: <Widget>[
//                                           Text(
//                                             "Send Message",
//                                             style: TextStyle(
//                                                 fontFamily: font.fontfaimly,
//                                                 color: Color(0xff394c81),
//                                                 fontSize: 20),
//                                           ),
//                                           SizedBox(
//                                             height: 10,
//                                           ),
//                                           Container(
//                                             alignment: Alignment.bottomLeft,
//                                             decoration: BoxDecoration(
//                                               color: Color(0xfff5f5f5),
//                                               borderRadius:
//                                                   BorderRadius.circular(15),
//                                             ),
//                                             padding: EdgeInsets.all(5.0),
//                                             child: Text(
//                                               """Hi, I am interested in your product, I need some more information about your product. I Look forward to discussing and hearing back from you back.

// Thank you""",
//                                               style: TextStyle(
//                                                   fontFamily:
//                                                       font.fontfaimlyregular,
//                                                   color: Color(0xff3d3d3d)),
//                                             ),
//                                           ),
//                                           SizedBox(
//                                             height: 10,
//                                           ),
//                                           Stack(
//                                             children: [
//                                               filledButton(
//                                                 shadowColor: ismessagesend
//                                                     ? Colors.grey
//                                                     : null,
//                                                 endColor: ismessagesend
//                                                     ? Colors.grey
//                                                     : null,
//                                                 startColor: ismessagesend
//                                                     ? Colors.grey
//                                                     : null,
//                                                 txt: ismessagesend
//                                                     ? "Message Sent"
//                                                     : "Send Message",
//                                                 onTap: () {
//                                                   setState(() {
//                                                     ismessagesend = true;
//                                                   });
//                                                   checkboxmessagemodel(
//                                                     userId: user.sId,
//                                                     categoryId: searchmodel[0]
//                                                         .categoryId,
//                                                     subCategoryId:
//                                                         searchmodel[0]
//                                                             .subCategoryId,
//                                                     bulk: bulk,
//                                                     context: context,
//                                                     message:
//                                                         "Hi, I am interested in your product, I need some more information about your product. I Look forward to discussing and hearing back from you back.Thank you",
//                                                   ).then((value) {
//                                                     if (value == true) {
//                                                       getAllinboxmessagesfrommodel(
//                                                               userid: user.sId,
//                                                               context: context)
//                                                           .then((value) {
//                                                         Provider.of<AllmessagesHelper>(
//                                                                 context,
//                                                                 listen: false)
//                                                             .onmessagesrecieved(
//                                                                 value);
//                                                         Navigator.of(context)
//                                                             .pop();
//                                                       });
//                                                     }
//                                                   });
//                                                 },
//                                               ),
//                                             ],
//                                           )
//                                         ]),
//                                       ),
//                                     ),
//                                   );
//                                 },
//                               );
//                             })
//                         : membershipandproductdialog(
//                             contexts: context,
//                             text: "Get Subscription",
//                             page: "sub",
//                             searchProductModel: widget.product,
//                             userid: user.sId,
//                             subtitle:
//                                 """Get membership to unblock chat and deal with your vendors for relevent category only""",
//                             image: "assets/images/NewMembership.svg",
//                           );
//                   },
//                   child: Container(
//                     height: 68,
//                     width: 68,
//                     decoration: BoxDecoration(
//                       shape: BoxShape.circle,
//                       gradient: LinearGradient(
//                         begin: const Alignment(0.7, -0.5),
//                         end: const Alignment(0.6, 0.5),
//                         colors: [
//                           Color(0xff69bef7),
//                           Color(0xff7595f4),
//                         ],
//                       ),
//                     ),
//                     child: Icon(
//                       Icons.send,
//                     ),
//                   ),
//                 ),
                    ),
                    isvisible
                        ? Stack(
                            children: <Widget>[
                              Container(
                                color: Color(0xffffffff).withOpacity(0.4),
                                height: double.infinity,
                                width: double.infinity,
                                child: Opacity(
                                  opacity: 1.0,
                                  child: Center(
                                      child: Image.asset(
                                    "assets/images/logo2.gif",
                                    height: 200,
                                    width: 100,
                                  )),
                                ),
                              ),
                            ],
                          )
                        : Container(),
                  ],
                );
              }
              return shimmer();
            },
          ),
        ),
      ],
    );
  }

  Widget buildList(BuildContext context, int index, searchmodel) {
    var userid = Provider.of<LoginHelper>(context).user.sId;
    // var searchmodel = Provider.of<SearchHelper>(context).dogs;
    var productdetails = Provider.of<ProductDetailsHelperData>(context);
    return CardTile(
      quantityUnit: searchmodel[index]?.quantityUnit ?? "",
      onTap: () async {
        setState(() {
          isvisible = true;
        });
        getProductDetails(
                context: context,
                productId: searchmodel[index].sId,
                userid: userid)
            .then((singleProductList) {
          productdetails.productdetailsfunction(singleProductList);
          setState(() {
            isvisible = false;
          });
          if (singleProductList != null) {
            Navigator.push(
                    context,
                    PageTransition(
                        child: ProductDetail(
                          product: singleProductList,
                          gettingproductid: searchmodel[index].sId,
                          userid: userid,
                        ),
                        type: PageTransitionType.fade,
                        duration: Duration(milliseconds: 800)))
                .then((value) {
              Provider.of<SearchHelper>(context, listen: false)
                  .changeSearchString('');
            });
          } else {}
        });
      },
      image: searchmodel[index]?.images[0] ?? "",
      title: searchmodel[index]?.title ?? "",
      ratingtext: searchmodel[index]?.ratings.toString() ?? "",
      ratings: searchmodel[index]?.ratings.toString() ?? "",
      country: searchmodel[index]?.country ?? "",
      moq: searchmodel[index]?.moq.toString() ?? "",
      price: searchmodel[index]?.price ?? "",
      quantity: searchmodel[index]?.quantity.toString() ?? "",
      userRole: searchmodel[index]?.userRole ?? "",
      context: context,
      // checkbox: CircularCheckBox(
      //     inactiveColor: Color(0xffdfdfdf),
      //     value: bulk.createdbyid.contains(searchmodel[index].createdById),
      //     onChanged: (bool selected) {
      //       if (selected == true) {
      //         this.setState(() {
      //           bulk.createdbyid.add(searchmodel[index].createdById);
      //           print(bulk.createdbyid);
      //           print(title);
      //         });
      //       } else {
      //         this.setState(() {
      //           title.remove(searchmodel[index].title);
      //           print(title);
      //           bulk.createdbyid.remove(searchmodel[index].createdById);
      //           print(bulk.createdbyid);
      //         });
      //       }
      //     }),
    );
  }
}
