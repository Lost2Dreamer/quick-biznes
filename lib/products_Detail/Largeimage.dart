import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class LargeImage extends StatefulWidget {
  final String image;
  LargeImage(this.image);

  @override
  _LargeImageState createState() => _LargeImageState();
}

class _LargeImageState extends State<LargeImage> {
  // double _scale = 1.0;
  // double _previousScale = 1.0;
  String url;
  String filename;
  var dataBytes;

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                  leading: new Icon(Icons.download_sharp),
                  title: new Text('Download'),
                  onTap: () async {
                    print("1");
                    final status = await Permission.storage
                        .request()
                        .whenComplete(() => print("loaded"));
                    if (status.isGranted) {
                      final externaldirectory = await (Platform.isAndroid
                          ? getExternalStorageDirectory()
                          : getApplicationDocumentsDirectory());
                      final id = FlutterDownloader.enqueue(
                        url: widget.image,
                        savedDir: externaldirectory.path,
                        showNotification: true,
                        openFileFromNotification: true,
                      );
                      id.whenComplete(() => print("downloaded"));
                    }
                    print("3");
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 4.0),
            child: IconButton(
              icon: Icon(Icons.more_vert_rounded),
              onPressed: () {
                _settingModalBottomSheet(context);
              },
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Hero(
          tag: 'imag',
          child: Container(
            alignment: Alignment.center,
            width: double.infinity,
            color: Colors.black,
            child: GestureDetector(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InteractiveViewer(
                    child: CachedNetworkImage(
                      width: double.infinity,
                      imageUrl: widget.image,
                    ),
                  )),
            ),
          ),
        ),
      ),
    );
  }
}
