import 'dart:io';
import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/login_UI/home.dart';
import 'package:biznes/products_Detail/productdetailsexteras.dart';
import 'package:biznes/repeatedWigets/customdialog.dart';
import 'package:biznes/res/color.dart' as font;
import 'package:biznes/helper/productdetailshelper.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:biznes/helper/counthelper.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/services/server.dart';
import 'package:biznes/memberShip/Membership.dart';
import 'package:biznes/model/single_message_model.dart';
import 'package:biznes/products_Detail/Largeimage.dart';
import 'package:biznes/setting/notification.dart';
import 'package:page_transition/page_transition.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

// import 'package:circular_check_box/circular_check_box.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';

// import 'package:biznes/model/search_product.dart';
import 'package:biznes/model/single_Product.dart';
import 'package:biznes/products_Detail/GetAllProductsIndividual.dart';
import 'package:biznes/size_config.dart';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import '../services/register_logic.dart';
import '../products_Detail/extra.dart';

class ProductDetail extends StatefulWidget {
  final SingleProductdetails product;
  final String gettingproductid;
  final String userid;
  final String currentuser;

  ProductDetail({
    this.product,
    this.gettingproductid,
    this.userid,
    this.currentuser,
  });

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  var formKey = GlobalKey<FormState>();
  var commentKey = GlobalKey<FormState>();

  //bool _saveCardInfo = false;
  double rating = 3.0;
  bool isvisible = false;
  double rating2 = 3.0;
  String image;
  String message;
  bool isGetMembership = false;
  bool ismessagesend = false;
  String comment;
  List<Ratings> gettinguseridfromratings;
  int count = 2;
  double heightcontainer = 310;
  double heightcontainer2 = 342;
  String text = 'Load more';
  String textshowless = 'Load less';
  bool showmore = false;
  SingleMessageModel singlemessagemodel;
  String tokens;

  getttingtoken() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    setState(() {
      tokens = _prefs.getString("token");
    });
  }

  Widget horizontalListView(context) {
    var productdetails =
        Provider.of<ProductDetailsHelperData>(context).singleproduct;
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 7.0,
      ),
      width:
          (485.3932272197492 / 4.853932272197492) * SizeConfig.widthMultiplier,
      height: (83 / 8.148314082864863) * SizeConfig.heightMultiplier,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage("assets/images/newback.jpg"),
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Color(0xffEbf3fa),
              blurRadius: 10.0,
              offset: Offset(0.0, 5.0),
              spreadRadius: 0.2)
        ],
        color: Colors.white,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(16.0),
            bottomRight: Radius.circular(16.0)),
      ),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: productdetails.singleProduct[0].images.length,
        itemBuilder: (context, int index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: GestureDetector(
              onTap: () {
                setState(() {
                  image =
                      '$base_url/${productdetails.singleProduct[0].images[index]}';
                });
              },
              child: Container(
                color: Colors.white,
                width: (97 / 4.853932272197492) * SizeConfig.widthMultiplier,
                height: (83 / 8.148314082864863) * SizeConfig.heightMultiplier,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Hero(
                    tag: 'SearchImage',
                    child: CachedNetworkImage(
                        imageUrl:
                            '$base_url/${productdetails.singleProduct[0].images[index]}',
                        fit: BoxFit.cover,
                        placeholder: (context, url) =>
                            Center(child: CircularProgressIndicator()),
                        errorWidget: (context, url, error) =>
                            Image.asset("assets/images/notfound.png")),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void initState() {
    var productdetails =
        Provider.of<ProductDetailsHelperData>(context, listen: false)
            .singleproduct;
    image = "$base_url/${productdetails.singleProduct[0].images[0]}";
    print(productdetails.singleProduct[0].subscriptionStatus);
    singlemessagemodel = SingleMessageModel(
        createdBy: '',
        createdFor: '',
        categoryId: '',
        subCategoryId: '',
        message: '');
    print(productdetails.singleProduct[0].canReview);
    getttingtoken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var user = Provider.of<LoginHelper>(context).user;
    var token = Provider.of<LoginHelper>(context).token;
    var productdetails =
        Provider.of<ProductDetailsHelperData>(context).singleproduct;
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.white,
          body: Builder(
            builder: (context) => ListView(
              children: <Widget>[
                header(context),
                horizontalListView(context),
                detailSection(context, user.sId),
                currentProductProfile(context),
                widget.currentuser == "currentuser"
                    ? Container()
                    : productdetails.singleProduct[0].subscriptionStatus
                        ? Container()
                        : membershipSection(context),
                showCommentAndReviews(context),
                widget.currentuser == "currentuser"
                    ? Container()
                    : productdetails.singleProduct[0].subscriptionStatus ==
                                false &&
                            productdetails.singleProduct[0].canReview == true
                        ? Container()
                        : productdetails.singleProduct[0].subscriptionStatus ==
                                    true &&
                                productdetails.singleProduct[0].canReview ==
                                    true
                            ? postCommentAndReviews(context, user, token)
                            : Container(
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "✪",
                                          style: TextStyle(
                                              color: Color(0xffffcd02)),
                                        ),
                                        Text(
                                          "You already Reviewed the product",
                                          style: TextStyle(
                                            fontFamily: font.fontfaimly,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                  ],
                                ),
                              ),
                Container(height: 10),
              ],
            ),
          ),
        ),
        isvisible ? stack() : Container(),
      ],
    );
  }

  Container header(context) {
    var counters = Provider.of<CountHelper>(context).counter;
    var counterset = Provider.of<CountHelper>(context);
    var user = Provider.of<LoginHelper>(context).user;
    return Container(
      width:
          (485.3932272197492 / 4.853932272197492) * SizeConfig.widthMultiplier,
      height: (290 / 8.148314082864863) * SizeConfig.heightMultiplier,
      child: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(
                builder: (context) => LargeImage(image),
              ));
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Hero(
                tag: 'image',
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  height:
                      (290 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  imageUrl: '$image',
                  placeholder: (context, url) =>
                      Center(child: CircularProgressIndicator()),
                  errorWidget: (context, url, error) =>
                      Image.asset("assets/images/notfound.png"),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10.0, left: 10, right: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                IconButton(
                    icon: Icon(
                      Icons.arrow_back,
                      color: font.appbarfontandiconcolor,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    }),
                Padding(
                  padding: const EdgeInsets.only(top: 2.0),
                  child: GestureDetector(
                    onTap: () {
                      counterset.zerocunter();
                      Navigator.of(context).push(PageTransition(
                          child: MyNotification(),
                          duration: Duration(milliseconds: 700),
                          type: PageTransitionType.leftToRightWithFade));
                    },
                    child: user.roleName == "Guest"
                        ? Container()
                        : Column(
                            children: [
                              NotificationIconWidget(
                                  context: context,
                                  length: counters.toString(),
                                  onTap: () {}),
                            ],
                          ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container detailSection(context, user) {
    var productdetails =
        Provider.of<ProductDetailsHelperData>(context).singleproduct;
    // TextEditingController _messaage = TextEditingController();
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage("assets/images/newback.jpg"),
        ),
      ),
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 10, top: 10.0, right: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Text(
                    "${productdetails.singleProduct[0].title}",
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    style: TextStyle(
                        fontFamily: font.fontfaimlyquicksandbold,
                        fontSize: (25 / 8.148314082864863) *
                            SizeConfig.heightMultiplier,
                        color: Color(0xff394C81)),
                  ),
                ),
                // GestureDetector(
                //   onTap: () async{
                //     await FlutterShare.share(
                //     title: 'Quick Biznes',
                //     text: 'Look at this 👀',
                //     linkUrl: "http://quickbiznes.com",
                //     chooserTitle: 'Example Chooser Title'
                //   );
                //   },
                //   child: isGetMembership
                //       ? Icon(
                //           Icons.share,
                //           color: Colors.blueGrey,
                //           size: (22 / 8.148314082864863) *
                //               SizeConfig.heightMultiplier,
                //         )
                //       : InkWell(
                //           child: Icon(
                //             Icons.share,
                //             color: Colors.green,
                //             size: (25 / 8.148314082864863) *
                //                 SizeConfig.heightMultiplier,
                //           ),
                //         ),
                // ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Row(
              children: <Widget>[
                Text(
                  "${productdetails.singleProduct[0].overAllRatings.toString()}.0",
                  style: TextStyle(
                      fontFamily: font.fontfaimly,
                      fontWeight: FontWeight.w400,
                      fontSize: (14 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                      color: Colors.blueGrey),
                ),
                SmoothStarRating(
                  isReadOnly: true,
                  borderColor: Color(0xffffb900),
                  rating: productdetails.singleProduct[0].overAllRatings == null
                      ? 0.0
                      : double.parse(widget
                          .product.singleProduct[0].overAllRatings
                          .toString()),
                  size: (20 / 8.148314082864863) * SizeConfig.heightMultiplier,
                  color: Color(0xffffb900),
                  filledIconData: Icons.star,
                  defaultIconData: Icons.star_border,
                  starCount: 5,
                  allowHalfRating: true,
                  spacing: 0.5,
                )
              ],
            ),
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  "Description",
                  style: TextStyle(
                    fontSize: size.convert(context, 15),
                    fontFamily: font.fontfaimlyquicksandbold,
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, top: 5),
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: [
                        Flexible(
                          child: Text(
                            "${productdetails.singleProduct[0].description ?? ""}",
                            style: TextStyle(
                                fontFamily: font.fontfaimlyregular,
                                color: Colors.black87,
                                fontSize: (15 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 6.0,
                    ),
                    Text(
                      "Details",
                      style: TextStyle(
                        fontSize: size.convert(context, 15),
                        fontFamily: font.fontfaimlyquicksandbold,
                        color: Colors.black,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        DetailsText(
                            title: "Category:",
                            name:
                                "${productdetails.singleProduct[0].category.title ?? ""}",
                            context: context),
                        DetailsText(
                            title: "Sub-Category:",
                            name:
                                "${productdetails.singleProduct[0].subCategory.title ?? ""}",
                            context: context),
                        DetailsText(
                            title: "Price:",
                            name:
                                "${productdetails.singleProduct[0].price ?? ""}",
                            context: context),
                        DetailsText(
                            title: "Quantity:",
                            name:
                                "${productdetails.singleProduct[0].quantity} (${productdetails.singleProduct[0].quantityUnit ?? "not mention"})",
                            context: context),
                        DetailsText(
                            title: "M.O.Q:",
                            name:
                                "${productdetails.singleProduct[0].moq} (${productdetails.singleProduct[0].quantityUnit ?? "not mention"})",
                            context: context),
                        SizedBox(
                          height: 10,
                        ),
                        widget.currentuser == "currentuser"
                            ? Container()
                            : productdetails.singleProduct[0].subscriptionStatus
                                ? Padding(
                                    padding:
                                        const EdgeInsets.only(bottom: 15.0),
                                    child: Center(
                                      child: filledButton(
                                        onTap: () {
                                          productdetails.singleProduct[0]
                                                  .subscriptionStatus
                                              ? showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return CustomDialog(
                                                      categoryId: widget
                                                          .product
                                                          .singleProduct[0]
                                                          .category
                                                          .sId,
                                                      subCategoryId: widget
                                                          .product
                                                          .singleProduct[0]
                                                          .subCategory
                                                          .sId,
                                                      userId: user,
                                                      iscurrentpage: false,
                                                      createdForId: widget
                                                          .product
                                                          .singleProduct[0]
                                                          .createdBy
                                                          .sId,
                                                    );
                                                  },
                                                )
                                              : Container();
                                        },
                                        txt: 'Send Message',
                                        w: 70 * SizeConfig.widthMultiplier,
                                      ),
                                    ),
                                  )
                                : Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        "Get membership below to unlock chat",
                                        style: TextStyle(
                                            fontFamily: font.fontfaimly,
                                            fontSize: 11,
                                            color: Colors.red),
                                      ),
                                    ],
                                  ),
                      ],
                    ),
                  ],
                ),
                Container(
                  // color: Colors.red,
                  alignment: Alignment.centerRight,
                  // padding: const EdgeInsets.only(right:30.0,),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  currentProductProfile(context) {
    var productdetails =
        Provider.of<ProductDetailsHelperData>(context).singleproduct;
    var token = Provider.of<LoginHelper>(context).token;
    var user = Provider.of<LoginHelper>(context).user;
    return Container(
      padding: const EdgeInsets.only(top: 15.0, bottom: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage("assets/images/newback.jpg"),
        ),
      ),
      child: Container(
        child: GestureDetector(
          onTap: () => widget.currentuser == "currentuser"
              ? Container()
              : user.roleName == "Guest"
                  ? Navigator.of(context, rootNavigator: true)
                      .pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()),
                          (route) => false)
                  : Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => GetAllProducts(
                            username: 'searchinguser',
                            userid:
                                productdetails.singleProduct[0].createdBy.sId,
                            profilepic:
                                "$base_url/${productdetails.singleProduct[0].createdBy.profilePic}",
                            organizationname: productdetails
                                .singleProduct[0].organizationName,
                            description: productdetails
                                .singleProduct[0].createdBy.description,
                            country: productdetails
                                .singleProduct[0].createdBy.countryId.title,
                            city: productdetails
                                .singleProduct[0].createdBy.cityId.title,
                            subscribtion: productdetails
                                .singleProduct[0].subscriptionStatus,
                            gettingproductid: widget.gettingproductid,
                            product: widget.product,
                          ))),
          child: Column(
            children: [
              Divider(),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("assets/images/newback.jpg"),
                  ),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: Color(0xffE5E5E5),
                        blurRadius: 2,
                        offset: Offset(0.0, 6.0),
                        spreadRadius: 0.1)
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15.0),
                      bottomRight: Radius.circular(15.0)),
                ),
                child: Row(
                  children: [
                    Expanded(
                        flex: 1,
                        child: Container(
                          child: Padding(
                            padding: EdgeInsets.only(left: 15.0, bottom: 10.0),
                            child: CircleAvatar(
                              radius: (55 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                              backgroundImage: CachedNetworkImageProvider(
                                  "$base_url/${productdetails.singleProduct[0].createdBy.profilePic}"),
                            ),
                          ),
                        )),
                    Expanded(
                        flex: 2,
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                decoration: productdetails.singleProduct[0]
                                            .subscriptionStatus ||
                                        widget.currentuser == "currentuser"
                                    ? BoxDecoration()
                                    : BoxDecoration(
                                        color: Color(0xffE8E8E8),
                                        borderRadius:
                                            BorderRadius.circular(40)),
                                child: productdetails.singleProduct[0]
                                            .subscriptionStatus ||
                                        widget.currentuser == "currentuser"
                                    ? Text(
                                        productdetails
                                            .singleProduct[0].organizationName,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontFamily:
                                              font.fontfaimlyquicksandbold,
                                        ),
                                      )
                                    : Container(),
                              ),
                              FutureBuilder(
                                future: gettingratingsbyuserid(
                                  productdetails.singleProduct[0].createdBy.sId,
                                ),
                                builder: (BuildContext context,
                                    AsyncSnapshot snapshot) {
                                  Map data = snapshot.data;
                                  return snapshot.hasData
                                      ? Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "${data["averageRating"].toStringAsFixed(1)}",
                                              style: TextStyle(
                                                fontFamily: font.fontfaimly,
                                                fontSize: (16 /
                                                        8.148314082864863) *
                                                    SizeConfig.heightMultiplier,
                                              ),
                                            ),
                                            Center(
                                              child: SmoothStarRating(
                                                isReadOnly: true,
                                                borderColor: Color(0xffffb900),
                                                rating: double.parse(
                                                    "${data["averageRating"].toString() == null ? 0 : data["averageRating"].toString()}"),
                                                size: (25 / 8.148314082864863) *
                                                    SizeConfig.heightMultiplier,
                                                color: Color(0xffffb900),
                                                filledIconData: Icons.star,
                                                halfFilledIconData:
                                                    Icons.star_half,
                                                defaultIconData:
                                                    Icons.star_border,
                                                starCount: 5,
                                                allowHalfRating: true,
                                                spacing: 0.5,
                                              ),
                                            ),
                                          ],
                                        )
                                      : Container(
                                          child: SmoothStarRating(
                                            isReadOnly: true,
                                            borderColor: Color(0xffffb900),
                                            size: (25 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                            color: Color(0xffffb900),
                                            filledIconData: Icons.star,
                                            halfFilledIconData: Icons.star_half,
                                            defaultIconData: Icons.star_border,
                                            starCount: 5,
                                            allowHalfRating: true,
                                            spacing: 0.5,
                                          ),
                                        );
                                },
                              ),
                              productdetails.singleProduct[0]
                                          .subscriptionStatus ||
                                      widget.currentuser == "currentuser"
                                  ? Container(
                                      width: (320 / 4.853932272197492) *
                                          SizeConfig.widthMultiplier,
                                      child: productdetails.singleProduct[0]
                                                  .createdBy.description !=
                                              null
                                          ? Text(
                                              productdetails.singleProduct[0]
                                                      .createdBy.description
                                                      .toString() ??
                                                  "Description",
                                              style: TextStyle(
                                                  fontFamily:
                                                      font.fontfaimlyregular,
                                                  fontSize: (13 /
                                                          8.148314082864863) *
                                                      SizeConfig
                                                          .heightMultiplier),
                                              textAlign: TextAlign.center,
                                            )
                                          : Container(),
                                    )
                                  : Container(),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    productdetails.singleProduct[0]
                                                .subscriptionStatus ||
                                            widget.currentuser == "currentuser"
                                        ? Text(
                                            productdetails.singleProduct[0]
                                                .createdBy.cityId.title,
                                            style: TextStyle(
                                                fontSize: (10 /
                                                        8.148314082864863) *
                                                    SizeConfig.heightMultiplier,
                                                fontFamily: font.fontfaimly,
                                                color: Color(0xff394c81),
                                                fontWeight: FontWeight.w500),
                                          )
                                        : Container(),
                                    SizedBox(
                                      width: (10 / 4.853932272197492) *
                                          SizeConfig.widthMultiplier,
                                    ),
                                    productdetails.singleProduct[0]
                                                .subscriptionStatus ||
                                            widget.currentuser == "currentuser"
                                        ? Icon(
                                            Icons.location_on,
                                            size: (15 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                            color: Color(0xff394c81),
                                          )
                                        : Container(),
                                    productdetails.singleProduct[0]
                                                .subscriptionStatus ||
                                            widget.currentuser == "currentuser"
                                        ? Text(
                                            productdetails.singleProduct[0]
                                                .createdBy.countryId.title,
                                            style: TextStyle(
                                                fontFamily: font.fontfaimly,
                                                fontSize: (11 /
                                                        8.148314082864863) *
                                                    SizeConfig.heightMultiplier,
                                                color: Color(0xff394c81),
                                                fontWeight: FontWeight.w500),
                                          )
                                        : Container()
                                  ],
                                ),
                              )
                            ],
                          ),
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  membershipSection(context) {
    var user = Provider.of<LoginHelper>(context).user;
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage("assets/images/newback.jpg"),
        ),
      ),
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SvgPicture.asset("assets/images/NewMembership.svg"),
          Text(
            "Get Subscription",
            style: TextStyle(
                color: Color(0xff394c81),
                fontSize:
                    (24 / 8.148314082864863) * SizeConfig.heightMultiplier,
                fontFamily: font.fontfaimlyquicksandbold,
                fontWeight: FontWeight.w400),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              "Get membership to unblock chat and deal with your vendors for relevent category only",
              textAlign: TextAlign.center,
              maxLines: 2,
              style: TextStyle(
                fontFamily: font.fontfaimlyregular,
                fontSize: 14,
                color: Colors.blueGrey,
              ),
            ),
          ),
          SizedBox(
            height: (10 / 8.148314082864863) * SizeConfig.heightMultiplier,
          ),
          filledButton(
            txt: "GET FREE TRAIL",
            onTap: () {
              user.roleName == "Guest"
                  ? Navigator.of(context, rootNavigator: true)
                      .pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()),
                          (route) => false)
                  : Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => Membership(
                        userid: user.sId,
                        singleproduct: widget.product,
                        gettingproductid: widget.gettingproductid,
                      ),
                    ));
            },
            w: size.convert(context, 300),
          ),
          SizedBox(
            height: (20 / 8.148314082864863) * SizeConfig.heightMultiplier,
          ),
        ],
      ),
    );
  }

  Widget showCommentAndReviews(context) {
    String userid = Provider.of<LoginHelper>(context).user.sId;
    var productdetails =
        Provider.of<ProductDetailsHelperData>(context).singleproduct;
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage("assets/images/newback.jpg"),
        ),
      ),
      child: FutureBuilder(
          future: getdata(productdetails.singleProduct[0].sId, userid, context),
          builder:
              (BuildContext context, AsyncSnapshot<List<Ratings>> snapshot) {
            List<Ratings> data = snapshot.data;
            return snapshot.hasData
                ? snapshot.data.length == 0
                    ? Container(
                        child: Center(
                          child: Text("No reviews",
                              style: TextStyle(
                                fontSize: (18 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                              )),
                        ),
                      )
                    : !snapshot.hasData
                        ? Container(
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          )
                        : snapshot.data.length == 1
                            ? Column(
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 12.0),
                                        child: Text(
                                          "All Reviews",
                                          style: TextStyle(
                                            fontFamily:
                                                font.fontfaimlyquicksandbold,
                                            color: Color(0xff394C81),
                                            fontSize: (18 / 8.148314082864863) *
                                                SizeConfig.heightMultiplier,
                                          ),
                                        ),
                                      ),
                                      Text(
                                        '(${snapshot.data.length.toString()})',
                                        style: TextStyle(
                                          color: Color(0xff394C81),
                                          fontFamily: font.fontfaimly,
                                          fontSize: (12 / 8.148314082864863) *
                                              SizeConfig.heightMultiplier,
                                        ),
                                      ),
                                    ],
                                  ),
                                  RatingData(
                                    fullname: data[0].commentedBy.fullName,
                                    ratings: data[0].ratings,
                                    ratingstar: data[0].ratings,
                                    onupdateratings: (ratings) {
                                      setState(() {
                                        rating2 = ratings;
                                      });
                                    },
                                    comments: data[0].comments,
                                    profileimage:
                                        "$base_url/${data[0].commentedBy.profilePic}",
                                  ),
                                ],
                              )
                            : snapshot.hasError
                                ? Container(
                                    child: Center(
                                      child: CircularProgressIndicator(),
                                    ),
                                  )
                                : snapshot.data.length == 2
                                    ? Column(
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              SizedBox(
                                                width: 2,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 12.0),
                                                child: Text(
                                                  "All Reviews",
                                                  style: TextStyle(
                                                    color: Color(0xff394C81),
                                                    fontFamily: font
                                                        .fontfaimlyquicksandbold,
                                                    fontSize: (18 /
                                                            8.148314082864863) *
                                                        SizeConfig
                                                            .heightMultiplier,
                                                  ),
                                                ),
                                              ),
                                              Text(
                                                '(${snapshot.data.length.toString()})',
                                                style: TextStyle(
                                                  fontSize:
                                                      (14 / 8.148314082864863) *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                  fontFamily: font.fontfaimly,
                                                ),
                                              ),
                                            ],
                                          ),
                                          RatingData(
                                            fullname:
                                                data[0].commentedBy.fullName,
                                            ratings: data[0].ratings,
                                            ratingstar: data[0].ratings,
                                            onupdateratings: (ratings) {
                                              setState(() {
                                                rating2 = ratings;
                                              });
                                            },
                                            comments: data[0].comments,
                                            profileimage:
                                                "$base_url/${data[0].commentedBy.profilePic}",
                                          ),
                                          SizedBox(
                                            height: 8.0,
                                          ),
                                          RatingData(
                                            fullname:
                                                data[1].commentedBy.fullName,
                                            ratings: data[1].ratings,
                                            ratingstar: data[1].ratings,
                                            onupdateratings: (ratings) {
                                              setState(() {
                                                rating2 = ratings;
                                              });
                                            },
                                            comments: data[1].comments,
                                            profileimage:
                                                "$base_url/${data[1].commentedBy.profilePic}",
                                          ),
                                        ],
                                      )
                                    : Container(
                                        height: (showmore
                                                ? heightcontainer2 /
                                                    8.148314082864863
                                                : heightcontainer /
                                                    8.148314082864863) *
                                            SizeConfig.heightMultiplier,
                                        child: Column(
                                          mainAxisSize: MainAxisSize.max,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                SizedBox(
                                                  width: 2,
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 12.0),
                                                  child: Text(
                                                    "All Reviews",
                                                    style: TextStyle(
                                                      color: Color(0xff394C81),
                                                      fontSize: (18 /
                                                              8.148314082864863) *
                                                          SizeConfig
                                                              .heightMultiplier,
                                                    ),
                                                  ),
                                                ),
                                                Text(
                                                  "(${(data.length.toString())})",
                                                  style: TextStyle(
                                                    fontSize: (12 /
                                                            8.148314082864863) *
                                                        SizeConfig
                                                            .heightMultiplier,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              child: Expanded(
                                                child: ListView.builder(
                                                    itemCount: showmore
                                                        ? data.length
                                                        : count,
                                                    itemBuilder:
                                                        (context, int index) {
                                                      return RatingData(
                                                        fullname: data[index]
                                                            .commentedBy
                                                            .fullName,
                                                        ratings:
                                                            data[index].ratings,
                                                        ratingstar:
                                                            data[index].ratings,
                                                        onupdateratings:
                                                            (ratings) {
                                                          setState(() {
                                                            rating2 = ratings;
                                                          });
                                                        },
                                                        comments: data[index]
                                                            .comments,
                                                        profileimage:
                                                            "$base_url/${data[index].commentedBy.profilePic}",
                                                      );
                                                    }),
                                              ),
                                            ),
                                            InkWell(
                                              child: Center(
                                                child: Text(
                                                  showmore
                                                      ? textshowless
                                                      : text,
                                                  style: TextStyle(
                                                    fontFamily:
                                                        font.fontfaimlyregular,
                                                    fontSize: (18 /
                                                            8.148314082864863) *
                                                        SizeConfig
                                                            .heightMultiplier,
                                                  ),
                                                ),
                                              ),
                                              onTap: () {
                                                setState(() {
                                                  showmore = !showmore;
                                                });
                                              },
                                            ),
                                          ],
                                        ),
                                      )
                : Container(
                    child: Center(
                      child: Text("No Reviews",
                          style: TextStyle(
                            fontSize: (18 / 8.148314082864863) *
                                SizeConfig.heightMultiplier,
                          )),
                    ),
                  );
          }),
    );
  }

  Padding postCommentAndReviews(context, var userId, String token) {
    var productdetails =
        Provider.of<ProductDetailsHelperData>(context).singleproduct;
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Container(
        width: (485.3932272197492 / 4.853932272197492) *
            SizeConfig.widthMultiplier,
        height: (300 / 8.148314082864863) * SizeConfig.heightMultiplier,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage("assets/images/newback.jpg"),
          ),
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 3.0),
              child: Text(
                "Add your Review",
                style: TextStyle(
                    fontFamily: font.fontfaimlyquicksandbold,
                    color: Color.fromRGBO(57, 76, 129, 1),
                    fontSize:
                        (18 / 8.148314082864863) * SizeConfig.heightMultiplier,
                    fontWeight: FontWeight.w600),
              ),
            ),
            Center(
              child: RatingBar(
                ratingWidget: RatingWidget(
                  empty: Icon(
                    Icons.star,
                    color: Colors.grey,
                  ),
                  full: Icon(
                    Icons.star,
                    color: Color(0xffffb900),
                  ),
                  half: Icon(
                    Icons.star,
                    color: Color(0xffffb900),
                  ),
                ),
                // updateOnDrag: true,
                itemSize:
                    (24 / 8.148314082864863) * SizeConfig.heightMultiplier,
                initialRating: 0,
                unratedColor: Colors.grey,
                direction: Axis.horizontal,
                allowHalfRating: false,
                itemCount: 5,

                // itemBuilder: (context, _) => Icon(
                //   Icons.star,
                //   color: Color(0xffffb900),
                // ),
                onRatingUpdate: (ratings) {
                  setState(() {
                    rating = ratings;
                  });
                },
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0, bottom: 5),
              child: Text(
                "Add Comment",
                style: TextStyle(
                    fontFamily: font.fontfaimly,
                    color: Colors.black,
                    fontSize:
                        (14 / 8.148314082864863) * SizeConfig.heightMultiplier,
                    fontWeight: FontWeight.w600),
              ),
            ),
            Form(
              key: commentKey,
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, bottom: 5),
                child: Container(
                  padding: EdgeInsets.only(left: 5, right: 5),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Color(0xffebf3fa)),
                  child: TextFormField(
                    textDirection: TextDirection.ltr,
                    decoration: InputDecoration(
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                      border: InputBorder.none,
                      errorMaxLines: 1,
                      errorStyle: TextStyle(),
                    ),
                    maxLines: 4,
                    validator: (value) {
                      if (value.isEmpty && value.length < 10) {
                        return 'field required & atleast length 10 char';
                      }
                      return null;
                    },
                    onSaved: (value) {
                      setState(() {
                        comment = value;
                      });
                    },
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Center(
              child: filledButton(
                txt: 'Post Review',
                startColor: Color(0xffffcd02),
                endColor: Color(0xffffcd02),
                shadowColor: Color(0xffffde66),
                onTap: () async {
                  if (commentKey.currentState.validate()) {
                    commentKey.currentState.save();
                    setState(() {
                      isvisible = true;
                    });
                    Map data = {
                      'commentedBy': userId.sId,
                      'comments': comment,
                      'ratings': rating.toString(),
                      'productId': productdetails.singleProduct[0].sId,
                    };

                    var commentUrl = '$base_url/product/review/post';
                    var response =
                        await http.post(commentUrl, body: data, headers: {
                      HttpHeaders.authorizationHeader: tokens,
                    });
                    var dataresponse = jsonDecode(response.body);
                    if (dataresponse.isNotEmpty) {
                      Navigator.of(context).pop();
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => ProductDetail(
                                product: widget.product,
                              )));
                      setState(() {
                        isvisible = false;
                      });
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (context) {
                            return Dialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: SingleChildScrollView(
                                padding: EdgeInsets.zero,
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(
                                      height: 20,
                                    ),
                                    SvgPicture.asset(
                                      "assets/images/ProductReviewed.svg",
                                      height: 100,
                                    ),
                                    Container(
                                      child: Text(
                                        "Product Reviewed",
                                        style: TextStyle(
                                          fontFamily:
                                              font.fontfaimlyquicksandbold,
                                          color: Color(0xff394c81),
                                          fontSize: (22 / 8.148314082864863) *
                                              SizeConfig.heightMultiplier,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      "Thanks for your review on product",
                                      style: TextStyle(
                                        fontFamily: font.fontfaimlyregular,
                                        fontSize: (16 / 8.148314082864863) *
                                            SizeConfig.heightMultiplier,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    filledButton(
                                      txt: "Done",
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                  ],
                                ),
                              ),
                            );
                          });
                    }
                    productdetails.singleProduct[0].canReview = false;
                    print(dataresponse.toString());
                  } else {}
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
