import 'package:biznes/res/color.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:biznes/memberShip/Membership.dart';
import 'package:biznes/model/search_product.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:biznes/res/color.dart' as font;
import 'package:biznes/res/size.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/repeatedWigets/filledButton.dart';
import 'package:biznes/add_product/addProduct.dart';

class RatingData extends StatelessWidget {
  final String fullname;
  final String ratings;
  final String ratingstar;
  final Function onupdateratings;
  final String comments;
  final String profileimage;

  RatingData({
    this.fullname,
    this.ratings,
    this.ratingstar,
    this.onupdateratings,
    this.comments,
    this.profileimage,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: (150/8.148314082864863)*SizeConfig.heightMultiplier,
      child: Column(
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 5),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 14, horizontal: 5),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.grey,
                          blurRadius: 1.0,
                          offset: Offset(0.0, 1.0),
                          spreadRadius: 0.1)
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        CircleAvatar(
                          radius: (40 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                          backgroundImage:
                              CachedNetworkImageProvider(profileimage),
                        ),
                        SizedBox(
                          height: (5 / 8.148314082864863) *
                              SizeConfig.heightMultiplier,
                        ),
                        Text(
                          fullname.substring(0).toUpperCase(),
                          style: style.QuicksandBold(
                              fontFamily: font.fontfaimlyquicksandbold,
                              fontSize: (14 / 8.148314082864863) *
                                  SizeConfig.heightMultiplier,
                              color: Color(0xff394c81)),
                        )
                      ],
                    ),
                    SizedBox(
                      width: (14 / 8.148314082864863) *
                          SizeConfig.heightMultiplier,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                ratings,
                                maxLines: 3,
                                style: TextStyle(
                                    fontFamily: font.fontfaimly,
                                    fontSize: (14 / 8.148314082864863) *
                                        SizeConfig.heightMultiplier,
                                    fontWeight: FontWeight.w400,
                                    color: Colors.blueGrey),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              SmoothStarRating(
                                color: Color(0xffffb900),
                                filledIconData: Icons.star,
                                defaultIconData: Icons.star_border,
                                starCount: 5,
                                allowHalfRating: true,
                                spacing: 0.5,
                                isReadOnly: true,
                                borderColor: Color(0xffffb900),
                                size: (18 / 8.148314082864863) *
                                    SizeConfig.heightMultiplier,
                                rating: double.parse(ratingstar),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Container(
                            width: 57 * SizeConfig.widthMultiplier,
                            child: Text(
                              '$comments',
                              style: style.MontserratRegular(
                                  fontSize: 12,
                                  fontFamily: font.fontfaimlyregular),
                              textAlign: TextAlign.start,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Text textinchat(String text) {
  return Text(
    text,
    style: TextStyle(
      fontFamily: 'MontserratRegular',
      color: Colors.black,
      fontSize: 12,
    ),
  );
}

Text textforsender(String text, {bool isoffer = false}) {
  return Text(
    text,
    style: TextStyle(
      fontFamily: 'MontserratRegular',
      color: isoffer ? Colors.black : Colors.white,
      fontSize: 12,
    ),
  );
}

offerText(context, text) {
  return Row(
    children: [
      Text(
        "$text",
        style: TextStyle(
            color: font.appbarfontandiconcolor,
            fontFamily: font.fontfaimlyquicksandbold,
            fontSize: size.convert(context, 16)),
      ),
    ],
  );
}

showdialogfordeleting({context, onPressed, String text, String title}) {
  showDialog(
    context: context,
    builder: (context) {
      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: size.convert(context, 20),
              ),
              SvgPicture.asset("assets/images/Delete.svg"),
              SizedBox(
                height: size.convert(context, 5),
              ),
              Text(
                title,
                style: TextStyle(
                  fontFamily: font.fontfaimlyquicksandbold,
                  color: font.appbarfontandiconcolor,
                  fontSize: size.convert(context, 22),
                ),
              ),
              Text(
                text,
                style: TextStyle(
                  fontFamily: font.fontfaimlyregular,
                ),
              ),
              SizedBox(
                height: size.convert(context, 15),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  filledButton(
                    shadowColor: Colors.white,
                    onTap: () {
                      Navigator.of(context).pop();
                      onPressed();
                    },
                    w: size.convert(context, 80),
                    fontsize: size.convert(context, 12),
                    h: size.convert(context, 30),
                    txt: "Delete",
                  ),
                  filledButton(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    fontsize: size.convert(context, 12),
                    txt: "Cancel",
                    shadowColor: Colors.white,
                    startColor: Color(0xffffcd02),
                    h: size.convert(context, 30),
                    endColor: Color(0xffffcd02),
                    w: size.convert(context, 80),
                  ),
                ],
              ),
              SizedBox(
                height: size.convert(context, 20),
              ),
            ],
          ),
        ),
      );
    },
    barrierDismissible: false,
  );
}

membershipandproductdialog(
    {contexts,
    text,
    subtitle,
    image,
    String page,
    String userid,
    SearchProductModel searchProductModel}) {
  return showDialog(
      context: contexts,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          child: SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: size.convert(context, 20)),
                  Container(
                    child: SvgPicture.asset(
                      "$image",
                      height: 100,
                      width: 100,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text(
                    "$text",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: font.fontfaimlyquicksandbold,
                        color: Color(0xff394c81),
                        fontSize: size.convert(context, 25)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Text(
                      "$subtitle",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: font.fontfaimlyregular,
                        fontSize: size.convert(context, 12),
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  filledButton(
                    txt: page == "sub"
                        ? "GET FREE TRAIL"
                        : page == "add"
                            ? "Add Product"
                            : "Done",
                    onTap: () {
                      if (page == "add") {
                        Navigator.of(context).pop();
                        Navigator.of(contexts).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AddProduct('membership')));
                      } else if (page == "sub") {
                        Navigator.of(context).pop();
                        Navigator.of(contexts).push(MaterialPageRoute(
                            builder: (context) => Membership(
                                userid: userid,
                                page: "Searchproductpage",
                                searchProductModel: searchProductModel)));
                      } else {
                        Navigator.of(context).pop();
                      }
                    },
                  ),
                  SizedBox(height: size.convert(context, 20)),
                ],
              ),
            ),
          ),
        );
      });
}

Material material({border}) {
  return Material(
    shadowColor: Color(0xffd4d4d4),
    elevation: 5,
    borderRadius: BorderRadius.circular(border ?? 25),
    child: Container(
      height: 48,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(border ?? 30.0),
      ),
    ),
  );
}

BoxDecoration decoration() {
  return BoxDecoration(
    borderRadius: BorderRadius.circular(25.0),
    gradient: LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [
        Color(0xFF69bef7),
        Color(0xFF2a65ff),
      ],
    ),
    boxShadow: [
      BoxShadow(
        color: Color(0xFF69bef7),
        spreadRadius: 1,
        blurRadius: 12,
        offset: Offset(0, 2), // changes position of shadow
      ),
    ],
  );
}

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(25),
    borderSide: BorderSide(color: Colors.red),
  );
}

Stack stack() {
  return Stack(
    children: <Widget>[
      Container(
        color: Color(0xffffffff).withOpacity(0.4),
        height: double.infinity,
        width: double.infinity,
        child: Opacity(
          opacity: 1.0,
          child: Center(
              child: Image.asset(
            "assets/images/logo2.gif",
            height: 200,
            width: 100,
          )),
        ),
      ),
    ],
  );
}

Widget NotificationIconWidget({context, String length, Function onTap}) {
  return GestureDetector(
    onTap: onTap(),
    child: Center(
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Center(
            child: Container(
              color: Colors.transparent,
              alignment: Alignment.bottomCenter,
              height: size.convert(context, 44),
              width: size.convertWidth(context, 38),
              child: Center(
                child: SvgPicture.asset(
                  "assets/icons/ic_notifications_active_24px.svg",
                  color: font.appbarfontandiconcolor,
                ),
              ),
            ),
          ),
          length == "0" || length == null || length == "null"
              ? Container()
              : Container(
                  height: size.convert(context, 15),
                  width: size.convertWidth(context, 15),
                  decoration: BoxDecoration(
                    color: font.tabbarcolor,
                    shape: BoxShape.circle,
                  ),
                  child: Center(
                    child: Text(
                      length ?? "1",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: size.convert(context, 7),
                      ),
                    ),
                  ),
                ),
          // CircleAvatar(
          //   radius: size.convert(context, 10),
          // ),
        ],
      ),
    ),
  );
}

Widget CustomAppBar(
    {context,
    String text,
    Function onTap,
    length,
    Icon icon,
    String role,
    bool isShowBackground = false,
    Function IconPressed}) {
  return Container(
    decoration: BoxDecoration(
      color: Colors.transparent,
      image: isShowBackground
          ? DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage("assets/images/newback.jpg"),
            )
          : null,
    ),
    padding: EdgeInsets.only(
        top: size.convert(context, 40),
        left: size.convert(context, 10),
        right: size.convert(context, 10)),
    height: size.convert(context, 80),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        icon == null
            ? Container(
                height: size.convert(context, 15),
                width: size.convertWidth(context, 15),
              )
            : IconButton(
                highlightColor: Colors.white,
                icon: icon,
                color: Color(0xff394c81),
                onPressed: () => IconPressed()),
        Center(
          child: Text(
            text ?? "",
            style: TextStyle(
              fontSize: size.convert(context, 21),
              fontFamily: font.fontfaimlyquicksandbold,
              color: Color(0xff394c81),
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        role == "Guest"
            ? Container(
                height: size.convert(context, 15),
                width: size.convertWidth(context, 15),
              )
            : NotificationIconWidget(
                context: context, length: length, onTap: () => onTap),
      ],
    ),
  );
}

Widget TextFieldAndText({
  String title,
  String hintText,
  Function onSaved,
  Function validator,
  TextInputType inputtype,
  context,
  border,
  Function onChanged,
}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        child: Text(
          title ?? "",
          style: style.MontserratMedium(
            fontSize: size.convert(context, 14),
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(2.0),
        child: Stack(
          children: [
            material(border: border),
            TextFormField(
              onChanged: onChanged,
              keyboardType: inputtype,
              decoration: InputDecoration(
                errorStyle: style.MontserratRegular(
                    color: Colors.red, fontSize: size.convert(context, 12)),
                errorBorder: outlineInputBorder(),
                focusedErrorBorder: outlineInputBorder(),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(border ?? 25),
                  borderSide: BorderSide(color: Color(0xff69BEF7)),
                ),
                hintStyle: style.MontserratRegular(
                    fontSize: size.convert(context, 12)),
                contentPadding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(border ?? 30.0),
                ),
                hintText: hintText,
              ),
              onSaved: onSaved,
              validator: validator,
            ),
          ],
        ),
      ),
    ],
  );
}

Row TextForInbox(
  name,
  title,
  context,
) {
  return Row(
    children: [
      Text(
        "$name" + title ?? "",
        style: TextStyle(
            fontSize: size.convert(context, 7),
            fontWeight: FontWeight.w400,
            color: Colors.black,
            fontFamily: 'MontserratRegular'),
      ),
    ],
  );
}

settingpagefonts(String text) {
  return Text(
    text,
    style: TextStyle(
      fontSize: (18 / 8.148314082864863) * SizeConfig.heightMultiplier,
      fontFamily: font.fontfaimly,
    ),
  );
}

String howItWorkText =
    """Quick Biznes is an online trading messenger and to fully understand its usage the user must go through the following process listed below:

1.	First user needs to sign up and fill up all necessary details about them selves and about the company as provided.
 
2.	Save your PIN number for your security as provided. 

3.	Go to profile section and add your product by filling up all necessary requirements about your product.

4.	Quick Biznes team will review your product which may take a while, and will notify you about your product approval. 

5.	You are now all set and will be able to send message individually in product section or to the entire category in (Send Message) section. Furthermore, user must know that in chat section on top, you can see enquiry section, this will show you details regarding how many users have accepted, rejected your message or are still pending for approval.

6.	In order to send a message, it is mandatory to Subscribe which is absolutely free for limited time period. Simply choose any plan and select your product which you want to subscribe and for your ease add additional bonus sub-categories to whom you also want to send messages apart from your product sub category. 

7.	In setting section, you can contact us whether it is about knowing more or any complaints on any aspect.

8.	You will receive notification time to time from Quick Biznes regarding updates about our app or any necessary information.
""";

Container AddImageContainer({Function onPressed}) {
  return Container(
      height: (80 / 8.148314082864863) * SizeConfig.heightMultiplier,
      width: (80 / 4.853932272197492) * SizeConfig.widthMultiplier,
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xff69bef7), Color(0xff7595f4)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          border: Border.all(color: Color(0xfff1f1f1)),
          borderRadius: BorderRadius.circular(20)),
      child: new IconButton(
        onPressed: onPressed,
        icon: Icon(
          Icons.add_to_photos,
          size: (25 / 8.148314082864863) * SizeConfig.heightMultiplier,
          color: Colors.white,
        ),
      ));
}
