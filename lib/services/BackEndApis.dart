import 'package:http/http.dart'as http;
import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
class BackEndApi{
  String baseurl;
  String endpoint;
  BackEndApi({this.baseurl,this.endpoint});
  Future getData()async{
    print(endpoint);
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String tokens = _prefs.getString("token");
    var request=await http.get(endpoint,headers: {HttpHeaders.authorizationHeader: tokens});
    return jsonDecode(request.body);
  }
}