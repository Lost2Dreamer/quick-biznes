import 'package:biznes/bloc/CurrrentUserMemberShip/CurrrentUserMemberShipBLoc.dart';
import 'package:biznes/bloc/Searchproduct/searchbloc.dart';
import 'package:biznes/bloc/inbox/ChatCategoriestile.dart/Categoriesbloc.dart';
import 'package:biznes/bloc/inbox/bloc.dart';
import 'package:biznes/helper/categories.dart';
import 'package:biznes/helper/gettingproducts.dart';
import 'package:biznes/helper/productdetailshelper.dart';
import 'package:biznes/helper/login_helper.dart';
import 'package:biznes/login_UI/forgotPassword.dart';
import 'package:biznes/login_UI/home.dart';
import 'package:biznes/login_UI/pinCodeVerification.dart';
import 'package:biznes/register/register.dart';
import 'package:biznes/splashScreen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'helper/conversationhelper.dart';
import 'helper/notificationsoundhelper.dart';
import 'helper/categoryInboxHelper.dart';
import 'package:biznes/login_UI/signIn.dart';
import 'package:biznes/helper/bottomnavigationbarhelper.dart';
import 'package:biznes/product_UI/product_home.dart';
import 'package:biznes/helper/profileupdatehelper.dart';
import 'package:biznes/helper/searchhelper.dart';
import 'package:biznes/size_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'helper/allmessageshelper.dart';
import 'helper/tab_bar.dart';
import 'helper/notificationhelper.dart';
import 'package:biznes/helper/counthelper.dart';
// import 'package:device_preview/device_preview.dart';
import 'package:flutter/services.dart';

import 'model/conversationmodel.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final docPath = await getApplicationDocumentsDirectory();
  Hive.init(docPath.path);
  Hive.registerAdapter(conversationsGeneration());
  Hive.registerAdapter(conversationsGeneration2());
  Hive.registerAdapter(conversationsGeneration3());
  Hive.registerAdapter(conversationsGeneration4());
  Hive.registerAdapter(conversationsGeneration5());
  await Hive.openBox('conversationsGeneration');
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
    statusBarBrightness: Brightness.dark,
    // statusBarIconBrightness: Brightness.light,
  ));
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<AlbumBloc>(
        create: (BuildContext context) => AlbumBloc(),
      ),
      BlocProvider<CategoriesInboxBloc>(
        create: (BuildContext context) => CategoriesInboxBloc(),
      ),
      BlocProvider<SearchProductBloc>(
        create: (BuildContext context) => SearchProductBloc(),
      ),
      BlocProvider<CurrrentUserMemberShipBloc>(
        create: (BuildContext context) => CurrrentUserMemberShipBloc(),
      )
    ],
    child: MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: LoginHelper()),
        ChangeNotifierProvider.value(value: ProductDetailsHelperData()),
        ChangeNotifierProvider.value(value: SocketMessaging()),
        ChangeNotifierProvider.value(value: CountHelper()),
        ChangeNotifierProvider.value(value: GettingProducts()),
        ChangeNotifierProvider.value(value: CategoriesData()),
        ChangeNotifierProvider.value(value: UpdatedProfile()),
        ChangeNotifierProvider.value(value: SearchHelper()),
        ChangeNotifierProvider.value(value: NewMessagesHlper()),
        ChangeNotifierProvider.value(value: CategoryHelper()),
        ChangeNotifierProvider.value(value: BottomNavigationBarHelper()),
        ChangeNotifierProvider.value(value: Notificationgetting()),
      ],
      child: MyApp(),
    ),
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizeConfig().init(constraints, orientation);
            return MaterialApp(
              title: "Quick Biznes",
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primarySwatch: Colors.blue,
              ),
              home: LoaderClass(),
              routes: <String, WidgetBuilder>{
                '/loginScreen': (BuildContext context) => LoginScreen(),
                '/signIn': (BuildContext context) => SignIn(),
                '/register': (BuildContext context) => Register(),
                '/confirmationpassword': (BuildContext context) =>
                    ConfirmationPassword(),
                '/forgotPassword': (BuildContext context) => ForgotPassword(),
                '/newPassword': (BuildContext context) => NewPassword(),
                '/productHome': (BuildContext context) => ProductHome(),
              },
            );
          },
        );
      },
    );
  }
}

class LoaderClass extends StatefulWidget {
  @override
  _LoaderClassState createState() => _LoaderClassState();
}

class _LoaderClassState extends State<LoaderClass> {
  String getuserid;
  bool showSplash = true;
  getdataforuserid() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    setState(() {
      getuserid = _prefs.getString("userid");
    });
  }

  @override
  void initState() {
    getdataforuserid();
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        showSplash = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: showSplash
          ? splashScreen()
          : getuserid != null
              ? PinVerification()
              : LoginScreen(),
    );
  }
}
