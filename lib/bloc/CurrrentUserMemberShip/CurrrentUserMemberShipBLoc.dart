import 'dart:io';
import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/bloc/CurrrentUserMemberShip/CurrrentUserMemberShipStates.dart';
import 'package:biznes/bloc/Exceptions.dart';
import 'package:biznes/model/membershitpdetailmodel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'CurrrentUserMemberShipEvent.dart';

class CurrrentUserMemberShipBloc
    extends Bloc<FetchCurrrentUserMemberShip, CurrrentUserMemberShipStates> {
  List<Membershipdetailmodel> membershipDetail;
  CurrrentUserMemberShipBloc()
      : super(CurrrentUserMemberShipStatesInitailState());
  @override
  Stream<CurrrentUserMemberShipStates> mapEventToState(
      FetchCurrrentUserMemberShip event) async* {
    if (event is FetchMembershipCurrrentUserMemberShip) {
      yield CurrrentUserMemberShipStatessLoadingState();
      try {
        membershipDetail = await getmembershipdetails(event.userId);
        print("heelo");
        print(membershipDetail.length);
        yield CurrrentUserMemberShipStatesLoadedState(
            memberShipDetails: membershipDetail);
      } on SocketException {
        yield CurrrentUserMemberShipErrorState(
            error: NoInterNetException(message: "No Internet Connection"));
      } on HttpException {
        yield CurrrentUserMemberShipErrorState(
            error:
                NoServiceFoundException(message: "No Service Found Exception"));
      } on FormatException {
        yield CurrrentUserMemberShipErrorState(
            error: InvalidFormatException(message: "Invalid Format Exception"));
      } catch (e) {
        yield CurrrentUserMemberShipErrorState(
            error: UnknownException(message: "Unknown Error Message"));
      }
    }
  }
}
