import 'dart:collection';
import 'dart:io';

import 'package:biznes/Controllers/backendservicescontroller.dart';
import 'package:biznes/bloc/Exceptions.dart';
import 'package:biznes/model/inboxlistmessages.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'events.dart';
import 'states.dart';

class AlbumBloc extends Bloc<AllMessageEevents, AllMessageStates> {
  ConversationModel albums;
  UnmodifiableListView<AllMessages> allmessages;
  AlbumBloc() : super(AllMessagesInitailState());
  @override
  Stream<AllMessageStates> mapEventToState(AllMessageEevents event) async* {
    if (event is FetchMessage) {
      yield AllMessagesLoadingState();
      try {
        albums = await gettingData(event.userId, event.catId, event.roleName);
        print("heelo");
        allmessages = UnmodifiableListView(albums.allMessages.where((message) =>
            message.createdBy.sId == event.userId
                ? message.createdFor.roleId.length == 1
                    ? message.createdFor.roleId[0].title.toLowerCase().startsWith(event.query.toLowerCase()) ||
                        message.createdFor.username
                            .toString()
                            .toLowerCase()
                            .startsWith(event.query.toString().toLowerCase())
                    : message.createdFor.roleId.length == 2
                        ? message.createdFor.roleId[0].title.toLowerCase().startsWith(event.query.toLowerCase()) ||
                            message.createdFor.roleId[1].title
                                .toLowerCase()
                                .startsWith(event.query.toLowerCase()) ||
                            message.createdFor.username
                                .toString()
                                .toLowerCase()
                                .startsWith(event.query.toString().toLowerCase())
                        : message.createdFor.roleId.length == 3
                            ? message.createdFor.roleId[0].title.toLowerCase().startsWith(event.query.toLowerCase()) ||
                                message.createdFor.roleId[1].title
                                    .toLowerCase()
                                    .startsWith(event.query.toLowerCase()) ||
                                message.createdFor.roleId[2].title
                                    .toLowerCase()
                                    .startsWith(event.query.toLowerCase()) ||
                                message.createdFor.username
                                    .toString()
                                    .toLowerCase()
                                    .startsWith(event.query.toString().toLowerCase())
                            : message.createdFor.roleId.length == 4
                                ? message.createdFor.roleId[0].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdFor.roleId[1].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdFor.roleId[2].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdFor.roleId[3].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdFor.username.toString().toLowerCase().startsWith(event.query.toString().toLowerCase())
                                : message.createdFor.username.toLowerCase().startsWith(event.query.toLowerCase())
                : message.createdBy.roleId.length == 1
                    ? message.createdBy.roleId[0].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.username.toString().toLowerCase().startsWith(event.query.toString().toLowerCase())
                    : message.createdBy.roleId.length == 2
                        ? message.createdBy.roleId[0].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.roleId[1].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.username.toString().toLowerCase().startsWith(event.query.toString().toLowerCase())
                        : message.createdBy.roleId.length == 3
                            ? message.createdBy.roleId[0].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.roleId[1].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.roleId[2].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.username.toString().toLowerCase().startsWith(event.query.toString().toLowerCase())
                            : message.createdBy.roleId.length == 4
                                ? message.createdBy.roleId[0].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.roleId[1].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.roleId[2].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.roleId[3].title.toLowerCase().startsWith(event.query.toLowerCase()) || message.createdBy.username.toString().toLowerCase().startsWith(event.query.toString().toLowerCase())
                                : message.createdBy.username.toLowerCase().startsWith(event.query.toLowerCase())));
        yield AllMessagesLoadedState(allmessages: allmessages);
      } on SocketException {
        yield AlbumErrorState(
            error: NoInterNetException(message: "No InterNext"));
      } on HttpException {
        yield AlbumErrorState(
            error:
                NoServiceFoundException(message: "No Service Found Exception"));
      } on FormatException {
        yield AlbumErrorState(
            error: InvalidFormatException(message: "Invalid Format Exception"));
      } catch (e) {
        yield AlbumErrorState(
            error: UnknownException(message: "Unknown Error Message"));
      }
    }
  }
}
