import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class AllMessageEevents extends Equatable {}

class FetchMessage extends AllMessageEevents {
  final String userId;
  final String catId;
  final String roleName;
  final String query;
  FetchMessage({
    @required this.userId,
    @required this.catId,
    @required this.roleName,
    @required this.query,
  });
  List<Object> get props => [userId + catId + roleName];
}
