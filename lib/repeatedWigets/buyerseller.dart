import 'package:biznes/res/color.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/style.dart';
import 'package:biznes/res/size.dart';

BuyerSellerContainer({context, title, onTap, isselected = false}) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
      margin: EdgeInsets.symmetric(horizontal: 4.0),
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: borderColor.withOpacity(0.2),
              offset: Offset(0, 4),
              blurRadius: 2)
        ],
        color: isselected ? selectedcolor : Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      height: size.convert(context, 35),
      child: Center(
        child: Text(
          title,
          style: style.MontserratMedium(
              color: isselected ? Colors.white : gradientStartColor,
              fontSize: size.convert(context, 12)),
        ),
      ),
    ),
  );
}
