import 'package:biznes/res/color.dart';
import 'package:biznes/res/size.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:biznes/res/style.dart';

class filledButton extends StatelessWidget {
  final Function onTap;
  Color startColor;
  Color endColor;
  Color shadowColor;
  Color txtcolor;
  String txt;
  double h;
  double w;
  double radius;
  Color borderColor;
  double borderwidth;
  double fontsize;
  String fontfamily;
  Widget leftWidget;
  Widget rigtWidget;

  filledButton(
      {this.borderColor,
      this.borderwidth,
      this.fontfamily,
      this.txt,
      this.h,
      this.w,
      this.txtcolor,
      this.fontsize,
      this.leftWidget,
      this.rigtWidget,
      this.onTap,
      this.endColor,
      this.startColor,
      this.radius,
      this.shadowColor});
  @override
  Widget build(BuildContext context) {
    Size size1 = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.only(right: 10, left: 10),
        height: h ?? size.convert(context, 48),
        width: w ?? size.convertWidth(context, 260),
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: shadowColor ?? Color(0xff8FACFF).withOpacity(0.99),
                offset: Offset(0, 6),
                blurRadius: 9)
          ],
          gradient: LinearGradient(
            colors: [
              startColor ?? Color(0xff69bef7),
              endColor ?? Color(0xff7595f4)
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          borderRadius:
              BorderRadius.circular(radius ?? size.convert(context, 30)),
//            border: Border.all(
//            width: borderwidth == null ? 0 : borderwidth,
//              color: borderColor == null ? appColor : borderColor,
//        )
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            leftWidget ?? Container(),
            Text(
              txt == null ? "Empty" : txt,
              style: style.MontserratRegular(
                  fontSize: fontsize ?? size.convert(context, 16),
                  color: whiteColor),
            ),
            rigtWidget ?? Container(),
          ],
        ),
      ),
    );
  }
}
